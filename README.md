# Meteo del Trentino - Fattazzo
![paypal-donate-button.png](https://bitbucket.org/repo/A7orjK/images/4010477437-paypal-donate-button.png)
[clicca qui](https://www.paypal.com/cgi-bin/webscr?cmd=_donations&business=HDEWG6JE9CWQW&lc=IT&item_name=Meteo%20del%20Trentino&item_number=meteoapp&currency_code=EUR&bn=PP%2dDonationsBF%3abtn_donate_LG%2egif%3aNonHosted)

=======================

## Descrizione
Informazioni meteo ottenute tramite l'Ufficio Previsioni e Pianificazione della Provincia Autonoma di Trento dal sito: http://dati.trentino.it forniti da www.meteotrentino.it

## Guida
[Vai al wiki](https://bitbucket.org/fattazzo/meteo/wiki/)

## Applicazione
Link all'ultima versione disponibile: [Google Play Store](https://play.google.com/store/apps/details?id=com.gmail.fattazzo.meteo)

## Screenshot
![s_splash.png](https://bitbucket.org/repo/A7orjK/images/3544174760-s_splash.png)
![s_widget.png](https://bitbucket.org/repo/A7orjK/images/3679807707-s_widget.png)
![s_home.png](https://bitbucket.org/repo/A7orjK/images/4280734812-s_home.png)
![s_webcam.png](https://bitbucket.org/repo/A7orjK/images/4029060686-s_webcam.png)
![s_radar.png](https://bitbucket.org/repo/A7orjK/images/1537844071-s_radar.png)
![s_boll_prob.png](https://bitbucket.org/repo/A7orjK/images/2837259875-s_boll_prob.png)
![s_boll_locale.png](https://bitbucket.org/repo/A7orjK/images/4024808896-s_boll_locale.png)
![s_stazioni.png](https://bitbucket.org/repo/A7orjK/images/679810185-s_stazioni.png)