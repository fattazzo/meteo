package com.gmail.fattazzo.meteo.icon.controls.pages.clean;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.jdesktop.swingx.HorizontalLayout;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.combobox.ListComboBoxModel;

import com.gmail.fattazzo.meteo.icon.controls.pages.AbstractPage;
import com.gmail.fattazzo.meteo.icon.domain.IconsTheme;

/**
 *
 * @author fattazzo
 *
 *         date: 13/ott/2014
 *
 */
public class CleanPage extends AbstractPage {

	private class DeleteThemeButton extends JButton {

		/**
		 * Costruttore.
		 */
		public DeleteThemeButton() {
			super();

			setAction(new AbstractAction() {

				@Override
				public void actionPerformed(ActionEvent e) {
					if (themeComboBox.getItemCount() > 0) {
						String selectedTheme = (String) themeComboBox.getSelectedItem();

						File dirTheme = new File(IconsTheme.getBaseIconsPath() + selectedTheme);
						deleteFolder(dirTheme);

						CleanPage.this.refreshControl();
					}
				}
			});

			setText("Elimina");
		}

		private void deleteFolder(File folder) {
			File[] files = folder.listFiles();
			if (files != null) { // some JVMs return null for empty dirs
				for (File f : files) {
					if (f.isDirectory()) {
						deleteFolder(f);
					} else {
						f.delete();
					}
				}
			}
			folder.delete();
		}

	}

	public static final String PAGE_ID = "CleanPage";

	private JComboBox<String> themeComboBox;
	private JButton deleteThemeButton = new DeleteThemeButton();

	/**
	 * Costruttore.
	 */
	public CleanPage() {
		super();
		themeComboBox = new JComboBox<>();
		JPanel panel2 = new JPanel(new HorizontalLayout(10));
		panel2.add(themeComboBox);
		panel2.add(deleteThemeButton);

		JPanel panel = new JPanel(new VerticalLayout(10));
		panel.add(new JLabel("Selezionare il tema da eliminare"));
		panel.add(panel2);
		panel.setBorder(BorderFactory.createEmptyBorder(10, 5, 10, 5));

		add(panel, BorderLayout.CENTER);
	}

	@Override
	public String getId() {
		return PAGE_ID;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void refreshControl() {

		File baseDir = new File(IconsTheme.getBaseIconsPath());
		String[] themeDirs = baseDir.list();

		List<String> themeList = new ArrayList<>();
		for (String dir : themeDirs) {
			File dirTheme = new File(IconsTheme.getBaseIconsPath() + dir);
			if (dirTheme.isDirectory()) {
				themeList.add(dirTheme.getName());
			}
		}
		themeComboBox.setModel(new ListComboBoxModel<>(themeList));
	}

}
