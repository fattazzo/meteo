package com.gmail.fattazzo.meteo.icon.manager;

import com.gmail.fattazzo.meteo.icon.domain.IconsTheme;

import java.io.File;
import java.io.FilenameFilter;
import java.util.Arrays;
import java.util.Comparator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author fattazzo
 *
 *         date: 14/ott/2014
 *
 */
public class IconsThemeRetriever {

	/**
	 * Restituisce tutti i file delle icone presenti nel tema e path specificato.
	 *
	 * @param themeName
	 *            nome del tema
	 * @param path
	 *            path da cui caricare le icone
	 * @return icone caricate
	 */
	public File[] getIconsTheme(String themeName, String path) {

		File dirTheme = new File(IconsTheme.getPath(themeName,path));

		File[] icons = new File[] {};

		final Pattern p = Pattern.compile("[0-9]+");

		if (dirTheme.exists()) {
			icons = dirTheme.listFiles(new FilenameFilter() {
				@Override
				public boolean accept(File dir, String name) {
					return name.toLowerCase().endsWith(".png") || name.toLowerCase().endsWith(".gif");
				}
			});
			Arrays.sort(icons, new Comparator<File>() {

				@Override
				public int compare(File o1, File o2) {

					Integer n1 = 0;
					Integer n2 = 0;

					Matcher m1 = p.matcher(o1.getName());
					if (m1.find()) {
						n1 = Integer.parseInt(m1.group());
					}

					Matcher m2 = p.matcher(o2.getName());
					if (m2.find()) {
						n2 = Integer.parseInt(m2.group());
					}

					return n1.compareTo(n2);
				}
			});
		}

		return icons;
	}
}
