package com.gmail.fattazzo.meteo.icon.domain;

/**
 * @author fattazzo
 *         <p/>
 *         date: 09/03/16
 */
public class ThemeConfiguration {

    private int fogXOffset;
    private int fogYOffset;

    private int lightningXOffset;
    private int lightningYOffset;

    /**
     * @return the fogXOffset
     */
    public int getFogXOffset() {
        return fogXOffset;
    }

    /**
     * Setter of fogXOffset
     *
     * @param fogXOffset the fogXOffset to set
     */
    public void setFogXOffset(int fogXOffset) {
        this.fogXOffset = fogXOffset;
    }

    /**
     * @return the fogYOffset
     */
    public int getFogYOffset() {
        return fogYOffset;
    }

    /**
     * Setter of fogYOffset
     *
     * @param fogYOffset the fogYOffset to set
     */
    public void setFogYOffset(int fogYOffset) {
        this.fogYOffset = fogYOffset;
    }

    /**
     * @return the lightningXOffset
     */
    public int getLightningXOffset() {
        return lightningXOffset;
    }

    /**
     * Setter of lightningXOffset
     *
     * @param lightningXOffset the lightningXOffset to set
     */
    public void setLightningXOffset(int lightningXOffset) {
        this.lightningXOffset = lightningXOffset;
    }

    /**
     * @return the lightningYOffset
     */
    public int getLightningYOffset() {
        return lightningYOffset;
    }

    /**
     * Setter of lightningYOffset
     *
     * @param lightningYOffset the lightningYOffset to set
     */
    public void setLightningYOffset(int lightningYOffset) {
        this.lightningYOffset = lightningYOffset;
    }
}
