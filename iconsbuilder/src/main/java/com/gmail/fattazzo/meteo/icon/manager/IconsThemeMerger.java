package com.gmail.fattazzo.meteo.icon.manager;

import com.gmail.fattazzo.meteo.icon.domain.IconsTheme;
import com.gmail.fattazzo.meteo.icon.domain.ThemeConfiguration;
import com.gmail.fattazzo.meteo.icon.domain.VariantIconName;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.imageio.ImageIO;

public class IconsThemeMerger {

    private void buildFogLightningSet(String themeName, String[] baseFilesName,ThemeConfiguration configuration) {

        for (String baseF : baseFilesName) {

            File iconBase = new File(IconsTheme.getPathBase(themeName) + File.separator + baseF);
            File iconFog = new File(IconsTheme.getPath(themeName, IconsTheme.PATH_VARIANTS) + File.separator + "foschia.gif");

            Pattern p = Pattern.compile("[0-9]+");
            Matcher m1 = p.matcher(baseF);
            m1.find();
            int iconNumber = Integer.parseInt(m1.group());

            String iconFogName = "ico" + iconNumber + "_1_1.png";
            File iconsDest = new File(IconsTheme.getPath(themeName, IconsTheme.PATH_FOG_LIGHTNING) + File.separator + iconFogName);

            mergeIcons(iconBase, iconFog, iconsDest, configuration.getFogXOffset(), configuration.getFogYOffset());

            File iconLightning = new File(IconsTheme.getPath(themeName, IconsTheme.PATH_VARIANTS) + File.separator + "fulmine.gif");
            mergeIcons(iconsDest, iconLightning, iconsDest, configuration.getLightningXOffset(), configuration.getLightningYOffset());
        }
    }

    private void buildIconSet(String themeName, String[] baseFilesName, VariantIconName variante, int xOffset, int yOffset) {

        for (String baseF : baseFilesName) {

            File iconBase = new File(IconsTheme.getPathBase(themeName) + File.separator + baseF);
            File iconVariant = new File(IconsTheme.getPath(themeName, IconsTheme.PATH_VARIANTS) + File.separator + variante.getIconName());

            Pattern p = Pattern.compile("[0-9]+");
            Matcher m1 = p.matcher(baseF);
            if (m1.find()) {
                int iconNumber = Integer.parseInt(m1.group());

                String iconName = "ico" + iconNumber + variante.getDestIconSuffix() + ".png";
                File iconsDest = new File(IconsTheme.getPath(themeName, variante.getIconPath()) + File.separator + iconName);

                mergeIcons(iconBase, iconVariant, iconsDest, xOffset, yOffset);
            }
        }
    }

    private void mergeIcons(File icon1, File icon2, File iconDest, int xoffset, int yoffset) {
        try {
            // load source images
            BufferedImage image = ImageIO.read(icon1);
            BufferedImage overlay = ImageIO.read(icon2);

            int w = 60;
            int h = 60;
            BufferedImage combined = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);

            // paint both images, preserving the alpha channels
            Graphics g = combined.getGraphics();
            g.drawImage(image, 0, 0, null);
            g.drawImage(overlay, xoffset, yoffset, null);

            // Save as new image
            ImageIO.write(combined, FilenameUtils.getExtension(icon1.getName()), iconDest);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void rebuildIconsTheme(String themeName) {

        ThemeConfiguration themeConfiguration = ThemeConfigurationManager.loadConfiguration(themeName);

        File baseDir = new File(IconsTheme.getPath(themeName, IconsTheme.PATH_BASE));

        String[] baseFiles = baseDir.list();

        buildIconSet(themeName, baseFiles, VariantIconName.FOG,themeConfiguration.getFogXOffset(),themeConfiguration.getFogYOffset());
        buildIconSet(themeName, baseFiles, VariantIconName.LIGHTNING,themeConfiguration.getLightningXOffset(),themeConfiguration.getLightningYOffset());
        buildFogLightningSet(themeName, baseFiles,themeConfiguration);

        copyIconsToAllDir(themeName);
    }

    private void copyIconsToAllDir(String themeName) {

        File dirAll = new File(IconsTheme.getPathAll(themeName));

        File baseDir = new File(IconsTheme.getPath(themeName, IconsTheme.PATH_BASE));
        File fogDir = new File(IconsTheme.getPath(themeName, IconsTheme.PATH_FOG));
        File lightningDir = new File(IconsTheme.getPath(themeName, IconsTheme.PATH_LIGHTNING));
        File fogLightningDir = new File(IconsTheme.getPath(themeName, IconsTheme.PATH_FOG_LIGHTNING));
        try {
            FileUtils.cleanDirectory(dirAll);
            FileUtils.copyDirectory(baseDir,dirAll);
            FileUtils.copyDirectory(fogDir,dirAll);
            FileUtils.copyDirectory(lightningDir,dirAll);
            FileUtils.copyDirectory(fogLightningDir,dirAll);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
