package com.gmail.fattazzo.meteo.icon.domain;

import com.gmail.fattazzo.meteo.icon.controls.MainFrame;

import org.apache.commons.lang3.StringUtils;

import java.io.File;

public class IconsTheme {

	public static final String PATH_BASE = "base";
	public static final String PATH_FOG = "fog";
	public static final String PATH_LIGHTNING = "lightning";
	public static final String PATH_FOG_LIGHTNING = "foglightning";
	private static final String PATH_ALL = "all";
	public static final String PATH_VARIANTS = "variants";

	public static final String METEO_TRENTINO_PATH_THEME_NAME = "meteo_trentino";

    private static final String CONFIGURATION_FILE_NAME = "config.xml";

    public static String getBaseIconsPath() {

		String mainFramePath = MainFrame.class.getProtectionDomain().getCodeSource().getLocation().getPath();
		String basePath = StringUtils.left(mainFramePath, StringUtils.lastIndexOf(mainFramePath,"build"));

		return basePath + "src" + File.separator + "main" + File.separator + "res" + File.separator + "icons" + File.separator;
	}

    public static String getPathAll(String themeName) {

        return getBaseIconsPath() + themeName + File.separator + PATH_ALL;
    }

	public static String getPathBase(String themeName) {

		return getBaseIconsPath() + themeName + File.separator + PATH_BASE;
	}

    public static String getPath(String themeName, String path) {

        return getBaseIconsPath() + themeName + File.separator + path;
    }

    public static String getConfigurationFilePath(String themeName) {
        return IconsTheme.getPath(themeName,"config") + File.separator + IconsTheme.CONFIGURATION_FILE_NAME;
    }
}
