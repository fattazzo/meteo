package com.gmail.fattazzo.meteo.icon.controls.components;

import com.gmail.fattazzo.meteo.icon.controls.components.viewer.SetIconViewer;
import com.gmail.fattazzo.meteo.icon.controls.components.viewer.VariantsIconViewer;
import com.gmail.fattazzo.meteo.icon.domain.IconsTheme;
import com.gmail.fattazzo.meteo.icon.domain.ThemeConfiguration;
import com.gmail.fattazzo.meteo.icon.manager.ThemeConfigurationManager;

import javax.swing.JTabbedPane;

public class ThemeTabbedPane extends JTabbedPane {

    private final ImageSelectionMouseListener imageMouseListener;

    private boolean enableEditImage = false;
    private String currentTheme = null;
    private ThemeConfiguration currentThemeConfiguration = null;

    private com.gmail.fattazzo.meteo.icon.controls.components.viewer.IconViewer variantsViewer;

    public ThemeTabbedPane() {
        super();
        imageMouseListener = new ImageSelectionMouseListener(this);
    }

    /**
     * @return the currentTheme
     */
    public String getCurrentTheme() {
        return currentTheme;
    }

    /**
     * @return the currentThemeConfiguration
     */
    public ThemeConfiguration getCurrentThemeConfiguration() {
        currentThemeConfiguration = variantsViewer.updateConfigurationValues(currentThemeConfiguration);
        return currentThemeConfiguration;
    }

    public String getSelectedPath() {
        int selIndex = ThemeTabbedPane.this.getSelectedIndex();

        switch (selIndex) {
            case 0:
                return IconsTheme.PATH_BASE;
            case 1:
                return IconsTheme.PATH_FOG;
            case 2:
                return IconsTheme.PATH_LIGHTNING;
            case 3:
                return IconsTheme.PATH_FOG_LIGHTNING;
            case 4:
                return IconsTheme.PATH_VARIANTS;
            default:
                return IconsTheme.PATH_BASE;
        }
    }

    public void loadThemeIcons(String themeName) {

        removeAll();
        currentTheme = themeName;
        currentThemeConfiguration = ThemeConfigurationManager.loadConfiguration(themeName);

        if (themeName == null || themeName.isEmpty()) {
            return;
        }

        // icone base
        addTab("Base", new SetIconViewer(themeName, IconsTheme.PATH_BASE,enableEditImage,imageMouseListener));

        // icone nebbia
        addTab("Nebbia", new SetIconViewer(themeName, IconsTheme.PATH_FOG,enableEditImage,imageMouseListener));

        // icone fulmini
        addTab("Fulmini", new SetIconViewer(themeName, IconsTheme.PATH_LIGHTNING,enableEditImage,imageMouseListener));

        // icone nebbia+fulmini
        addTab("Nebbia + Fulmini", new SetIconViewer(themeName, IconsTheme.PATH_FOG_LIGHTNING,enableEditImage,imageMouseListener));

        // icone varianti
        variantsViewer = new VariantsIconViewer(themeName,enableEditImage,imageMouseListener,currentThemeConfiguration);
        addTab("Varianti", variantsViewer);
    }

    /**
     * @param enableEditImage the enableEditImage to set
     */
    public void setEnableEditImage(boolean enableEditImage) {
        this.enableEditImage = enableEditImage;
    }
}
