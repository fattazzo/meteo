package com.gmail.fattazzo.meteo.icon.controls;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.HeadlessException;

import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.WindowConstants;

import org.jdesktop.swingx.VerticalLayout;

import com.gmail.fattazzo.meteo.icon.controls.pages.AbstractPage;
import com.gmail.fattazzo.meteo.icon.controls.pages.PageButton;
import com.gmail.fattazzo.meteo.icon.controls.pages.clean.CleanPage;
import com.gmail.fattazzo.meteo.icon.controls.pages.edit.EditPage;
import com.gmail.fattazzo.meteo.icon.controls.pages.imports.ImportsPage;

/**
 *
 * @author fattazzo
 *
 *         date: 13/ott/2014
 *
 */
public class MainFrame extends JFrame {

	public static void main(String[] args) {
		MainFrame mainFrame = new MainFrame();
		mainFrame.setLocationRelativeTo(null);
		mainFrame.setVisible(true);
	}

	private JPanel pagesPanel;

	private AbstractPage cleanPage;
	private AbstractPage importPage;
	private AbstractPage editPage;

	/**
	 * @throws HeadlessException
	 */
	public MainFrame() throws HeadlessException {
		super("Meteo del Trentino - Icon Theme builder");
		setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		setBounds(20, 20, 800, 600);

		getContentPane().setLayout(new BorderLayout(10, 0));
		getContentPane().add(createPagesPanel(), BorderLayout.CENTER);
		getContentPane().add(createButtonPanel(), BorderLayout.WEST);
	}

	/**
	 * Crea il pannello che contiene i button per le varie azioni.
	 *
	 * @return button panel
	 */
	private JComponent createButtonPanel() {
		JPanel panel = new JPanel(new VerticalLayout(10));

		panel.add(new PageButton("Clean", cleanPage, pagesPanel));
		panel.add(new PageButton("Import", importPage, pagesPanel));
		panel.add(new PageButton("Edit", editPage, pagesPanel));

		panel.setBorder(BorderFactory.createEmptyBorder(10, 5, 10, 5));

		return panel;
	}

	private JPanel createPagesPanel() {

		pagesPanel = new JPanel(new CardLayout());
		pagesPanel.add(new JPanel(), "StartPage");

		// clean page
		cleanPage = new CleanPage();
		pagesPanel.add(cleanPage, CleanPage.PAGE_ID);

		// import page
		importPage = new ImportsPage();
		pagesPanel.add(importPage, ImportsPage.PAGE_ID);

		// edit page
		editPage = new EditPage();
		pagesPanel.add(editPage, EditPage.PAGE_ID);

		return pagesPanel;
	}
}
