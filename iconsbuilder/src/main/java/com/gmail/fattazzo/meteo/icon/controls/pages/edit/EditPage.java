package com.gmail.fattazzo.meteo.icon.controls.pages.edit;

import com.gmail.fattazzo.meteo.icon.controls.components.ThemeTabbedPane;
import com.gmail.fattazzo.meteo.icon.controls.pages.AbstractPage;
import com.gmail.fattazzo.meteo.icon.domain.IconsTheme;
import com.gmail.fattazzo.meteo.icon.manager.IconsThemeMerger;
import com.gmail.fattazzo.meteo.icon.manager.ThemeConfigurationManager;

import org.apache.commons.io.FileUtils;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.combobox.ListComboBoxModel;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class EditPage extends AbstractPage {

    public static final String PAGE_ID = "EditPage";

    private final JPanel headerPanel;
    private final JComboBox<String> themeComboBox;
    private final JPanel iconsPanel;
    private final ThemeTabbedPane tabbedPane;
    private List<String> themeList;

    public EditPage() {
        super();

        headerPanel = new JPanel(new FlowLayout(FlowLayout.LEFT, 10, 2));
        headerPanel.add(new JLabel("Tema:"));
        themeComboBox = new JComboBox<>();
        themeComboBox.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                tabbedPane.loadThemeIcons((String) themeComboBox.getSelectedItem());
            }
        });
        headerPanel.add(themeComboBox);
        headerPanel.add(new NewThemeButton());
        headerPanel.add(new RebuldThemeIconsButton());
        add(headerPanel, BorderLayout.NORTH);

        iconsPanel = new JPanel(new BorderLayout());
        tabbedPane = new ThemeTabbedPane();
        tabbedPane.setEnableEditImage(true);
        iconsPanel.add(tabbedPane, BorderLayout.CENTER);
        add(iconsPanel);
    }

    @Override
    public String getId() {
        return PAGE_ID;
    }

    @SuppressWarnings("unchecked")
    @Override
    public void refreshControl() {

        File baseDir = new File(IconsTheme.getBaseIconsPath());
        String[] themeDirs = baseDir.list();

        themeList = new ArrayList<>();
        for (String dir : themeDirs) {
            File dirTheme = new File(IconsTheme.getBaseIconsPath() + dir);
            if (dirTheme.isDirectory() && !IconsTheme.METEO_TRENTINO_PATH_THEME_NAME.equals(dirTheme.getName())) {
                themeList.add(dirTheme.getName());
            }
        }
        themeComboBox.setModel(new ListComboBoxModel<>(themeList));

        tabbedPane.loadThemeIcons((String) themeComboBox.getSelectedItem());
    }

    private class NewThemeButton extends JButton {

        private static final long serialVersionUID = 6960997283930578825L;

        public NewThemeButton() {
            super();

            setAction(new AbstractAction() {

                private static final long serialVersionUID = 2739510069733251219L;

                @Override
                public void actionPerformed(ActionEvent arg0) {

                    JPanel panel = new JPanel(new VerticalLayout(5));

                    JPanel namePanel = new JPanel(new BorderLayout(2, 2));
                    namePanel.add(new JLabel("Nome:"), BorderLayout.WEST);
                    JTextField newThemeTextField = new JTextField();
                    namePanel.add(newThemeTextField, BorderLayout.CENTER);
                    panel.add(namePanel);

                    JPanel refThemePanel = new JPanel(new BorderLayout(2, 2));
                    refThemePanel.add(new JLabel("Copia da:"), BorderLayout.WEST);

                    List<String> list = new ArrayList<>();
                    list.add(IconsTheme.METEO_TRENTINO_PATH_THEME_NAME);
                    list.addAll(themeList);
                    JComboBox<String> comboDir = new JComboBox<>(list.toArray(new String[list.size()]));
                    refThemePanel.add(comboDir, BorderLayout.CENTER);
                    panel.add(refThemePanel);

                    int dialogResult = JOptionPane.showConfirmDialog(null, panel, "Crea nuovo tema",
                            JOptionPane.OK_CANCEL_OPTION);
                    if (dialogResult == JOptionPane.YES_OPTION) {
                        try {
                            FileUtils.copyDirectory(new File(IconsTheme.getBaseIconsPath() + comboDir.getSelectedItem()), new File(
                                    IconsTheme.getBaseIconsPath() + newThemeTextField.getText()));
                        } catch (IOException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        } finally {
                            refreshControl();
                        }
                    }
                }
            });

            setText("Nuovo");
        }

    }

    private class RebuldThemeIconsButton extends JButton {

        private final IconsThemeMerger iconsThemeMerger = new IconsThemeMerger();

        public RebuldThemeIconsButton() {
            super();

            setAction(new AbstractAction() {

                @Override
                public void actionPerformed(ActionEvent arg0) {
                    if (themeList != null && !themeList.isEmpty()) {
                        ThemeConfigurationManager.saveConfiguration((String) themeComboBox.getSelectedItem(), tabbedPane.getCurrentThemeConfiguration());

                        iconsThemeMerger.rebuildIconsTheme((String) themeComboBox.getSelectedItem());

                        refreshControl();
                    }
                }
            });

            setText("Rebuild icons");
        }

    }

}
