package com.gmail.fattazzo.meteo.icon.controls.pages;

import java.awt.CardLayout;
import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.JButton;
import javax.swing.JPanel;

/**
 *
 * @author fattazzo
 *
 *         date: 13/ott/2014
 *
 */
public class PageButton extends JButton {

	private AbstractPage page;

	private JPanel pagesPanel;

	/**
	 * Costruttore.
	 *
	 * @param text testo
	 * @param page page
	 * @param pagesPanel panel
	 */
	public PageButton(String text, AbstractPage page, JPanel pagesPanel) {
		super();
		this.page = page;
		this.pagesPanel = pagesPanel;

		setAction(new AbstractAction() {

			@Override
			public void actionPerformed(ActionEvent e) {
				CardLayout cl = (CardLayout) (PageButton.this.pagesPanel.getLayout());
				cl.show(PageButton.this.pagesPanel, PageButton.this.page.getId());
				PageButton.this.page.refreshControl();
			}
		});

		setText(text);
	}

}
