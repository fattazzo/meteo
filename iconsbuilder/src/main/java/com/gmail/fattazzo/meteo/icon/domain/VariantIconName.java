package com.gmail.fattazzo.meteo.icon.domain;

/**
 * @author fattazzo
 *         <p/>
 *         date: 09/03/16
 */
public enum VariantIconName {

    FOG("foschia.gif", IconsTheme.PATH_FOG,"_1_0"), LIGHTNING("fulmine.gif", IconsTheme.PATH_LIGHTNING,"_0_1");

    private String iconPath;
    private String iconName;
    private String destIconSuffix;

    VariantIconName(String iconName, String iconPath,String destIconSuffix) {
        this.iconName = iconName;
        this.iconPath = iconPath;
        this.destIconSuffix = destIconSuffix;
    }

    /**
     * @return the destIconSuffix
     */
    public String getDestIconSuffix() {
        return destIconSuffix;
    }

    /**
     * @return the iconPath
     */
    public String getIconPath() {
        return iconPath;
    }

    /**
     * @return the iconName
     */
    public String getIconName() {
        return iconName;
    }
}
