package com.gmail.fattazzo.meteo.icon.controls.components.viewer;

import com.gmail.fattazzo.meteo.icon.controls.components.ImageSelectionMouseListener;
import com.gmail.fattazzo.meteo.icon.domain.IconsTheme;
import com.gmail.fattazzo.meteo.icon.domain.ThemeConfiguration;
import com.gmail.fattazzo.meteo.icon.domain.VariantIconName;
import com.jgoodies.forms.builder.PanelBuilder;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;

import java.awt.BorderLayout;
import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SwingConstants;

/**
 * @author fattazzo
 *         <p/>
 *         date: 10/03/16
 */
public class VariantsIconViewer extends com.gmail.fattazzo.meteo.icon.controls.components.viewer.IconViewer {

    private JSpinner fogXOffsetSpinner = new JSpinner();
    private JSpinner fogYOffsetSpinner = new JSpinner();

    private JSpinner lighningXOffsetSpinner = new JSpinner();
    private JSpinner lighningYOffsetSpinner = new JSpinner();

    /**
     * Costruttore.
     *
     * @param themeName nome tema
     * @param enableEditImage abilita la possibilità di editare le immagini
     * @param imageMouseListener listener per la selezione dell'immagine
     * @param configuration configurazione del tema
     */
    public VariantsIconViewer(String themeName, boolean enableEditImage, ImageSelectionMouseListener imageMouseListener, ThemeConfiguration configuration) {

        init(themeName, enableEditImage, imageMouseListener);

        loadFromConfiguration(configuration);
    }

    /**
     * Init dei controlli.
     *
     * @param themeName nome tema
     * @param enableEditImage abilita la possibilità di editare le immagini
     * @param imageMouseListener listener per la selezione dell'immagine
     */
    private void init(String themeName, boolean enableEditImage, ImageSelectionMouseListener imageMouseListener) {
        FormLayout layout = new FormLayout("50dlu, 10dlu, left:pref,4dlu,40dlu", "2dlu,30dlu,2dlu,default,20dlu,30dlu,2dlu,default");
        PanelBuilder builder = new PanelBuilder(layout);
        CellConstraints cc = new CellConstraints();

        //fog
        try {
            File file = new File(IconsTheme.getPath(themeName, IconsTheme.PATH_VARIANTS) + File.separator + VariantIconName.FOG.getIconName());
            JPanel imagePanel = new JPanel(new BorderLayout());

            BufferedImage image = ImageIO.read(file);
            JLabel imageLabel = new JLabel(new ImageIcon(image));
            imageLabel.setName(file.getName());
            if (enableEditImage) {
                imageLabel.addMouseListener(imageMouseListener);
            }
            imagePanel.add(imageLabel, BorderLayout.CENTER);
            imagePanel.add(new JLabel(file.getName(), SwingConstants.CENTER), BorderLayout.SOUTH);

            builder.add(imagePanel, cc.xywh(1, 2, 1, 3));
            builder.add(new JLabel("X offset"), cc.xy(3, 2));
            builder.add(fogXOffsetSpinner, cc.xy(5, 2));
            builder.add(new JLabel("Y offset"), cc.xy(3, 4));
            builder.add(fogYOffsetSpinner, cc.xy(5, 4));

        } catch (Exception e) {
            System.err.println("Errore nel visualizzare l'icona.");
        }

        //lighning
        try {
            File file = new File(IconsTheme.getPath(themeName, IconsTheme.PATH_VARIANTS) + File.separator + VariantIconName.LIGHTNING.getIconName());
            JPanel imagePanel = new JPanel(new BorderLayout());

            BufferedImage image = ImageIO.read(file);
            JLabel imageLabel = new JLabel(new ImageIcon(image));
            imageLabel.setName(file.getName());
            if (enableEditImage) {
                imageLabel.addMouseListener(imageMouseListener);
            }
            imagePanel.add(imageLabel, BorderLayout.CENTER);
            imagePanel.add(new JLabel(file.getName(), SwingConstants.CENTER), BorderLayout.SOUTH);

            builder.add(imagePanel, cc.xywh(1, 6, 1, 3));
            builder.add(new JLabel("X offset"), cc.xy(3, 6));
            builder.add(lighningXOffsetSpinner, cc.xy(5, 6));
            builder.add(new JLabel("Y offset"), cc.xy(3, 8));
            builder.add(lighningYOffsetSpinner, cc.xy(5, 8));

        } catch (Exception e) {
            System.err.println("Errore nel visualizzare l'icona.");
        }

        add(builder.getPanel());
    }

    /**
     * Carica i valori della configurazione nei controlli.
     *
     * @param configuration configurazione
     */
    private void loadFromConfiguration(ThemeConfiguration configuration) {

        fogXOffsetSpinner.setValue(configuration.getFogXOffset());
        fogYOffsetSpinner.setValue(configuration.getFogYOffset());

        lighningXOffsetSpinner.setValue(configuration.getLightningXOffset());
        lighningYOffsetSpinner.setValue(configuration.getLightningYOffset());
    }

    @Override
    public ThemeConfiguration updateConfigurationValues(ThemeConfiguration configuration) {

        configuration.setFogXOffset((Integer) fogXOffsetSpinner.getValue());
        configuration.setFogYOffset((Integer) fogYOffsetSpinner.getValue());

        configuration.setLightningXOffset((Integer) lighningXOffsetSpinner.getValue());
        configuration.setLightningYOffset((Integer) lighningYOffsetSpinner.getValue());

        return configuration;
    }
}
