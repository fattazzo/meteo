package com.gmail.fattazzo.meteo.icon.controls.pages.imports;

import java.awt.BorderLayout;
import java.awt.Dialog.ModalityType;
import java.awt.FlowLayout;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;

import javax.swing.AbstractAction;
import javax.swing.AbstractButton;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;

import com.gmail.fattazzo.meteo.icon.controls.components.ThemeTabbedPane;
import com.gmail.fattazzo.meteo.icon.controls.pages.AbstractPage;
import com.gmail.fattazzo.meteo.icon.domain.IconsTheme;
import com.gmail.fattazzo.meteo.icon.manager.MeteoTrentinoIconDownloader;

/**
 *
 * @author fattazzo
 *
 *         date: 13/ott/2014
 *
 */
public class ImportsPage extends AbstractPage {

	private class DownloadButton extends JButton {

		private static final long serialVersionUID = -3037792018962917578L;

		/**
		 * Costruttore.
		 */
		public DownloadButton() {
			super();

			setAction(new AbstractAction() {

				private static final long serialVersionUID = -2362894325021904498L;

				@Override
				public void actionPerformed(ActionEvent e) {

					SwingWorker<Void, Void> worker = new SwingWorker<Void, Void>() {

						@Override
						protected Void doInBackground() throws Exception {
							MeteoTrentinoIconDownloader downloader = new MeteoTrentinoIconDownloader();
							downloader.downloadMeteoTrentinoIconTheme();

							return null;
						}

					};

					Window win = SwingUtilities.getWindowAncestor((AbstractButton) e.getSource());
					final JDialog dialog = new JDialog(win, "Attendere", ModalityType.APPLICATION_MODAL);

					worker.addPropertyChangeListener(new PropertyChangeListener() {

						@Override
						public void propertyChange(PropertyChangeEvent evt) {
							if (evt.getPropertyName().equals("state")) {
								if (evt.getNewValue() == SwingWorker.StateValue.DONE) {
									dialog.dispose();
									ImportsPage.this.refreshControl();
								}
							}
						}
					});
					worker.execute();

					JProgressBar progressBar = new JProgressBar();
					progressBar.setIndeterminate(true);
					JPanel panel = new JPanel(new BorderLayout());
					panel.add(progressBar, BorderLayout.CENTER);
					panel.add(new JLabel("Donwload del tema in corso......."), BorderLayout.PAGE_START);
					dialog.add(panel);
					dialog.pack();
					dialog.setLocationRelativeTo(win);
					dialog.setVisible(true);
				}
			});

			setText("Scarica");
		}
	}

	public static final String PAGE_ID = "ImportPage";

	private JPanel headerPanel;
	private JPanel iconsPanel;

	private ThemeTabbedPane tabbedPane;

	/**
	 * Costruttore.
	 */
	public ImportsPage() {
		super();

		headerPanel = new JPanel(new FlowLayout(FlowLayout.LEFT, 10, 2));
		headerPanel.add(new JLabel("Tema non presente."));
		headerPanel.add(new DownloadButton());
		add(headerPanel, BorderLayout.NORTH);

		iconsPanel = new JPanel(new BorderLayout());
		tabbedPane = new ThemeTabbedPane();
		iconsPanel.add(tabbedPane, BorderLayout.CENTER);
		add(iconsPanel);
	}

	@Override
	public String getId() {
		return PAGE_ID;
	}

	private void populateIconsPanel() {

		tabbedPane.loadThemeIcons(IconsTheme.METEO_TRENTINO_PATH_THEME_NAME);
	}

	@Override
	public void refreshControl() {

		File meteoTrentinoThemeDir = new File(IconsTheme.getBaseIconsPath() + File.separator + IconsTheme.METEO_TRENTINO_PATH_THEME_NAME);
		headerPanel.setVisible(!meteoTrentinoThemeDir.exists());

		iconsPanel.setVisible(meteoTrentinoThemeDir.exists());
		if (meteoTrentinoThemeDir.exists()) {
			populateIconsPanel();
		}
	}

}
