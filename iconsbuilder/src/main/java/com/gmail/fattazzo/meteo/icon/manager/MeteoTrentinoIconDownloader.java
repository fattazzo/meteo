package com.gmail.fattazzo.meteo.icon.manager;

import com.gmail.fattazzo.meteo.icon.domain.IconsTheme;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;

/**
 * @author Fattazzo
 *         <p/>
 *         Date 12/ott/2014
 */
public class MeteoTrentinoIconDownloader {

    private static final int ICON_NUMBER = 50;

    private static final String URL_ICON_BASE = "http://www.meteotrentino.it/bollettini/today/icometeo/";

    private String buidIconName(int number, boolean addFog, boolean addLightning) {
        return "ico" + number + "_" + (addFog ? "1" : "0") + "_" + (addLightning ? "1" : "0") + ".png";
    }

    private void downloadIcons(String path, boolean addFog, boolean addLightning) {

        File themeDir = new File(IconsTheme.getPath(IconsTheme.METEO_TRENTINO_PATH_THEME_NAME, path));
        themeDir.mkdir();

        for (int i = 0; i < ICON_NUMBER; i++) {
            String iconName = buidIconName(i, addFog, addLightning);
            try {
                String iconNamePath = IconsTheme.getPath(IconsTheme.METEO_TRENTINO_PATH_THEME_NAME, path) + File.separator + iconName;
                String iconNamePathAll = IconsTheme.getPath(IconsTheme.METEO_TRENTINO_PATH_THEME_NAME, path) + File.separator + iconName;

                saveImage(URL_ICON_BASE + iconName, iconNamePath, iconNamePathAll);
                System.out.println("Salvata icona: " + iconName);
            } catch (FileNotFoundException e1) {
                // e1.printStackTrace();
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

    public void downloadMeteoTrentinoIconTheme() {

        File themeDir = new File(IconsTheme.getPathAll(IconsTheme.METEO_TRENTINO_PATH_THEME_NAME));
        themeDir.mkdirs();

        downloadIcons(IconsTheme.PATH_BASE, false, false);
        downloadIcons(IconsTheme.PATH_FOG, true, false);
        downloadIcons(IconsTheme.PATH_LIGHTNING, false, true);
        downloadIcons(IconsTheme.PATH_FOG_LIGHTNING, true, true);
        downloadVariants();
    }

    private void downloadVariants() {

        File themeDir = new File(IconsTheme.getPath(IconsTheme.METEO_TRENTINO_PATH_THEME_NAME,IconsTheme.PATH_VARIANTS));
        themeDir.mkdir();

        try {
            String iconNamePath = IconsTheme.getPath(IconsTheme.METEO_TRENTINO_PATH_THEME_NAME,IconsTheme.PATH_VARIANTS) + File.separator;
            saveImage(URL_ICON_BASE + "foschia.gif", iconNamePath + "foschia.gif", null);
            saveImage(URL_ICON_BASE + "fulmine.gif", iconNamePath + "fulmine.gif", null);
        } catch (FileNotFoundException e1) {
            // e1.printStackTrace();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    private void saveImage(String imageUrl, String destinationFile, String destinationFileAll) throws Exception {
        URL url = new URL(imageUrl);

        InputStream is = url.openStream();
        OutputStream os = new FileOutputStream(destinationFile);
        InputStream isAll = url.openStream();

        byte[] b = new byte[2048];
        int length;

        while ((length = is.read(b)) != -1) {
            os.write(b, 0, length);
        }

        is.close();
        os.close();

        if (destinationFileAll != null) {
            OutputStream osAll = new FileOutputStream(destinationFileAll);
            while ((length = isAll.read(b)) != -1) {
                osAll.write(b, 0, length);
            }
            osAll.close();
        }

        isAll.close();

    }

}
