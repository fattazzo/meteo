package com.gmail.fattazzo.meteo.icon.controls.pages;

import java.awt.BorderLayout;

import javax.swing.JPanel;

/**
 *
 * @author fattazzo
 *
 *         date: 13/ott/2014
 *
 */
public abstract class AbstractPage extends JPanel implements IPage {

	/**
	 * Costruttore.
	 */
	protected AbstractPage() {
		super(new BorderLayout());
	}

	public abstract String getId();

}
