package com.gmail.fattazzo.meteo.icon.manager;

import com.gmail.fattazzo.meteo.icon.domain.IconsTheme;
import com.gmail.fattazzo.meteo.icon.domain.ThemeConfiguration;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import java.io.File;
import java.io.FileWriter;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

/**
 * @author fattazzo
 *         <p/>
 *         date: 09/03/16
 */
public class ThemeConfigurationManager {

    public static void saveConfiguration(String themeName, ThemeConfiguration configuration) {

        try {
            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

            // root elements
            Document doc = docBuilder.newDocument();
            Element rootElement = doc.createElement("configuration");
            doc.appendChild(rootElement);

            Element variantsNode = doc.createElement("variants");
            rootElement.appendChild(variantsNode);

            Element fogNode = doc.createElement("fog");
            variantsNode.appendChild(fogNode);
            Element fogXOffset = doc.createElement("XOffset");
            fogXOffset.appendChild(doc.createTextNode(String.valueOf(configuration.getFogXOffset())));
            fogNode.appendChild(fogXOffset);
            Element fogYOffset = doc.createElement("YOffset");
            fogYOffset.appendChild(doc.createTextNode(String.valueOf(configuration.getFogYOffset())));
            fogNode.appendChild(fogYOffset);

            Element lightningNode = doc.createElement("lightning");
            variantsNode.appendChild(lightningNode);
            Element lightningXOffset = doc.createElement("XOffset");
            lightningXOffset.appendChild(doc.createTextNode(String.valueOf(configuration.getLightningXOffset())));
            lightningNode.appendChild(lightningXOffset);
            Element lightningYOffset = doc.createElement("YOffset");
            lightningYOffset.appendChild(doc.createTextNode(String.valueOf(configuration.getLightningYOffset())));
            lightningNode.appendChild(lightningYOffset);


            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
            DOMSource source = new DOMSource(doc);
            File file = new File(IconsTheme.getConfigurationFilePath(themeName));
            file.getParentFile().mkdir();
            FileWriter fileWriter = new FileWriter(file);
            StreamResult streamResult = new StreamResult(fileWriter);
            transformer.transform(source, streamResult);

        } catch (Exception e) {
            System.err.println("Errore durante la lettura della configurazione per il tema " + themeName);
        }
    }

    public static ThemeConfiguration loadConfiguration(String themeName) {

        ThemeConfiguration configuration = new ThemeConfiguration();

        File fileConfig = new File(IconsTheme.getConfigurationFilePath(themeName));

        if (fileConfig.exists()) {

            try {
                File fXmlFile = new File(IconsTheme.getConfigurationFilePath(themeName));
                DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
                DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
                Document doc = dBuilder.parse(fXmlFile);
                doc.getDocumentElement().normalize();

                NodeList nList = doc.getElementsByTagName("fog");
                Element fogNode = (Element) nList.item(0);
                configuration.setFogXOffset(Integer.valueOf(fogNode.getElementsByTagName("XOffset").item(0).getTextContent()));
                configuration.setFogYOffset(Integer.valueOf(fogNode.getElementsByTagName("YOffset").item(0).getTextContent()));

                nList = doc.getElementsByTagName("lightning");
                Element lightningNode = (Element) nList.item(0);
                configuration.setLightningXOffset(Integer.valueOf(lightningNode.getElementsByTagName("XOffset").item(0).getTextContent()));
                configuration.setLightningYOffset(Integer.valueOf(lightningNode.getElementsByTagName("YOffset").item(0).getTextContent()));
            } catch (Exception e) {
                System.err.println("Errore durante la lettura della configurazione per il tema " + themeName);
            }
        }

        return configuration;
    }
}
