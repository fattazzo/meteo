package com.gmail.fattazzo.meteo.icon.controls.components.viewer;

import com.gmail.fattazzo.meteo.icon.domain.ThemeConfiguration;

import javax.swing.JPanel;

/**
 * @author fattazzo
 *         <p/>
 *         date: 17/03/16
 */
public abstract class IconViewer extends JPanel {

    /**
     * Aggiorna i dati della configurazione con quelli presenti nel viewer.
     *
     * @param configuration configurazione da aggiornare
     * @return configurazione aggiornata
     */
    public ThemeConfiguration updateConfigurationValues(ThemeConfiguration configuration){
        return configuration;
    }
}
