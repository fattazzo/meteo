package com.gmail.fattazzo.meteo.icon.controls.pages;

/**
 *
 * @author fattazzo
 *
 *         date: 13/ott/2014
 *
 */
public interface IPage {

	void refreshControl();

}
