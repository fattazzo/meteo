package com.gmail.fattazzo.meteo.icon.controls.components.viewer;

import com.gmail.fattazzo.meteo.icon.controls.components.ImageSelectionMouseListener;
import com.gmail.fattazzo.meteo.icon.manager.IconsThemeRetriever;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

/**
 * @author fattazzo
 *         <p/>
 *         date: 17/03/16
 */
public class SetIconViewer extends IconViewer {

    private static final int GRID_COLUMNS = 6;
    private static final int GRID_ROWS = 5;

    private final IconsThemeRetriever iconsRetriever = new IconsThemeRetriever();
    private final boolean enableEditImage;
    private final ImageSelectionMouseListener imageMouseListener;

    /**
     * Costruttore.
     *
     * @param themeName nome tema
     * @param path path da visualizzare
     * @param enableEditImage abilita la possibilità di editare le immagini
     * @param imageMouseListener listener per la selezione dell'immagine
     */
    public SetIconViewer(String themeName, String path, boolean enableEditImage, ImageSelectionMouseListener imageMouseListener) {
        this.enableEditImage = enableEditImage;
        this.imageMouseListener = imageMouseListener;

        init(themeName, path);
    }

    /**
     * Init dei controlli.
     *
     * @param themeName nome tema
     * @param path path
     */
    private void init(String themeName, String path) {
        setLayout(new GridLayout(GRID_ROWS, GRID_COLUMNS, 5, 5));

        File[] iconsFile = iconsRetriever.getIconsTheme(themeName, path);

        for (File file : iconsFile) {
            try {
                JPanel imagePanel = new JPanel(new BorderLayout());

                BufferedImage image = ImageIO.read(file);
                JLabel imageLabel = new JLabel(new ImageIcon(image));
                imageLabel.setName(file.getName());
                if (enableEditImage) {
                    imageLabel.addMouseListener(imageMouseListener);
                }
                imagePanel.add(imageLabel, BorderLayout.CENTER);
                imagePanel.add(new JLabel(file.getName(), SwingConstants.CENTER), BorderLayout.SOUTH);

                add(imagePanel);
            } catch (IOException e) {
                e.printStackTrace();
                add(new JLabel("error"));
            }
        }
    }
}
