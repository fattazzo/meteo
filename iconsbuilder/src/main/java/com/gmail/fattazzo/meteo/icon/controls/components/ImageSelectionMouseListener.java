package com.gmail.fattazzo.meteo.icon.controls.components;

import com.gmail.fattazzo.meteo.icon.domain.IconsTheme;

import org.apache.commons.io.FilenameUtils;

import java.awt.Graphics;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.filechooser.FileNameExtensionFilter;

public class ImageSelectionMouseListener extends MouseAdapter {

    private ThemeTabbedPane themeTabbedPane;

    public ImageSelectionMouseListener(ThemeTabbedPane themeTabbedPane) {
        this.themeTabbedPane = themeTabbedPane;
    }

    @Override
    public void mousePressed(MouseEvent e) {
        if (e.getClickCount() == 2 && e.getSource().getClass().equals(JLabel.class) && themeTabbedPane.getCurrentTheme() != null
                && !themeTabbedPane.getCurrentTheme().isEmpty()) {
            String iconName = ((JLabel) e.getSource()).getName();

            JFileChooser fc = new JFileChooser();
            fc.setDialogTitle("Choose Image..");
            fc.setAccessory(new ImagePreviewPanel());

            FileNameExtensionFilter filter = new FileNameExtensionFilter("JPG Images", "jpg", "Png Images", "png");
            fc.setFileFilter(filter);
            int returnVal = fc.showDialog(null, "Choose..");

            if (returnVal == JFileChooser.APPROVE_OPTION) {
                File file = fc.getSelectedFile();

                try {
                    BufferedImage image = ImageIO.read(file);

                    int selIndex = themeTabbedPane.getSelectedIndex();
                    String currentPAth = themeTabbedPane.getSelectedPath();
                    File fileName = new File(IconsTheme.getPath(themeTabbedPane.getCurrentTheme(), currentPAth) + File.separator + iconName);
                    fileName.delete();

                    BufferedImage combined = new BufferedImage(60, 60, BufferedImage.TYPE_INT_ARGB);
                    // paint both images, preserving the alpha channels
                    Graphics g = combined.getGraphics();
                    g.drawImage(image, 0, 0, null);
                    ImageIO.write(combined, FilenameUtils.getExtension(file.getName()), fileName);

                    themeTabbedPane.loadThemeIcons(themeTabbedPane.getCurrentTheme());
                    themeTabbedPane.setSelectedIndex(selIndex);
                } catch (Exception e1) {
                    e1.printStackTrace();
                }
            }
        }
    }
}