package com.gmail.fattazzo.meteo.fragment.webcam.component;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;

/**
 *
 * @author fattazzo
 *
 *         date: 13/lug/2015
 *
 */
public class WebcamWebView extends AbstractWebcamView {

    private WebView webView;

    /**
     * Costruttore.
     *
     * @param context
     *            context
     * @param paramUrlToLoad
     *            url da caricare
     */
    public WebcamWebView(final Context context, final String paramUrlToLoad) {
        super(paramUrlToLoad, context);
    }

    /**
     * Crea la webview che contiene il contenuto della webcam.
     */
    private void createWebView() {
        webView = new WebView(getContext());
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setUseWideViewPort(true);
        webView.setPadding(0, 0, 0, 0);
        webView.setBackgroundColor(0x00000000);

        webView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                getProgressBar().setVisibility(View.GONE);
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                getProgressBar().setVisibility(View.VISIBLE);
                getProgressBar().animate();
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }
        });
    }

    @Override
    public View getWebcamView() {

        createWebView();

        return webView;
    }

    @Override
    public void loadWebcam() {
        webView.loadUrl(urlToLoad);
    }

}
