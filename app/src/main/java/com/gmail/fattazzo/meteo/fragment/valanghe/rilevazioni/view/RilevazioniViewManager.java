package com.gmail.fattazzo.meteo.fragment.valanghe.rilevazioni.view;

import java.util.ArrayList;
import java.util.List;

import android.util.SparseArray;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Toast;

import com.gmail.fattazzo.meteo.R;
import com.gmail.fattazzo.meteo.domain.valanghe.anagrafica.StazioneValanghe;
import com.gmail.fattazzo.meteo.domain.valanghe.dati.DatiStazione;
import com.gmail.fattazzo.meteo.fragment.valanghe.rilevazioni.sezioni.altezzamantonevoso.AltezzaMantoNevosoSezioneManager;
import com.gmail.fattazzo.meteo.fragment.valanghe.rilevazioni.sezioni.altezzanevefresca.AltezzaNeveFrescaSezioneManager;
import com.gmail.fattazzo.meteo.fragment.valanghe.rilevazioni.sezioni.brinasuperficie.BrinaSuperficieSezioneManager;
import com.gmail.fattazzo.meteo.fragment.valanghe.rilevazioni.sezioni.condtempo.CondTempoSezioneManager;
import com.gmail.fattazzo.meteo.fragment.valanghe.rilevazioni.sezioni.nuvolisita.NuvolositaSezioneManager;
import com.gmail.fattazzo.meteo.fragment.valanghe.rilevazioni.sezioni.rugositasuperficiale.RugositaSuperficialeSezioneManager;
import com.gmail.fattazzo.meteo.fragment.valanghe.rilevazioni.sezioni.stratosuperficiale.StratoSuperficialeSezioneManager;
import com.gmail.fattazzo.meteo.fragment.valanghe.rilevazioni.sezioni.temperaturaaria.TemperaturaAriaSezioneManager;
import com.gmail.fattazzo.meteo.fragment.valanghe.rilevazioni.sezioni.temperaturamaxmin.TemperaturaMaxMinSezioneManager;
import com.gmail.fattazzo.meteo.fragment.valanghe.rilevazioni.sezioni.ventoquotafenomeni.VentoQuotaFenomeniSezioneManager;
import com.gmail.fattazzo.meteo.fragment.valanghe.rilevazioni.sezioni.ventoquotatipo.VentoQuotaTipoSezioneManager;
import com.gmail.fattazzo.meteo.fragment.valanghe.rilevazioni.sezioni.visibilita.VisibilitaSezioneManager;

/**
 *
 * @author fattazzo
 *
 *         date: 03/feb/2016
 *
 */
public class RilevazioniViewManager {

    private SparseArray<AbstractSezioneManager> sezioni;

    private String codiceStazione = null;

    private List<DatiStazione> datiStazione;

    private View view;

    /**
     * Costruttore.
     *
     * @param rootView
     *            view da gestire
     */
    public RilevazioniViewManager(final View rootView) {
        super();
        this.view = rootView;

        sezioni = new SparseArray<>();
        sezioni.put(R.id.anag_stazione_neve_condMeteo_layout, new CondTempoSezioneManager(view));
        sezioni.put(R.id.anag_stazione_neve_nuvolosita_layout, new NuvolositaSezioneManager(view));
        sezioni.put(R.id.anag_stazione_neve_visibilita_layout, new VisibilitaSezioneManager(view));
        sezioni.put(R.id.anag_stazione_neve_vqt_layout, new VentoQuotaTipoSezioneManager(view));
        sezioni.put(R.id.anag_stazione_neve_vqf_layout, new VentoQuotaFenomeniSezioneManager(view));
        sezioni.put(R.id.anag_stazione_neve_stratosuperficiale_layout, new StratoSuperficialeSezioneManager(view));
        sezioni.put(R.id.anag_stazione_neve_rugositasuperficiale_layout, new RugositaSuperficialeSezioneManager(view));
        sezioni.put(R.id.anag_stazione_neve_brinasuperficie_layout, new BrinaSuperficieSezioneManager(view));
        sezioni.put(R.id.anag_stazione_neve_temperaturaaria_layout, new TemperaturaAriaSezioneManager(view));
        sezioni.put(R.id.anag_stazione_neve_temperaturaminmax_layout, new TemperaturaMaxMinSezioneManager(view));
        sezioni.put(R.id.anag_stazione_neve_altezzamantonevoso_layout, new AltezzaMantoNevosoSezioneManager(view));
        sezioni.put(R.id.anag_stazione_neve_altezzanevefresca_layout, new AltezzaNeveFrescaSezioneManager(view));
    }

    /**
     * Filtra i dati di tutte le stazioni estraendo quella corrente.
     *
     * @param datiiStazioni
     *            dati di tutte le stazioni
     */
    private void filtraDatiStazioni(List<DatiStazione> datiiStazioni) {
        datiStazione = new ArrayList<>();
        if (datiiStazioni != null) {
            for (DatiStazione dati : datiiStazioni) {
                if (dati.getCodiceStazione().equals(codiceStazione)) {
                    datiStazione.add(dati);
                }
            }
        }
    }

    /**
     * Registra il listener alle viste che espandono le sezioni.
     *
     * @param clickListener
     *            listener
     */
    public void registerOnCLickListener(OnClickListener clickListener) {

        for (int i = 0, nsize = sezioni.size(); i < nsize; i++) {
            AbstractSezioneManager sezione = sezioni.valueAt(i);

            View viewRegister = view.findViewById(sezione.getHeaderViewId());
            if (viewRegister != null) {
                viewRegister.setOnClickListener(clickListener);
            }
        }
    }

    /**
     * Setta la stazione corrente.
     *
     * @param stazione
     *            stazione corrente
     * @param datiiStazioni
     *            dati di tutte le stazioni
     */
    public void setStazioneCorrente(StazioneValanghe stazione, List<DatiStazione> datiiStazioni) {

        if (this.codiceStazione == null || !this.codiceStazione.equals(stazione.getCodice())) {
            this.codiceStazione = stazione.getCodice();

            filtraDatiStazioni(datiiStazioni);

            // pulisco tutti i dati delle sezioni
            for (int i = 0, nsize = sezioni.size(); i < nsize; i++) {
                AbstractSezioneManager sezione = sezioni.valueAt(i);
                sezione.setDatiStazione(datiStazione);
                sezione.clear();
            }
        }
    }

    /**
     * Visualizza o nasconde il contenuto della sezione gestita dalla risorsa passata come parametro.
     *
     * @param resId
     *            id risorsa
     */
    public void toggleContent(int resId) {

        AbstractSezioneManager sezione = sezioni.get(resId);
        if (sezione != null) {
            if(datiStazione == null || datiStazione.isEmpty()) {
                Toast toast = Toast.makeText(view.getContext(), "Nessun dato al momento disponibile per la stazione.", Toast.LENGTH_SHORT);
                toast.show();
            } else {
                sezione.toggleContent();
            }
        }
    }

}
