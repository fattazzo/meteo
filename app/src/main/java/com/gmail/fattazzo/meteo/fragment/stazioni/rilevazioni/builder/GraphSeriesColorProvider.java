package com.gmail.fattazzo.meteo.fragment.stazioni.rilevazioni.builder;

import android.graphics.Color;
import android.util.SparseIntArray;

import com.github.mikephil.charting.utils.ColorTemplate;

/**
 *
 * @author fattazzo
 *
 *         date: 28/lug/2014
 *
 */
final class GraphSeriesColorProvider {

    /**
     * Restituisce il colore per la serie richiesta.
     *
     * @param serieNumber
     *            numero della serie
     * @return colore corrispondente
     */
    static int getSeriesColor(int serieNumber) {
        if (serieNumber > COLORS.size()) {
            return COLORS.get(0);
        } else {
            return COLORS.get(serieNumber);
        }
    }

    private static final SparseIntArray COLORS;

    static {
        COLORS = new SparseIntArray(7);
        COLORS.append(0, ColorTemplate.MATERIAL_COLORS[3]);
        COLORS.append(1, ColorTemplate.MATERIAL_COLORS[2]);
        COLORS.append(2, ColorTemplate.MATERIAL_COLORS[1]);
        COLORS.append(3, ColorTemplate.MATERIAL_COLORS[0]);
        COLORS.append(4, Color.MAGENTA);
        COLORS.append(5, Color.GRAY);
        COLORS.append(6, Color.BLACK);
    }

    /**
     * Costruttore.
     */
    private GraphSeriesColorProvider() {

    }
}
