package com.gmail.fattazzo.meteo.domain.stazioni.dati;

import java.util.Date;

/**
 *
 * @author fattazzo
 *
 *         date: 28/lug/2014
 *
 */
public interface IDatoStazione {

    /**
     * @return data di riferimento
     */
    Date getData();

    /**
     * @return the unitaMisura
     */
    String getUnitaMisura();

    /**
     * @return valore di riferimento
     */
    double getValore();
}
