package com.gmail.fattazzo.meteo.fragment.webcam.component;

import android.content.Context;

import com.gmail.fattazzo.meteo.domain.webcam.WebcamLink;

/**
 *
 * @author fattazzo
 *
 *         date: 13/lug/2015
 *
 */
public class WebcamComponentViewerFactory {

    /**
     * Restituisce la vista da utilizzare per visualizzare la webcam.
     *
     * @param context
     *            context
     * @param webcamLink
     *            {@link WebcamLink}
     * @return {@link AbstractWebcamView}
     */
    public static AbstractWebcamView getView(WebcamLink webcamLink, Context context) {

        AbstractWebcamView resultView;
        switch (webcamLink.getViewerType()) {
        case IMAGE:
            resultView = new WebcamImageView(context, webcamLink.getLink());
            break;
        case WEBVIEW:
            resultView = new WebcamWebView(context, webcamLink.getLink());
            break;
        default:
            resultView = null;
            break;
        }

        return resultView;
    }

    /**
     * Costruttore.
     */
    private WebcamComponentViewerFactory() {
        super();
    }
}
