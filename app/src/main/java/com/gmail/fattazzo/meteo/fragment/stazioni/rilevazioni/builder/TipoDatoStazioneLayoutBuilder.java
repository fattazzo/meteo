package com.gmail.fattazzo.meteo.fragment.stazioni.rilevazioni.builder;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;

import android.content.Context;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.gmail.fattazzo.meteo.R;
import com.gmail.fattazzo.meteo.domain.stazioni.dati.DatiStazione;
import com.gmail.fattazzo.meteo.fragment.stazioni.rilevazioni.TipoDatoStazione;

/**
 *
 * @author fattazzo
 *
 *         date: 18/lug/2014
 *
 */
public abstract class TipoDatoStazioneLayoutBuilder {

    private RelativeLayout layout;

    private final DateFormat dateFormat;
    private final DateFormat dateShortFormat;
    private final DecimalFormat decimalFormat;

    /**
     * Costruttore.
     *
     */
    public TipoDatoStazioneLayoutBuilder() {
        super();
        this.dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss", Locale.ITALIAN);
        this.dateShortFormat = new SimpleDateFormat("dd/MM", Locale.ITALIAN);
        this.decimalFormat = new DecimalFormat("0.00");
    }

    /**
     * Costruisce la vista dei dati della stazione meteo.
     *
     * @param context
     *            context
     * @param datiStazione
     *            dati stazione
     * @param tipoDatoStazione
     *            tipo di dati da utilizzare
     * @return vista creata
     */
    public abstract View buildView(Context context, DatiStazione datiStazione, TipoDatoStazione tipoDatoStazione);

    /**
     * @return the dateFormat
     */
    public DateFormat getDateFormat() {
        return dateFormat;
    }

    /**
     * @return the dateShortFormat
     */
    public DateFormat getDateShortFormat() {
        return dateShortFormat;
    }

    /**
     * @return the decimalFormat
     */
    public DecimalFormat getDecimalFormat() {
        return decimalFormat;
    }

    /**
     * Layout associato al builder.
     *
     * @param context
     *            context
     * @return layout
     */
    public RelativeLayout getLayout(final Context context) {
        LayoutInflater inflater = LayoutInflater.from(context);
        layout = (RelativeLayout) inflater.inflate(getLayoutResourceId(), (ViewGroup) null, false);

        return layout;
    }

    /**
     * @return id della risorsa del layout
     */
    public abstract Integer getLayoutResourceId();

    /**
     * Visualizza il testo per informare dei dati assenti della stazione.
     *
     * @param context
     *            context
     */
    public void showEmptyDataText(Context context) {
        layout.removeAllViews();

        TextView textView = new TextView(context);
        textView.setText(Html.fromHtml(context.getString(R.string.station_values_not_available)));
        textView.setMovementMethod(LinkMovementMethod.getInstance());
        layout.addView(textView);
    }

}
