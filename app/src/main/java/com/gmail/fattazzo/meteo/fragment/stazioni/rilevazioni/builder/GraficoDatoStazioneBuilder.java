package com.gmail.fattazzo.meteo.fragment.stazioni.rilevazioni.builder;

import android.content.Context;
import android.view.View;
import android.widget.RelativeLayout;

import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.gmail.fattazzo.meteo.R;
import com.gmail.fattazzo.meteo.domain.stazioni.dati.DatiStazione;
import com.gmail.fattazzo.meteo.domain.stazioni.dati.IDatoStazione;
import com.gmail.fattazzo.meteo.fragment.stazioni.rilevazioni.TipoDatoStazione;
import com.gmail.fattazzo.meteo.settings.StazioniMeteoSettingsManager;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author fattazzo
 *
 *         date: 28/lug/2014
 *
 */
public class GraficoDatoStazioneBuilder extends TipoDatoStazioneLayoutBuilder {

    /**
     * Costruttore.
     *
     */
    public GraficoDatoStazioneBuilder() {
        super();
    }

    @Override
    public View buildView(Context context, DatiStazione datiStazione, TipoDatoStazione tipoDatoStazione) {
        RelativeLayout layout = getLayout(context);
        layout.removeAllViews();

        if (datiStazione == null) {
            showEmptyDataText(context);
        } else {


            List<IDatoStazione> datiTipo = new ArrayList<>();
            switch (tipoDatoStazione) {
            case PRECIPITAZIONE:
                datiTipo.addAll(datiStazione.getPrecipitazioni());
                break;
            case TEMPERATURA:
                datiTipo.addAll(datiStazione.getTemperature());
                break;
            default:
                break;
            }

            View chartView = getGraphView(context,datiTipo);

            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
            layout.addView(chartView,layoutParams);
        }

        return layout;
    }

    private View getGraphView(Context context,List<IDatoStazione> datiTipo) {
        StazioniMeteoSettingsManager settingsManager = new StazioniMeteoSettingsManager(context);

        LineChart lineChart = new LineChart(context);

        Map<String,List<Entry>> entries = new HashMap<>();

        Calendar calendar = Calendar.getInstance();
        Calendar refTime = Calendar.getInstance();

        String currentDay;
        for (IDatoStazione dato : datiTipo) {
            calendar.setTime(dato.getData());

            refTime.set(Calendar.HOUR_OF_DAY, calendar.get(Calendar.HOUR_OF_DAY));
            refTime.set(Calendar.MINUTE, calendar.get(Calendar.MINUTE));
            refTime.set(Calendar.SECOND, calendar.get(Calendar.SECOND));

            Entry entry = new Entry(Long.valueOf(refTime.getTimeInMillis()).floatValue(), Double.valueOf(dato.getValore()).floatValue());
            currentDay = calendar.get(Calendar.DAY_OF_MONTH) + "/" + (calendar.get(Calendar.MONTH)+1);

            List<Entry> entryDay = entries.get(currentDay);
            if(entryDay == null) {
                entryDay = new ArrayList<>();
            }
            entryDay.add(entry);
            entries.put(currentDay,entryDay);
        }

        int serieNumber = 0;
        List<ILineDataSet> dataSetList = new ArrayList<>();
        for (Map.Entry<String,List<Entry>> entry: entries.entrySet()) {
            LineDataSet dataSet = new LineDataSet(entry.getValue(),entry.getKey());
            dataSet.setColor(GraphSeriesColorProvider.getSeriesColor(serieNumber));
            dataSet.setLineWidth(4f);
            dataSet.setDrawFilled(settingsManager.showGraphSeriesBackground());
            dataSet.setFillColor(GraphSeriesColorProvider.getSeriesColor(serieNumber));
            dataSet.setDrawCircles(settingsManager.showGraphPoint());
            dataSet.setCircleColor(GraphSeriesColorProvider.getSeriesColor(serieNumber));
            dataSet.setCircleRadius(settingsManager.getGraphPointRadius());

            dataSetList.add(dataSet);
            serieNumber++;
        }

        Legend l = lineChart.getLegend();
        l.setFormSize(10f);
        l.setForm(Legend.LegendForm.CIRCLE);

        lineChart.setTouchEnabled(false);
        lineChart.setDragEnabled(false);
        lineChart.setScaleEnabled(false);
        lineChart.setPinchZoom(false);
        lineChart.setDoubleTapToZoomEnabled(false);

        XAxis xAxis = lineChart.getXAxis();
        xAxis.setValueFormatter(new DateXAxisValueFormatter());
        xAxis.setGranularity(3f);

        Description charDescription = new Description();
        charDescription.setText("");
        lineChart.setDescription(charDescription);

        LineData data = new LineData(dataSetList);
        lineChart.setData(data);
        lineChart.invalidate();
        lineChart.animateX(1500,Easing.EasingOption.EaseOutSine);

        return lineChart;
    }

    @Override
    public Integer getLayoutResourceId() {
        return R.layout.dato_stazione_meteo_grafico;
    }

}
