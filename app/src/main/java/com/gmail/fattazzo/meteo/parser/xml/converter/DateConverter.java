package com.gmail.fattazzo.meteo.parser.xml.converter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;

/**
 *
 * @author fattazzo
 *
 *         date: 13/lug/2015
 *
 */
public class DateConverter implements Converter {

    private final String dateFormat;

    /**
     * Costruttore.
     *
     * @param format
     *            formato della data
     */
    public DateConverter(final String format) {
        super();
        this.dateFormat = format;
    }

    @Override
    public boolean canConvert(@SuppressWarnings("rawtypes") Class clazz) {
        return clazz.equals(Date.class);
    }

    @Override
    public void marshal(Object value, HierarchicalStreamWriter writer, MarshallingContext context) {
        Date data = (Date) value;
        writer.startNode("data");
        writer.setValue(data.toString());
        writer.endNode();
    }

    @Override
    public Object unmarshal(HierarchicalStreamReader reader, UnmarshallingContext context) {
        String dateStr = reader.getValue();

        Date date;
        try {
            date = new SimpleDateFormat(dateFormat, Locale.ENGLISH).parse(dateStr);
        } catch (ParseException e) {
            date = null;
        }

        return date;
    }

}
