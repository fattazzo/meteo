package com.gmail.fattazzo.meteo.domain.webcam;

import java.io.Serializable;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

/**
 *
 * @author fattazzo
 *
 *         date: 13/lug/2015
 *
 */
@XStreamAlias("webcam")
public class Webcam implements Serializable, Comparable<Webcam> {

    private static final long serialVersionUID = 2716843895314981689L;

    @XStreamAlias("id")
    @XStreamAsAttribute
    private int id;

    @XStreamAlias("description")
    @XStreamAsAttribute
    private String descrizione;

    @XStreamImplicit(itemFieldName = "link")
    private List<WebcamLink> links;

    @XStreamAlias("website")
    private String website;

    private boolean showInWidget;

    {
        showInWidget = false;
    }

    @Override
    public int compareTo(Webcam another) {

        if (this == another) {
            return 0;
        }
        if (another == null || another.getDescrizione() == null) {
            return 1;
        }
        if (descrizione == null) {
            return -1;
        } else {
            return descrizione.compareTo(another.getDescrizione());
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Webcam other = (Webcam) obj;
        return id == other.id;
    }

    /**
     * @return the descrizione
     */
    public String getDescrizione() {
        return descrizione;
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @return the links
     */
    public List<WebcamLink> getLinks() {
        return links;
    }

    /**
     * @return the website
     */
    public String getWebsite() {
        return website;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + id;
        return result;
    }

    /**
     * @return the showInWidget
     */
    public boolean isShowInWidget() {
        return showInWidget;
    }

    /**
     * @param descrizione
     *            the descrizione to set
     */
    public void setDescrizione(String descrizione) {
        this.descrizione = descrizione;
    }

    /**
     * @param id
     *            the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @param links
     *            the links to set
     */
    public void setLinks(List<WebcamLink> links) {
        this.links = links;
    }

    /**
     * @param showInWidget
     *            the showInWidget to set
     */
    public void setShowInWidget(boolean showInWidget) {
        this.showInWidget = showInWidget;
    }

    /**
     * @param website
     *            the website to set
     */
    public void setWebsite(String website) {
        this.website = website;
    }

    /*
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "Webcam [descrizione=" + descrizione + "]";
    }

}
