package com.gmail.fattazzo.meteo.fragment.webcam.component;

import java.io.InputStream;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.gmail.fattazzo.meteo.utils.view.ZoomableImageView;

/**
 * @author fattazzo
 *         <p/>
 *         date: 13/lug/2015
 */
public class WebcamImageView extends AbstractWebcamView {

    /**
     * @author fattazzo
     *         <p/>
     *         date: 13/lug/2015
     */
    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {

        /**
         * Costruttore.
         */
        public DownloadImageTask() {
        }

        @Override
        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return mIcon11;
        }

        @Override
        protected void onPostExecute(Bitmap result) {
            getProgressBar().setVisibility(View.GONE);
            removeView(imageView);
            imageView = new ZoomableImageView(getContext());
            imageView.setDrawingCacheEnabled(false);
            imageView.setLayoutParams(new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT));
            imageView.setImageBitmap(result);
            addView(imageView, 0);

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            getProgressBar().setVisibility(View.VISIBLE);
            getProgressBar().animate();
        }
    }

    private ZoomableImageView imageView;

    /**
     * Costruttore.
     *
     * @param context
     *            context
     * @param urlToLoadParam
     *            url da caricare
     */
    public WebcamImageView(final Context context, final String urlToLoadParam) {
        super(urlToLoadParam, context);
    }

    @Override
    public View getWebcamView() {

        imageView = new ZoomableImageView(getContext());
        imageView.setLayoutParams(new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT));

        return imageView;
    }

    @Override
    public void loadWebcam() {

        new DownloadImageTask().execute(urlToLoad);
    }

}
