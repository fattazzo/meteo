package com.gmail.fattazzo.meteo.fragment.stazioni;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.FragmentTabHost;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TabHost;

import com.gmail.fattazzo.meteo.R;
import com.gmail.fattazzo.meteo.fragment.BaseFragment;
import com.gmail.fattazzo.meteo.fragment.stazioni.anagrafica.AnagraficaFragment;
import com.gmail.fattazzo.meteo.fragment.stazioni.rilevazioni.RilevazioniFragment;

/**
 * @author fattazzo
 *         <p>
 *         date: 17/lug/2014
 */
public class StazioniFragment extends BaseFragment {

    private View rootView;

    private FragmentTabHost mTabHost;

    private Activity activity;

    /**
     * Inizializzazione del tab per la visualizzazione delle sezioni riguardanti le stazioni meteo.
     */
    private void initTabHost() {
        mTabHost = (FragmentTabHost) rootView.findViewById(android.R.id.tabhost);
        mTabHost.setup(activity, getChildFragmentManager(), R.id.realtabcontent);

        String[] stringArray = rootView.getResources().getStringArray(R.array.stazioni_sezioni);

        mTabHost.addTab(mTabHost.newTabSpec(stringArray[0]).setIndicator(stringArray[0]), RilevazioniFragment.class,
                null);
        mTabHost.addTab(mTabHost.newTabSpec(stringArray[1]).setIndicator(stringArray[1]), AnagraficaFragment.class,
                null);

        mTabHost.setOnTabChangedListener(new TabHost.OnTabChangeListener() {

            @Override
            public void onTabChanged(String tabId) {
                mTabHost.getCurrentTabView().refreshDrawableState();

            }
        });
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (activity == null && getActivity() != null) {
            activity = getActivity();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_stazioni, container, false);

        initTabHost();

        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mTabHost = null;
    }

    @Override
    public int getTitleResId() {
        return R.string.nav_stazioni_meteo;
    }
}
