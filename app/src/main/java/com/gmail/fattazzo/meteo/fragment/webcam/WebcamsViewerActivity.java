package com.gmail.fattazzo.meteo.fragment.webcam;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.support.v13.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.Menu;

import com.gmail.fattazzo.meteo.R;
import com.gmail.fattazzo.meteo.domain.webcam.Webcam;

/**
 *
 * @author fattazzo
 *
 *         date: 13/lug/2015
 *
 */
public class WebcamsViewerActivity extends Activity implements OnPageChangeListener {

    /**
     *
     * @author fattazzo
     *
     *         date: 13/lug/2015
     *
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        /**
         * Costruttore.
         *
         * @param fm
         *            {@link FragmentManager}
         */
        public SectionsPagerAdapter(final FragmentManager fm) {
            super(fm);
        }

        @Override
        public int getCount() {
            return webcams.size();
        }

        @Override
        public Fragment getItem(int position) {
            return WebcamViewerFragment.newInstance(webcams, position);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return webcams.get(position).getDescrizione();
        }
    }

    SectionsPagerAdapter mSectionsPagerAdapter;

    public List<Webcam> webcams;

    public static final String EXTRA_WEBCAMS = "extraWebcams";

    ViewPager mViewPager;

    @SuppressWarnings("unchecked")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_webcams_viewer);

        webcams = new ArrayList<>();

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            webcams = (ArrayList<Webcam>) extras.getSerializable(EXTRA_WEBCAMS);
        }

        mSectionsPagerAdapter = new SectionsPagerAdapter(getFragmentManager());

        mViewPager = (ViewPager) findViewById(R.id.pager);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        mViewPager.setOnPageChangeListener(this);

        this.setTitle(mSectionsPagerAdapter.getPageTitle(0));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return false;
    }

    @Override
    public void onPageScrolled(int arg0, float arg1, int arg2) {
    }

    @Override
    public void onPageScrollStateChanged(int arg0) {
    }

    @Override
    public void onPageSelected(int arg0) {
        this.setTitle(mSectionsPagerAdapter.getPageTitle(arg0));
    }

}
