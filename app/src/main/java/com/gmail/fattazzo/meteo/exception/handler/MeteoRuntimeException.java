package com.gmail.fattazzo.meteo.exception.handler;

/**
 *
 * @author fattazzo
 *
 *         date: 12/giu/2014
 *
 */
public class MeteoRuntimeException extends RuntimeException {

    private static final long serialVersionUID = -3777218762708815947L;

    /**
     * Costruttore.
     *
     * @param detailMessage
     *            messaggio
     */
    public MeteoRuntimeException(final String detailMessage) {
        super(detailMessage);
    }

    /**
     * Costruttore.
     *
     * @param detailMessage
     *            messaggio
     * @param throwable
     *            eccezione di riferimento
     */
    public MeteoRuntimeException(final String detailMessage, final Throwable throwable) {
        super(detailMessage, throwable);
    }

    /**
     * Costruttore.
     *
     * @param throwable
     *            eccezione di riferimento
     */
    public MeteoRuntimeException(final Throwable throwable) {
        super(throwable);
    }

}
