package com.gmail.fattazzo.meteo.fragment.valanghe.anagrafica;

import java.util.List;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.gmail.fattazzo.meteo.R;
import com.gmail.fattazzo.meteo.domain.valanghe.anagrafica.StazioneValanghe;

/**
 *
 * @author fattazzo
 *
 *         date: 01/feb/2016
 *
 */
public class AnagraficaStazioneValangheListAdapter extends ArrayAdapter<StazioneValanghe> {

    /**
     *
     * @author fattazzo
     *
     *         date: 01/feb/2016
     *
     */
    static class ViewHolderItem {
        protected TextView nomeTextView;
        protected TextView codiceTextView;
        protected TextView quotaTextView;
    }

    private LayoutInflater layoutInflater = null;

    /**
     * Costruttore.
     *
     * @param context
     *            context
     * @param objects
     *            stazioni valanghe
     */
    public AnagraficaStazioneValangheListAdapter(final Context context, final List<StazioneValanghe> objects) {
        super(context, R.layout.anag_stazione_valanghe_list_adapter, objects);
        this.layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolderItem viewHolder;
        View v = convertView;

        if (v == null) {
            v = layoutInflater.inflate(R.layout.anag_stazione_valanghe_list_adapter, (ViewGroup) null);

            viewHolder = new ViewHolderItem();
            viewHolder.nomeTextView = (TextView) v.findViewById(R.id.anag_stazione_valanghe_nome_label);
            viewHolder.codiceTextView = (TextView) v.findViewById(R.id.anag_stazione_valanghe_codice_label);
            viewHolder.quotaTextView = (TextView) v.findViewById(R.id.anag_stazione_valanghe_quota_label);
            v.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolderItem) v.getTag();
        }

        StazioneValanghe stazioneValanghe = getItem(position);

        viewHolder.nomeTextView.setText(stazioneValanghe.getNome());
        viewHolder.codiceTextView.setText(stazioneValanghe.getCodice());
        viewHolder.quotaTextView.setText(String.valueOf(stazioneValanghe.getQuota()));

        if (position % 2 == 1) {
            v.setBackgroundColor(Color.WHITE);
        } else {
            v.setBackgroundColor(Color.LTGRAY);
        }

        return v;
    }

}
