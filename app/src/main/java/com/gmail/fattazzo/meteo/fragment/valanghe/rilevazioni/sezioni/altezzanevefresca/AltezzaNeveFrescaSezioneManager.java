package com.gmail.fattazzo.meteo.fragment.valanghe.rilevazioni.sezioni.altezzanevefresca;

import android.view.View;

import com.gmail.fattazzo.meteo.R;
import com.gmail.fattazzo.meteo.domain.valanghe.dati.DatiStazione;
import com.gmail.fattazzo.meteo.fragment.valanghe.rilevazioni.sezioni.RilevazioneSezione;
import com.gmail.fattazzo.meteo.fragment.valanghe.rilevazioni.sezioni.RilevazioneSezioneListAdapter;
import com.gmail.fattazzo.meteo.fragment.valanghe.rilevazioni.view.AbstractSezioneManager;
import com.gmail.fattazzo.meteo.utils.ExpandableHeightListView;

import java.util.ArrayList;
import java.util.List;

/**
 * @author fattazzo
 *         <p/>
 *         date: 03/feb/2016
 */
public class AltezzaNeveFrescaSezioneManager extends AbstractSezioneManager {

    /**
     * Costruttore.
     *
     * @param view view
     */
    public AltezzaNeveFrescaSezioneManager(final View view) {
        super(view);
    }

    @Override
    protected void clearContent() {
        ExpandableHeightListView condTempoList = (ExpandableHeightListView) view
                .findViewById(R.id.neve_altezzanevefresca_list);
        condTempoList.setAdapter(null);
    }

    @Override
    protected int getHeaderViewId() {
        return R.id.anag_stazione_neve_altezzanevefresca_layout;
    }

    @Override
    protected int getToggleViewId() {
        return R.id.anag_stazione_neve_altezzanevefrescaData_layout;
    }

    @Override
    protected void loadContent() {
        List<RilevazioneSezione> altezzaNeveFrescaList = new ArrayList<>();
        for (DatiStazione dato : getDatiStazione()) {
            RilevazioneSezione altezzaNeveFresca = new RilevazioneSezione();
            altezzaNeveFresca.setData(dato.getDataMisurazione());
            String descrizione = getLegendaDatiNeveManager().parseAltezzaNeveFresca(dato.getAltezzaNeveFresca());
            altezzaNeveFresca.setDescrizione(descrizione);
            altezzaNeveFrescaList.add(altezzaNeveFresca);
        }

        RilevazioneSezioneListAdapter adapter = new RilevazioneSezioneListAdapter(view.getContext(), altezzaNeveFrescaList);

        ExpandableHeightListView listView = (ExpandableHeightListView) view.findViewById(R.id.neve_altezzanevefresca_list);
        listView.setAdapter(adapter);
        listView.setExpanded(true);

    }
}
