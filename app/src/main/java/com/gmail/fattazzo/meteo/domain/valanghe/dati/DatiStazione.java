package com.gmail.fattazzo.meteo.domain.valanghe.dati;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 *
 * @author fattazzo
 *
 *         date: 01/feb/2016
 *
 */
public class DatiStazione {

    public static final String XML_BEAN_NAME = "rilievo_neve";
    public static final List<String> XMLPROP = new ArrayList<>(Arrays.asList(new String[] { "dataMis", "codStaz",
            "ww", "n", "v", "vq1", "vq2", "ta", "tmin", "tmax", "hs", "hn", "fi", "t10", "t30", "pr", "cs", "s", "b" }));

    private static final DateFormat DATA_FORMAT = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss", Locale.getDefault());

    // "dataMis"
    private Date dataMisurazione;

    // "codStaz"
    private String codiceStazione;

    // "ww"
    private String condizioniTempo;

    // "n"
    private String nuvolosita;

    // "v"
    private String visibilita;

    // "vq1"
    private String ventoQuotaTipo;

    // "vq2"
    private String ventoQuotaLocalizzazione;

    // "ta"
    private String temperaturaAria;

    // "tmin"
    private String temperaturaMinima;

    // "tmax"
    private String temperaturaMassima;

    // "hs"
    private String altezzaNeve;

    // "hn"
    private String altezzaNeveFresca;

    // "fi"
    private String densitaNeve;

    // "t10"
    private String temperaturaNeve10;

    // "t30"
    private String temperaturaNeve30;

    // "pr"
    private String penetrazioneSonda;

    // "cs"
    private String stratoSuperficiale;

    // "s"
    private String rugositaSuperficiale;

    // "b"
    private String brinaSuperficie;

    /**
     * @return the altezzaNeve
     */
    public String getAltezzaNeve() {
        return altezzaNeve;
    }

    /**
     * @return the altezzaNeveFresca
     */
    public String getAltezzaNeveFresca() {
        return altezzaNeveFresca;
    }

    /**
     * @return the brinaSuperficie
     */
    public String getBrinaSuperficie() {
        return brinaSuperficie;
    }

    /**
     * @return the codiceStazione
     */
    public String getCodiceStazione() {
        return codiceStazione;
    }

    /**
     * @return the condizioniTempo
     */
    public String getCondizioniTempo() {
        return condizioniTempo;
    }

    /**
     * @return the dataMisurazione
     */
    public Date getDataMisurazione() {
        return dataMisurazione;
    }

    /**
     * @return the densitaNeve
     */
    public String getDensitaNeve() {
        return densitaNeve;
    }

    /**
     * @return the nuvolosita
     */
    public String getNuvolosita() {
        return nuvolosita;
    }

    /**
     * @return the penetrazioneSonda
     */
    public String getPenetrazioneSonda() {
        return penetrazioneSonda;
    }

    /**
     * @return the rugositaSuperficiale
     */
    public String getRugositaSuperficiale() {
        return rugositaSuperficiale;
    }

    /**
     * @return the stratoSuperficiale
     */
    public String getStratoSuperficiale() {
        return stratoSuperficiale;
    }

    /**
     * @return the temperaturaAria
     */
    public String getTemperaturaAria() {
        return temperaturaAria;
    }

    /**
     * @return the temperaturaMassima
     */
    public String getTemperaturaMassima() {
        return temperaturaMassima;
    }

    /**
     * @return the temperaturaMinima
     */
    public String getTemperaturaMinima() {
        return temperaturaMinima;
    }

    /**
     * @return the temperaturaNeve10
     */
    public String getTemperaturaNeve10() {
        return temperaturaNeve10;
    }

    /**
     * @return the temperaturaNeve30
     */
    public String getTemperaturaNeve30() {
        return temperaturaNeve30;
    }

    /**
     * @return the ventoQuotaLocalizzazione
     */
    public String getVentoQuotaLocalizzazione() {
        return ventoQuotaLocalizzazione;
    }

    /**
     * @return the ventoQuotaTipo
     */
    public String getVentoQuotaTipo() {
        return ventoQuotaTipo;
    }

    /**
     * @return the visibilita
     */
    public String getVisibilita() {
        return visibilita;
    }

    /**
     * @param altezzaNeve
     *            the altezzaNeve to set
     */
    public void setAltezzaNeve(String altezzaNeve) {
        this.altezzaNeve = altezzaNeve;
    }

    /**
     * @param altezzaNeveFresca
     *            the altezzaNeveFresca to set
     */
    public void setAltezzaNeveFresca(String altezzaNeveFresca) {
        this.altezzaNeveFresca = altezzaNeveFresca;
    }

    /**
     * @param brinaSuperficie
     *            the brinaSuperficie to set
     */
    public void setBrinaSuperficie(String brinaSuperficie) {
        this.brinaSuperficie = brinaSuperficie;
    }

    /**
     * @param codiceStazione
     *            the codiceStazione to set
     */
    public void setCodiceStazione(String codiceStazione) {
        this.codiceStazione = codiceStazione;
    }

    /**
     * @param condizioniTempo
     *            the condizioniTempo to set
     */
    public void setCondizioniTempo(String condizioniTempo) {
        this.condizioniTempo = condizioniTempo;
    }

    /**
     * @param dataMisurazione
     *            the dataMisurazione to set
     */
    public void setDataMisurazione(Date dataMisurazione) {
        this.dataMisurazione = dataMisurazione;
    }

    /**
     * @param densitaNeve
     *            the densitaNeve to set
     */
    public void setDensitaNeve(String densitaNeve) {
        this.densitaNeve = densitaNeve;
    }

    /**
     * @param nuvolosita
     *            the nuvolosita to set
     */
    public void setNuvolosita(String nuvolosita) {
        this.nuvolosita = nuvolosita;
    }

    /**
     * @param penetrazioneSonda
     *            the penetrazioneSonda to set
     */
    public void setPenetrazioneSonda(String penetrazioneSonda) {
        this.penetrazioneSonda = penetrazioneSonda;
    }

    /**
     * @param rugositaSuperficiale
     *            the rugositaSuperficiale to set
     */
    public void setRugositaSuperficiale(String rugositaSuperficiale) {
        this.rugositaSuperficiale = rugositaSuperficiale;
    }

    /**
     * @param stratoSuperficiale
     *            the stratoSuperficiale to set
     */
    public void setStratoSuperficiale(String stratoSuperficiale) {
        this.stratoSuperficiale = stratoSuperficiale;
    }

    /**
     * @param temperaturaAria
     *            the temperaturaAria to set
     */
    public void setTemperaturaAria(String temperaturaAria) {
        this.temperaturaAria = temperaturaAria;
    }

    /**
     * @param temperaturaMassima
     *            the temperaturaMassima to set
     */
    public void setTemperaturaMassima(String temperaturaMassima) {
        this.temperaturaMassima = temperaturaMassima;
    }

    /**
     * @param temperaturaMinima
     *            the temperaturaMinima to set
     */
    public void setTemperaturaMinima(String temperaturaMinima) {
        this.temperaturaMinima = temperaturaMinima;
    }

    /**
     * @param temperaturaNeve10
     *            the temperaturaNeve10 to set
     */
    public void setTemperaturaNeve10(String temperaturaNeve10) {
        this.temperaturaNeve10 = temperaturaNeve10;
    }

    /**
     * @param temperaturaNeve30
     *            the temperaturaNeve30 to set
     */
    public void setTemperaturaNeve30(String temperaturaNeve30) {
        this.temperaturaNeve30 = temperaturaNeve30;
    }

    /**
     * Setta il valore della proprietà dell'xml.
     *
     * @param propName
     *            nome proprietà
     * @param propValue
     *            valore proprietà
     */
    public void setValue(String propName, String propValue) {
        String value = propValue.replace("\n", "").trim();
        if (propName.equalsIgnoreCase("dataMis")) {
            try {
                setDataMisurazione(DATA_FORMAT.parse(value));
            } catch (Exception e) {
                setDataMisurazione(null);
            }
        } else if (propName.equalsIgnoreCase("codStaz")) {
            setCodiceStazione(value);
        } else if (propName.equalsIgnoreCase("ww")) {
            setCondizioniTempo(value);
        } else if (propName.equalsIgnoreCase("n")) {
            setNuvolosita(value);
        } else if (propName.equalsIgnoreCase("v")) {
            setVisibilita(value);
        } else if (propName.equalsIgnoreCase("vq1")) {
            setVentoQuotaTipo(value);
        } else if (propName.equalsIgnoreCase("vq2")) {
            setVentoQuotaLocalizzazione(value);
        } else if (propName.equalsIgnoreCase("ta")) {
            setTemperaturaAria(value);
        } else if (propName.equalsIgnoreCase("tmin")) {
            setTemperaturaMinima(value);
        } else if (propName.equalsIgnoreCase("tmax")) {
            setTemperaturaMassima(value);
        } else if (propName.equalsIgnoreCase("hs")) {
            setAltezzaNeve(value);
        } else if (propName.equalsIgnoreCase("hn")) {
            setAltezzaNeveFresca(value);
        } else if (propName.equalsIgnoreCase("fi")) {
            setDensitaNeve(value);
        } else if (propName.equalsIgnoreCase("t10")) {
            setTemperaturaNeve10(value);
        } else if (propName.equalsIgnoreCase("t30")) {
            setTemperaturaNeve30(value);
        } else if (propName.equalsIgnoreCase("pr")) {
            setPenetrazioneSonda(value);
        } else if (propName.equalsIgnoreCase("cs")) {
            setStratoSuperficiale(value);
        } else if (propName.equalsIgnoreCase("s")) {
            setRugositaSuperficiale(value);
        } else if (propName.equalsIgnoreCase("b")) {
            setBrinaSuperficie(value);
        }
    }

    /**
     * @param ventoQuotaLocalizzazione
     *            the ventoQuotaLocalizzazione to set
     */
    public void setVentoQuotaLocalizzazione(String ventoQuotaLocalizzazione) {
        this.ventoQuotaLocalizzazione = ventoQuotaLocalizzazione;
    }

    /**
     * @param ventoQuotaTipo
     *            the ventoQuotaTipo to set
     */
    public void setVentoQuotaTipo(String ventoQuotaTipo) {
        this.ventoQuotaTipo = ventoQuotaTipo;
    }

    /**
     * @param visibilita
     *            the visibilita to set
     */
    public void setVisibilita(String visibilita) {
        this.visibilita = visibilita;
    }
}
