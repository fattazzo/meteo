package com.gmail.fattazzo.meteo.domain.stazioni.dati;

import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 *
 * @author fattazzo
 *
 *         date: 13/lug/2015
 *
 */
public class DatiStazione {

    private String codiceStazione;

    @XStreamAlias("data")
    private Date data;

    @XStreamAlias("tmin")
    private double temperaturaMinima;

    @XStreamAlias("tmax")
    private double temperaturaMassima;

    @XStreamAlias("rain")
    private double pioggia;

    @XStreamAlias("temperature")
    private List<TemperaturaAriaStazione> temperature;

    @XStreamAlias("precipitazioni")
    private List<PrecipitazioneStazione> precipitazioni;

    /**
     * @return the codiceStazione
     */
    public String getCodiceStazione() {
        return codiceStazione;
    }

    /**
     * @return the data
     */
    public Date getData() {
        return data;
    }

    /**
     * @return the pioggia
     */
    public double getPioggia() {
        return pioggia;
    }

    /**
     * @return the precipitazioni
     */
    public List<PrecipitazioneStazione> getPrecipitazioni() {
        Collections.sort(precipitazioni, new Comparator<PrecipitazioneStazione>() {

            @Override
            public int compare(PrecipitazioneStazione lhs, PrecipitazioneStazione rhs) {
                return lhs.getData().compareTo(rhs.getData());
            }
        });
        return precipitazioni;
    }

    /**
     * @return the temperaturaMassima
     */
    public double getTemperaturaMassima() {
        return temperaturaMassima;
    }

    /**
     * @return the temperaturaMinima
     */
    public double getTemperaturaMinima() {
        return temperaturaMinima;
    }

    /**
     * @return the temperature
     */
    public List<TemperaturaAriaStazione> getTemperature() {
        Collections.sort(temperature, new Comparator<TemperaturaAriaStazione>() {

            @Override
            public int compare(TemperaturaAriaStazione lhs, TemperaturaAriaStazione rhs) {
                return lhs.getData().compareTo(rhs.getData());
            }
        });
        return temperature;
    }

    /**
     * @param codiceStazione
     *            the codiceStazione to set
     */
    public void setCodiceStazione(String codiceStazione) {
        this.codiceStazione = codiceStazione;
    }

    /**
     * @param data
     *            the data to set
     */
    public void setData(Date data) {
        this.data = data;
    }

    /**
     * @param pioggia
     *            the pioggia to set
     */
    public void setPioggia(double pioggia) {
        this.pioggia = pioggia;
    }

    /**
     * @param precipitazioni
     *            the precipitazioni to set
     */
    public void setPrecipitazioni(List<PrecipitazioneStazione> precipitazioni) {
        this.precipitazioni = precipitazioni;
    }

    /**
     * @param temperaturaMassima
     *            the temperaturaMassima to set
     */
    public void setTemperaturaMassima(double temperaturaMassima) {
        this.temperaturaMassima = temperaturaMassima;
    }

    /**
     * @param temperaturaMinima
     *            the temperaturaMinima to set
     */
    public void setTemperaturaMinima(double temperaturaMinima) {
        this.temperaturaMinima = temperaturaMinima;
    }

    /**
     * @param temperature
     *            the temperature to set
     */
    public void setTemperature(List<TemperaturaAriaStazione> temperature) {
        this.temperature = temperature;
    }

}
