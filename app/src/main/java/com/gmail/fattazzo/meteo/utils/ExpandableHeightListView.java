package com.gmail.fattazzo.meteo.utils;

import android.content.Context;
import android.util.AttributeSet;
import android.view.ViewGroup;
import android.widget.ListView;

/**
 *
 * @author fattazzo
 *
 *         date: 13/lug/2015
 *
 */
public class ExpandableHeightListView extends ListView {

    private boolean expanded = false;

    /**
     * Costructor.
     *
     * @param context
     *            context
     */
    public ExpandableHeightListView(final Context context) {
        super(context);
    }

    /**
     * Costructor.
     *
     * @param context
     *            context
     * @param attrs
     *            attributes
     */
    public ExpandableHeightListView(final Context context, final AttributeSet attrs) {
        super(context, attrs);
    }

    /**
     * Costructor.
     *
     * @param context
     *            context
     * @param attrs
     *            attributes
     * @param defStyle
     *            style
     */
    public ExpandableHeightListView(final Context context, final AttributeSet attrs, final int defStyle) {
        super(context, attrs, defStyle);
    }

    /**
     * @return <code>true</code> if expandable
     */
    public boolean isExpanded() {
        return expanded;
    }

    @Override
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        // HACK! TAKE THAT ANDROID!
        if (isExpanded()) {
            // Calculate entire height by providing a very large height hint.
            // But do not use the highest 2 bits of this integer; those are
            // reserved for the MeasureSpec mode.
            int expandSpec = MeasureSpec.makeMeasureSpec(Integer.MAX_VALUE >> 2, MeasureSpec.AT_MOST);
            super.onMeasure(widthMeasureSpec, expandSpec);

            ViewGroup.LayoutParams params = getLayoutParams();
            params.height = getMeasuredHeight();
        } else {
            super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        }
    }

    /**
     * @param expanded
     *            set expanded
     */
    public void setExpanded(boolean expanded) {
        this.expanded = expanded;
    }
}
