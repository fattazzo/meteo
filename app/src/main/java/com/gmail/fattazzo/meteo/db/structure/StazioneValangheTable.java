package com.gmail.fattazzo.meteo.db.structure;

import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;

import com.gmail.fattazzo.meteo.domain.valanghe.anagrafica.StazioneValanghe;

/**
 *
 * @author fattazzo
 *
 *         date: 01/feb/2016
 *
 */
public class StazioneValangheTable implements TableStructure<StazioneValanghe> {

    public static final String CODICE = "codice";
    public static final String NOME = "nome";
    public static final String NOME_BREVE = "nomeBreve";
    public static final String QUOTA = "quota";
    public static final String LATITUDINE = "latitudine";
    public static final String LONGITUDINE = "longitudine";

    private static final String[] COLUMNS = new String[] { CODICE, NOME, NOME_BREVE, QUOTA, LATITUDINE, LONGITUDINE };

    public static final Uri URI = Uri.parse("sqlite://com.gmail.fattazzo.meteo/table/stazioniValanghe");

    @Override
    public ContentValues createContentValues(final StazioneValanghe stazioneValanghe) {
        ContentValues values = new ContentValues();
        values.put(CODICE, stazioneValanghe.getCodice());
        values.put(NOME, stazioneValanghe.getNome());
        values.put(NOME_BREVE, stazioneValanghe.getNomeBreve());
        values.put(QUOTA, stazioneValanghe.getQuota());
        values.put(LATITUDINE, stazioneValanghe.getLatitudine());
        values.put(LONGITUDINE, stazioneValanghe.getLongitudine());

        return values;
    }

    @Override
    public String[] getColumns() {
        return COLUMNS;
    }

    @Override
    public String getCreateTableSQL() {
        return "CREATE TABLE " + getTableName() + "(" + CODICE + " INTEGER TEXT KEY," + NOME + " TEXT," + NOME_BREVE
                + " TEXT," + QUOTA + " INTEGER," + LATITUDINE + " DOUBLE," + LONGITUDINE + " DOUBLE" + ")";
    }

    @Override
    public String getDropTableSQL() {
        return "DROP TABLE IF EXISTS " + getTableName();
    }

    @Override
    public String getInsertTableSQL() {
        return null;
    }

    @Override
    public String getTableName() {
        return "stazioniValanghe";
    }

    @Override
    public Uri getUri() {
        return URI;
    }

    @Override
    public StazioneValanghe parseCursor(final Cursor cursor) {
        StazioneValanghe stazioneValanghe = new StazioneValanghe();
        stazioneValanghe.setCodice(cursor.getString(0));
        stazioneValanghe.setNome(cursor.getString(1));
        stazioneValanghe.setNomeBreve(cursor.getString(2));
        stazioneValanghe.setQuota(Integer.parseInt(cursor.getString(3)));
        stazioneValanghe.setLatitudine(cursor.getDouble(4));
        stazioneValanghe.setLongitudine(cursor.getDouble(5));
        return stazioneValanghe;
    }

}
