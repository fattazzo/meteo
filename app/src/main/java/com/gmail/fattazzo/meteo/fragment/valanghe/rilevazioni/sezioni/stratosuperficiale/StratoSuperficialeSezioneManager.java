package com.gmail.fattazzo.meteo.fragment.valanghe.rilevazioni.sezioni.stratosuperficiale;

import java.util.ArrayList;
import java.util.List;

import android.view.View;

import com.gmail.fattazzo.meteo.R;
import com.gmail.fattazzo.meteo.domain.valanghe.dati.DatiStazione;
import com.gmail.fattazzo.meteo.fragment.valanghe.rilevazioni.sezioni.RilevazioneSezione;
import com.gmail.fattazzo.meteo.fragment.valanghe.rilevazioni.sezioni.RilevazioneSezioneListAdapter;
import com.gmail.fattazzo.meteo.fragment.valanghe.rilevazioni.view.AbstractSezioneManager;
import com.gmail.fattazzo.meteo.utils.ExpandableHeightListView;

/**
 *
 * @author fattazzo
 *
 *         date: 04/feb/2016
 *
 */
public class StratoSuperficialeSezioneManager extends AbstractSezioneManager {

    /**
     * Costruttore.
     *
     * @param view
     *            view
     */
    public StratoSuperficialeSezioneManager(final View view) {
        super(view);
    }

    @Override
    protected void clearContent() {
        ExpandableHeightListView condTempoList = (ExpandableHeightListView) view
                .findViewById(R.id.neve_stratosuperficiale_list);
        condTempoList.setAdapter(null);
    }

    @Override
    protected int getHeaderViewId() {
        return R.id.anag_stazione_neve_stratosuperficiale_layout;
    }

    @Override
    protected int getToggleViewId() {
        return R.id.anag_stazione_neve_stratosuperficialeData_layout;
    }

    @Override
    protected void loadContent() {
        List<RilevazioneSezione> stratosuperficialeList = new ArrayList<>();
        for (DatiStazione dato : getDatiStazione()) {
            RilevazioneSezione stratosuperficiale = new RilevazioneSezione();
            stratosuperficiale.setData(dato.getDataMisurazione());
            String descrizione = getLegendaDatiNeveManager().getMapStratoSuperficiale().get(
                    dato.getStratoSuperficiale());
            stratosuperficiale.setDescrizione(descrizione != null ? descrizione : "");
            stratosuperficialeList.add(stratosuperficiale);
        }

        RilevazioneSezioneListAdapter adapter = new RilevazioneSezioneListAdapter(view.getContext(),
                stratosuperficialeList);

        ExpandableHeightListView listView = (ExpandableHeightListView) view
                .findViewById(R.id.neve_stratosuperficiale_list);
        listView.setAdapter(adapter);
        listView.setExpanded(true);

    }
}
