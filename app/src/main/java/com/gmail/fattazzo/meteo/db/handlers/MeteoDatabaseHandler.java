/**
 *
 */
package com.gmail.fattazzo.meteo.db.handlers;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;
import android.util.Log;

import com.gmail.fattazzo.meteo.db.MeteoDatabase;
import com.gmail.fattazzo.meteo.db.structure.StazioneMeteoTable;
import com.gmail.fattazzo.meteo.db.structure.StazioneValangheTable;
import com.gmail.fattazzo.meteo.domain.stazioni.anagrafica.StazioneMeteo;
import com.gmail.fattazzo.meteo.domain.valanghe.anagrafica.StazioneValanghe;

import java.util.ArrayList;
import java.util.List;

/**
 * @author fattazzo
 *         <p/>
 *         date: 17/lug/2014
 */
public final class MeteoDatabaseHandler extends SQLiteOpenHelper {

    private static final String TAG = "MeteoDatabaseHandler";

    private static MeteoDatabaseHandler instance;
    private static MeteoDatabaseHandler testInstance;

    /**
     * Costruttore.
     *
     * @param context context
     * @return MeteoDatabaseHandler
     */
    public static synchronized MeteoDatabaseHandler getInstance(final Context context) {
        if (instance == null) {
            instance = new MeteoDatabaseHandler(context,false);
        }
        return instance;
    }

    /**
     * Costruttore.
     *
     * @param context context
     * @return MeteoDatabaseHandler
     */
    public static synchronized MeteoDatabaseHandler getTestInstance(final Context context) {
        if (testInstance == null) {
            testInstance = new MeteoDatabaseHandler(context,true);
        }
        return testInstance;
    }

    protected Context context;

    private SQLiteDatabase db;

    /**
     * Costruttore.
     *
     * @param paramContext context
     * @param inMemoryDB indica se creare il db solo in memoria, utilizzato a <code>true</code> per eseguire i test
     */
    private MeteoDatabaseHandler(final Context paramContext, boolean inMemoryDB) {
        super(paramContext, inMemoryDB ? null : MeteoDatabase.DATABASE_NAME, null, MeteoDatabase.DATABASE_VERSION);
        this.context = paramContext;
        db = this.getWritableDatabase();
    }

    /**
     * Cancella tutte le stazioni meteo.
     */
    public void cancellaStazioniMeteo() {
        db.delete(MeteoDatabase.STAZIONI_METEO_TABLE.getTableName(), "1", null);
        context.getContentResolver().notifyChange(MeteoDatabase.STAZIONI_METEO_TABLE.getUri(), null);
    }

    /**
     * Cancella tutte le stazioni valanghe.
     */
    public void cancellaStazioniValanghe() {
        db.delete(MeteoDatabase.STAZIONI_VALANGHE_TABLE.getTableName(), "1", null);
        context.getContentResolver().notifyChange(MeteoDatabase.STAZIONI_VALANGHE_TABLE.getUri(), null);
    }

    /**
     * Carica la stazione meteo in base al codice.
     *
     * @param codiceStazione codice della stazione meteo
     * @return stazione meteo caricata, <code>null</code> se non esiste
     */
    public StazioneMeteo caricaStazioneByCodice(final String codiceStazione) {
        StazioneMeteo stazioneMeteo = null;

        Cursor cursor = db.query(MeteoDatabase.STAZIONI_METEO_TABLE.getTableName(),
                MeteoDatabase.STAZIONI_METEO_TABLE.getColumns(), StazioneMeteoTable.CODICE + "=?",
                new String[]{codiceStazione}, null, null, null, null);

        if (cursor != null && cursor.getCount() != 0) {
            cursor.moveToFirst();
            stazioneMeteo = MeteoDatabase.STAZIONI_METEO_TABLE.parseCursor(cursor);
        }

        return stazioneMeteo;
    }

    /**
     * Carica tutte le stazioni meteo presenti.
     *
     * @return stazioni caricati
     */
    public List<StazioneMeteo> caricaStazioniMeteo() {
        List<StazioneMeteo> stazioniMeteo = new ArrayList<>();

        Cursor cursor = db.query(MeteoDatabase.STAZIONI_METEO_TABLE.getTableName(),
                MeteoDatabase.STAZIONI_METEO_TABLE.getColumns(), null, null, null, null, StazioneMeteoTable.NOME
                        + " asc");

        if (cursor.moveToFirst()) {
            do {
                StazioneMeteo stazioneMeteo = MeteoDatabase.STAZIONI_METEO_TABLE.parseCursor(cursor);
                stazioniMeteo.add(stazioneMeteo);
            } while (cursor.moveToNext());
        }

        return stazioniMeteo;
    }

    /**
     * Carica tutte le stazioni valanghe presenti.
     *
     * @return stazioni caricati
     */
    public List<StazioneValanghe> caricaStazioniValanghe() {
        List<StazioneValanghe> stazioniValanghe = new ArrayList<>();

        Cursor cursor = db.query(MeteoDatabase.STAZIONI_VALANGHE_TABLE.getTableName(),
                MeteoDatabase.STAZIONI_VALANGHE_TABLE.getColumns(), null, null, null, null, StazioneValangheTable.NOME
                        + " asc");

        if (cursor.moveToFirst()) {
            do {
                StazioneValanghe stazioneValanghe = MeteoDatabase.STAZIONI_VALANGHE_TABLE.parseCursor(cursor);
                stazioniValanghe.add(stazioneValanghe);
            } while (cursor.moveToNext());
        }

        return stazioniValanghe;
    }

    /**
     * Restituisce il numero stazioni meteo presenti.
     *
     * @return numero stazioni
     */
    public int getCountStazioni() {
        String selectQuery = "SELECT * FROM " + MeteoDatabase.STAZIONI_METEO_TABLE.getTableName();

        Cursor cursor = db.rawQuery(selectQuery, null);

        int count = cursor.getCount();
        cursor.close();

        return count;
    }

    /**
     * Restituisce il numero stazioni valanghe presenti.
     *
     * @return numero stazioni
     */
    public int getCountStazioniValanghe() {
        String selectQuery = "SELECT * FROM " + MeteoDatabase.STAZIONI_VALANGHE_TABLE.getTableName();

        Cursor cursor = db.rawQuery(selectQuery, null);

        int count = cursor.getCount();
        cursor.close();

        return count;
    }

    @Override
    public void onCreate(final SQLiteDatabase paramdb) {
        String[] createTables = MeteoDatabase.createTablesSQL();
        for (String sql : createTables) {
            paramdb.execSQL(sql);
        }
    }

    @Override
    public void onUpgrade(final SQLiteDatabase paramdb, final int oldVersion, final int newVersion) {
        Log.d(TAG, String.format("Upgrade da version from %d to %d", oldVersion, newVersion));

        switch (oldVersion) {
            case 1:
                StazioneValangheTable stazioneValangheTable = new StazioneValangheTable();
                paramdb.execSQL(stazioneValangheTable.getCreateTableSQL());
            default:
                break;
        }
    }

    /**
     * Inserisce nel database le stazioni meteo passate come parametro.
     *
     * @param stazioniMeteo stazioni da inserire
     */
    public void salvaStazioniMeteo(final List<StazioneMeteo> stazioniMeteo) {


        final SQLiteStatement statement = db.compileStatement(MeteoDatabase.STAZIONI_METEO_TABLE.getInsertTableSQL());
        db.beginTransaction();
        try {
            for (StazioneMeteo stazioneMeteo : stazioniMeteo) {
                statement.clearBindings();
                statement.bindString(1, stazioneMeteo.getCodice());
                statement.bindString(2, stazioneMeteo.getNome());
                statement.bindString(3, stazioneMeteo.getNomeBreve());
                statement.bindLong(4, stazioneMeteo.getQuota());
                statement.bindDouble(5, stazioneMeteo.getLatitudine());
                statement.bindDouble(6, stazioneMeteo.getLongitudine());
                statement.bindDouble(7, stazioneMeteo.getEst());
                statement.bindDouble(8, stazioneMeteo.getNord());
                statement.execute();
            }
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }

        context.getContentResolver().notifyChange(MeteoDatabase.STAZIONI_METEO_TABLE.getUri(), null);
    }

    /**
     * Inserisce nel database le stazioni valanghe passate come parametro.
     *
     * @param stazioniValanghe stazioni da inserire
     */
    public void salvaStazioniValanghe(final List<StazioneValanghe> stazioniValanghe) {

        for (StazioneValanghe stazioneValanghe : stazioniValanghe) {
            ContentValues values = MeteoDatabase.STAZIONI_VALANGHE_TABLE.createContentValues(stazioneValanghe);
            db.insert(MeteoDatabase.STAZIONI_VALANGHE_TABLE.getTableName(), null, values);
        }

        context.getContentResolver().notifyChange(MeteoDatabase.STAZIONI_VALANGHE_TABLE.getUri(), null);
    }

}
