package com.gmail.fattazzo.meteo.fragment.valanghe.rilevazioni.sezioni.nuvolisita;

import java.util.ArrayList;
import java.util.List;

import android.view.View;

import com.gmail.fattazzo.meteo.R;
import com.gmail.fattazzo.meteo.domain.valanghe.dati.DatiStazione;
import com.gmail.fattazzo.meteo.fragment.valanghe.rilevazioni.sezioni.RilevazioneSezione;
import com.gmail.fattazzo.meteo.fragment.valanghe.rilevazioni.sezioni.RilevazioneSezioneListAdapter;
import com.gmail.fattazzo.meteo.fragment.valanghe.rilevazioni.view.AbstractSezioneManager;
import com.gmail.fattazzo.meteo.utils.ExpandableHeightListView;

/**
 *
 * @author fattazzo
 *
 *         date: 03/feb/2016
 *
 */
public class NuvolositaSezioneManager extends AbstractSezioneManager {

    /**
     * Costruttore.
     *
     * @param view
     *            view
     */
    public NuvolositaSezioneManager(final View view) {
        super(view);
    }

    @Override
    protected void clearContent() {
        ExpandableHeightListView condTempoList = (ExpandableHeightListView) view
                .findViewById(R.id.neve_nuvolosita_list);
        condTempoList.setAdapter(null);
    }

    @Override
    protected int getHeaderViewId() {
        return R.id.anag_stazione_neve_nuvolosita_layout;
    }

    @Override
    protected int getToggleViewId() {
        return R.id.anag_stazione_neve_nuvolositaData_layout;
    }

    @Override
    protected void loadContent() {
        List<RilevazioneSezione> nuvolositaList = new ArrayList<>();
        for (DatiStazione dato : getDatiStazione()) {
            RilevazioneSezione nuvolosita = new RilevazioneSezione();
            nuvolosita.setData(dato.getDataMisurazione());
            String descrizione = getLegendaDatiNeveManager().getMapNuvolosita().get(dato.getNuvolosita());
            nuvolosita.setDescrizione(descrizione != null ? descrizione : "");
            nuvolositaList.add(nuvolosita);
        }

        RilevazioneSezioneListAdapter adapter = new RilevazioneSezioneListAdapter(view.getContext(), nuvolositaList);

        ExpandableHeightListView listView = (ExpandableHeightListView) view.findViewById(R.id.neve_nuvolosita_list);
        listView.setAdapter(adapter);
        listView.setExpanded(true);

    }
}
