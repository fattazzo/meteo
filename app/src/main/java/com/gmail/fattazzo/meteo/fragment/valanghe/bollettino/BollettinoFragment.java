package com.gmail.fattazzo.meteo.fragment.valanghe.bollettino;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.GestureDetectorCompat;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.gmail.fattazzo.meteo.R;
import com.gmail.fattazzo.meteo.domain.valanghe.bollettino.Bollettino;
import com.gmail.fattazzo.meteo.domain.valanghe.bollettino.Giorno;
import com.gmail.fattazzo.meteo.parser.manager.MeteoManager;
import com.gmail.fattazzo.meteo.preferences.ApplicationPreferencesManager;
import com.gmail.fattazzo.meteo.preferences.ApplicationPreferencesManager_;
import com.gmail.fattazzo.meteo.utils.Closure;
import com.gmail.fattazzo.meteo.utils.LoadImagefromUrl;

import java.lang.reflect.Field;

/**
 * @author fattazzo
 *         <p/>
 *         date: 04/feb/2016
 */
public class BollettinoFragment extends Fragment implements OnCheckedChangeListener, OnTouchListener {

    private View view;

    private MeteoManager meteoManager;
    private ApplicationPreferencesManager preferencesManager;

    private Bollettino bollettino = null;

    private GestureDetectorCompat detector;

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

        TextView situazioneText = (TextView) view.findViewById(R.id.boll_valanghe_situazione_label);

        if (isChecked) {
            situazioneText.setText(bollettino.getSituazione());
        }

        situazioneText.setVisibility(isChecked ? View.VISIBLE : View.GONE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_bollettino_valanghe, container, false);

        meteoManager = new MeteoManager(view.getContext());
        preferencesManager = ApplicationPreferencesManager_.getInstance_(view.getContext());
        scaricaBollettino();

        Switch situazioneSwitch = (Switch) view.findViewById(R.id.boll_valanghe_situazione_switch);
        situazioneSwitch.setOnCheckedChangeListener(this);

        detector = new GestureDetectorCompat(view.getContext(), new SimpleOnGestureListener() {

            @Override
            public boolean onSingleTapConfirmed(MotionEvent arg0) {
                return true;
            }

            @Override
            public boolean onSingleTapUp(MotionEvent e) {
                return true;
            }

        });

        return view;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        try {
            Field childFragmentManager = Fragment.class.getDeclaredField("mChildFragmentManager");
            childFragmentManager.setAccessible(true);
            childFragmentManager.set(this, null);
        } catch (NoSuchFieldException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if (detector.onTouchEvent(event) && v instanceof ImageView) {
            ImageView image = (ImageView) v;
            Dialog dialog = new ImageDialog(view.getContext(), image.getDrawable());
            dialog.show();
        }

        return true;
    }

    /**
     * Visualizza tutti i dati del bollettino caricato nella vista.
     */
    private void populateView() {

        if (bollettino == null) {
            view.findViewById(R.id.boll_valanghe_scrollView).setVisibility(View.INVISIBLE);
        } else {
            view.findViewById(R.id.boll_valanghe_scrollView).setVisibility(View.VISIBLE);

            TextView emissioneText = (TextView) view.findViewById(R.id.boll_valanghe_emissione_text);
            emissioneText.setText(bollettino.getGiornoEmissione(preferencesManager.getAppLocale()));

            TextView evoluzioneTempo = (TextView) view.findViewById(R.id.boll_valanghe_evoluzione_tempo_label);
            evoluzioneTempo.setText(String.format("%s: %s", view.getContext().getString(R.string.avalanche_evolution), bollettino.getEvoluzioneTempo()));

            ImageView indicePericoloView = (ImageView) view.findViewById(R.id.boll_valanghe_indice_paricolo_image);
            new LoadImagefromUrl().execute(indicePericoloView, bollettino.getMappaPericolo());
            indicePericoloView.setOnTouchListener(this);

            ImageView mappaNeveView = (ImageView) view.findViewById(R.id.boll_valanghe_mappa_neve_image);
            new LoadImagefromUrl().execute(mappaNeveView, bollettino.getMappaNeve());
            mappaNeveView.setOnTouchListener(this);

            TextView ultimaNavicata = (TextView) view.findViewById(R.id.boll_valanghe_ultima_nevicata_text);
            ultimaNavicata.setText(String.format("%s: %s", view.getContext().getString(R.string.avalanche_last_snowfall), bollettino.getUltimaNevicata(preferencesManager.getAppLocale())));

            TextView puntiPericolosi = (TextView) view.findViewById(R.id.boll_valanghe_punti_pericolosi_text);
            puntiPericolosi.setText(String.format("%s: %s", view.getContext().getString(R.string.avalanche_dangerous_points), bollettino.getPuntiPericolosi()));

            TextView evoluzioneProx = (TextView) view.findViewById(R.id.boll_valanghe_evoluzione_prox_text);
            evoluzioneProx.setText(String.format("%s: %s", view.getContext().getString(R.string.avalanche_evolution_coming_days), bollettino.getEvoluzioneProx()));

            LinearLayout giorniLayout = (LinearLayout) view.findViewById(R.id.boll_valanghe_giorni_layout);
            giorniLayout.removeAllViews();

            for (Giorno giorno : bollettino.getGiorni()) {
                LayoutInflater inflater = LayoutInflater.from(view.getContext());
                LinearLayout layoutGiorno = (LinearLayout) inflater.inflate(R.layout.fragment_bollettino_valanghe_giorno,
                        (ViewGroup) null, false);
                android.widget.LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                        android.widget.LinearLayout.LayoutParams.MATCH_PARENT,
                        android.widget.LinearLayout.LayoutParams.WRAP_CONTENT);
                layoutGiorno.setLayoutParams(layoutParams);
                TextView giornoTitle = (TextView) layoutGiorno.findViewById(R.id.boll_valanghe_giorno_title);
                giornoTitle.setText(giorno.getData());

                TextView zeroTermico = (TextView) layoutGiorno.findViewById(R.id.boll_valanghe_giorno_zero_termico_label);
                zeroTermico.setText(giorno.getZeroTermico());

                ImageView mappaPericoloView = (ImageView) layoutGiorno.findViewById(R.id.boll_valanghe_giorno_mappa);
                new LoadImagefromUrl().execute(mappaPericoloView, giorno.getMappapericolo());
                mappaPericoloView.setOnTouchListener(this);

                ImageView iconaTempo = (ImageView) layoutGiorno.findViewById(R.id.boll_valanghe_giorno_icona_tempo);
                new LoadImagefromUrl().execute(iconaTempo, giorno.getIconaUrl());

                TextView statoCielo = (TextView) layoutGiorno.findViewById(R.id.boll_valanghe_giorno_stato_cielo_label);
                statoCielo.setText(String.format("%s: %s", view.getContext().getString(R.string.avalanche_sky), giorno.getStatoCielo()));

                TextView situazione = (TextView) layoutGiorno.findViewById(R.id.boll_valanghe_giorno_situazione_label);
                situazione.setText(String.format("%s: %s", view.getContext().getString(R.string.avalanche_situation_description), giorno.getSituazione()));

                giorniLayout.addView(layoutGiorno);
            }
        }
    }

    /**
     * Scarica il bollettino giornaliero delle valanghe.
     */
    private void scaricaBollettino() {
        if (bollettino == null) {
            meteoManager.caricaBollettinoValanghe(new Closure<Bollettino>() {

                @Override
                public void execute(Bollettino paramObject) {
                    bollettino = paramObject;
                    populateView();
                }
            });
        } else {
            populateView();
        }
    }
}
