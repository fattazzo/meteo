package com.gmail.fattazzo.meteo.parser.xml;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;

import com.gmail.fattazzo.meteo.domain.valanghe.anagrafica.StazioneValanghe;
import com.gmail.fattazzo.meteo.domain.valanghe.anagrafica.Stazioni;
import com.gmail.fattazzo.meteo.domain.valanghe.dati.DatiStazione;
import com.gmail.fattazzo.meteo.parser.xml.handler.DatiNeveStazioniHandler;
import com.gmail.fattazzo.meteo.settings.MeteoSettings;
import com.thoughtworks.xstream.XStream;

/**
 *
 * @author fattazzo
 *
 *         date: 01/feb/2016
 *
 */
public class StazioniValangheXmlParser extends MeteoXmlParser {

    /**
     * Carica l'anagrafica delle stazioni valanghe.
     *
     * @return stazioni caricate
     */
    public List<StazioneValanghe> caricaAnagrafica() {

        List<StazioneValanghe> stazioniValanghe = new ArrayList<>();

        XStream xStream = new XStream();
        xStream.ignoreUnknownElements();
        xStream.alias("ArrayOfAnagrafica_neve", Stazioni.class);
        xStream.alias("anagrafica_neve", StazioneValanghe.class);
        xStream.autodetectAnnotations(true);
        xStream.addImplicitCollection(Stazioni.class, "stazioniValanghe");

        Stazioni stazioni = loadXml(MeteoSettings.ANAGRAFICA_STAZIONI_VALANGHE_XML, xStream);
        if (stazioni != null && stazioni.getStazioniValanghe() != null) {
            stazioniValanghe = stazioni.getStazioniValanghe();
        }

        return stazioniValanghe;
    }

    /**
     * Carica tutti i dati sul rilevamento neve delle stazioni.
     *
     * @return dati caricati
     */
    public List<DatiStazione> caricaDatiStazioneNeve() {

        List<DatiStazione> datiStazione = new ArrayList<>();

        try {
            InputStream inputStream = getInputStreamFromURL(MeteoSettings.DATI_STAZIONI_NEVE_XML);

            SAXParserFactory factory = SAXParserFactory.newInstance();
            SAXParser parser = factory.newSAXParser();
            DatiNeveStazioniHandler datiNeveStazioniHandler = new DatiNeveStazioniHandler();
            // parser.parse(inputStream, datiNeveStazioniHandler);
            XMLReader xr = parser.getXMLReader();
            xr.setContentHandler(datiNeveStazioniHandler);
            xr.parse(new InputSource(inputStream));
            if (datiNeveStazioniHandler.getDatiStazioni() != null && datiNeveStazioniHandler.getDatiStazioni() != null) {
                datiStazione = datiNeveStazioniHandler.getDatiStazioni();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return datiStazione;
    }

}
