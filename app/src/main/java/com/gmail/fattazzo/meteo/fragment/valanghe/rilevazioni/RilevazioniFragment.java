package com.gmail.fattazzo.meteo.fragment.valanghe.rilevazioni;

import java.lang.reflect.Field;
import java.util.List;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.gmail.fattazzo.meteo.R;
import com.gmail.fattazzo.meteo.db.handlers.MeteoDatabaseHandler;
import com.gmail.fattazzo.meteo.domain.valanghe.anagrafica.StazioneValanghe;
import com.gmail.fattazzo.meteo.domain.valanghe.dati.DatiStazione;
import com.gmail.fattazzo.meteo.fragment.valanghe.rilevazioni.view.RilevazioniViewManager;
import com.gmail.fattazzo.meteo.parser.manager.MeteoManager;
import com.gmail.fattazzo.meteo.utils.Closure;

/**
 *
 * @author fattazzo
 *
 *         date: 02/feb/2016
 *
 */
public class RilevazioniFragment extends Fragment implements OnClickListener, OnItemSelectedListener {

    private View view;

    private MeteoManager meteoManager;
    private MeteoDatabaseHandler databaseHandler;
    private RilevazioniViewManager rilevazioniViewManager;

    private List<DatiStazione> datiStazioni = null;

    private Spinner stazioniSpinner;

    /**
     * Carica i dati delle stazioni presenti.
     */
    private void loadData() {
        new AsyncTask<Void, Void, List<StazioneValanghe>>() {

            @Override
            protected List<StazioneValanghe> doInBackground(Void... params) {
                return databaseHandler.caricaStazioniValanghe();
            }

            @Override
            protected void onPostExecute(java.util.List<StazioneValanghe> result) {
                if (result.size() == 0) {
                    showNoStationsAvailableDialog();
                    return;
                }

                StazioneValanghe stazioneValanghe = new StazioneValanghe();
                stazioneValanghe.setCodice(null);
                stazioneValanghe.setNome("");

                result.add(0, stazioneValanghe);

                ArrayAdapter<StazioneValanghe> dataAdapter = new StazioneNeveAdapter(view.getContext(), result);
                dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                stazioniSpinner.setAdapter(dataAdapter);

                // carico già i dati delle stazioni
                if (datiStazioni == null) {
                    meteoManager.caricaDatiStazioniNeve(new Closure<List<DatiStazione>>() {

                        @Override
                        public void execute(List<DatiStazione> paramObject) {
                            datiStazioni = paramObject;
                        }
                    });
                }
            }
        }.execute();
    }

    @Override
    public void onClick(View v) {
        rilevazioniViewManager.toggleContent(v.getId());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (container != null) {
            container.removeAllViews();
        }

        if (view != null && view.getParent() != null) {
            ((ViewGroup) view.getParent()).removeView(view);
        } else {
            view = inflater.inflate(R.layout.fragment_dati_stazioni_neve, container, false);
        }

        meteoManager = new MeteoManager(view.getContext());
        databaseHandler = MeteoDatabaseHandler.getInstance(view.getContext());
        rilevazioniViewManager = new RilevazioniViewManager(view);
        rilevazioniViewManager.registerOnCLickListener(this);

        stazioniSpinner = (Spinner) view.findViewById(R.id.dati_stazione_neve_spinner);
        loadData();
        stazioniSpinner.setOnItemSelectedListener(this);

        return view;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        try {
            Field childFragmentManager = Fragment.class.getDeclaredField("mChildFragmentManager");
            childFragmentManager.setAccessible(true);
            childFragmentManager.set(this, null);
        } catch (NoSuchFieldException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        // non faccio niente finchè non viene scelta una stazione meteo
        if (datiStazioni != null && stazioniSpinner.getSelectedItemPosition() > 0) {

            StazioneValanghe stazioneValanghe = (StazioneValanghe) stazioniSpinner.getSelectedItem();
            rilevazioniViewManager.setStazioneCorrente(stazioneValanghe, datiStazioni);
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
    }

    /**
     * Visualizza il dialog che avverte l'utente del fatto che non sono ancora presenti delle stazioni.
     */
    private void showNoStationsAvailableDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());
        builder.setMessage(view.getContext().getString(R.string.no_snow_stations_alert_dialog)).setCancelable(false)
        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }
}
