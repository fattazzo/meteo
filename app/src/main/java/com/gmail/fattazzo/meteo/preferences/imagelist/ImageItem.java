package com.gmail.fattazzo.meteo.preferences.imagelist;

/**
 *
 * @author fattazzo
 *
 *         date: 27/ago/2014
 *
 */
public class ImageItem {

    private String file;
    private boolean checked;
    private String name;

    /**
     * Costruttore.
     *
     * @param name
     *            nome immagine
     * @param file
     *            nome del file
     * @param isChecked
     *            <code>true</code> se selezionata
     */
    public ImageItem(final CharSequence name, final CharSequence file, final boolean isChecked) {
        this(name.toString(), file.toString(), isChecked);
    }

    /**
     * Costruttore.
     *
     * @param name
     *            nome immagine
     * @param file
     *            nome del file
     * @param isChecked
     *            <code>true</code> se selezionata
     */
    public ImageItem(final String name, final String file, final boolean isChecked) {
        this.name = name;
        this.file = file;
        this.checked = isChecked;
    }

    /**
     * @return the file
     */
    public String getFile() {
        return file;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @return the checked
     */
    public boolean isChecked() {
        return checked;
    }

    /**
     * @param checked
     *            the checked to set
     */
    public void setChecked(boolean checked) {
        this.checked = checked;
    }

}
