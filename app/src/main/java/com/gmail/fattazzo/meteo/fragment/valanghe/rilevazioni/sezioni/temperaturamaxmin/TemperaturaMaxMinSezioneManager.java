package com.gmail.fattazzo.meteo.fragment.valanghe.rilevazioni.sezioni.temperaturamaxmin;

import android.view.View;

import com.gmail.fattazzo.meteo.R;
import com.gmail.fattazzo.meteo.domain.valanghe.dati.DatiStazione;
import com.gmail.fattazzo.meteo.fragment.valanghe.rilevazioni.sezioni.RilevazioneSezione;
import com.gmail.fattazzo.meteo.fragment.valanghe.rilevazioni.sezioni.RilevazioneSezioneListAdapter;
import com.gmail.fattazzo.meteo.fragment.valanghe.rilevazioni.view.AbstractSezioneManager;
import com.gmail.fattazzo.meteo.utils.ExpandableHeightListView;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author fattazzo
 *
 *         date: 03/feb/2016
 *
 */
public class TemperaturaMaxMinSezioneManager extends AbstractSezioneManager {

    /**
     * Costruttore.
     *
     * @param view
     *            view
     */
    public TemperaturaMaxMinSezioneManager(final View view) {
        super(view);
    }

    @Override
    protected void clearContent() {
        ExpandableHeightListView condTempoList = (ExpandableHeightListView) view
                .findViewById(R.id.neve_temperaturaminmax_list);
        condTempoList.setAdapter(null);
    }

    @Override
    protected int getHeaderViewId() {
        return R.id.anag_stazione_neve_temperaturaminmax_layout;
    }

    @Override
    protected int getToggleViewId() {
        return R.id.anag_stazione_neve_temperaturaminmaxData_layout;
    }

    @Override
    protected void loadContent() {
        List<RilevazioneSezione> temperaturaList = new ArrayList<>();
        for (DatiStazione dato : getDatiStazione()) {
            RilevazioneSezione temperatura = new RilevazioneSezione();
            temperatura.setData(dato.getDataMisurazione());
            String descrizione = getLegendaDatiNeveManager().parseTemperatura(dato.getTemperaturaMinima());
            descrizione += " - ";
            descrizione += getLegendaDatiNeveManager().parseTemperatura(dato.getTemperaturaMassima());
            temperatura.setDescrizione(descrizione);
            temperaturaList.add(temperatura);
        }

        RilevazioneSezioneListAdapter adapter = new RilevazioneSezioneListAdapter(view.getContext(), temperaturaList);

        ExpandableHeightListView listView = (ExpandableHeightListView) view.findViewById(R.id.neve_temperaturaminmax_list);
        listView.setAdapter(adapter);
        listView.setExpanded(true);

    }
}
