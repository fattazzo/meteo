package com.gmail.fattazzo.meteo.fragment.webcam;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Spinner;

import com.gmail.fattazzo.meteo.R;
import com.gmail.fattazzo.meteo.domain.webcam.Webcam;
import com.gmail.fattazzo.meteo.domain.webcam.Webcams;
import com.gmail.fattazzo.meteo.domain.webcam.ZoneWebcam;
import com.gmail.fattazzo.meteo.fragment.BaseFragment;
import com.gmail.fattazzo.meteo.parser.xml.WebcamXmlParser;
import com.gmail.fattazzo.meteo.settings.MeteoSettings;
import com.gmail.fattazzo.meteo.settings.widget.webcam.WebcamWidgetsSettingsManager;
import com.gmail.fattazzo.meteo.utils.ExpandableHeightListView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author fattazzo
 *         <p>
 *         date: 13/lug/2015
 */
public class WebcamFragment extends BaseFragment implements OnItemSelectedListener, OnItemClickListener,
        View.OnClickListener, OnItemLongClickListener {

    private View view;

    private Webcams webcams = null;
    private List<Webcam> zoneWebcams;

    private Spinner zoneSpinner;

    private WebcamXmlParser webcamXmlParser = new WebcamXmlParser();

    private ExpandableHeightListView webcamListView;

    private WebcamWidgetsSettingsManager webcamWidgetsSettingsManager;

    /**
     * @return the webcams
     */
    public Webcams getWebcams() {
        if (webcams == null) {
            webcams = webcamXmlParser.caricaWebcam(view.getContext());
        }

        return webcams;
    }

    @Override
    public void onClick(final View v) {

        switch (v.getId()) {
            case R.id.webcam_zone_button:
                Intent intent = new Intent(view.getContext(), WebcamsViewerActivity.class);
                String zone = (String) zoneSpinner.getSelectedItem();
                ArrayList<Webcam> zoneWebcams = (ArrayList<Webcam>) webcams.getWebcams(zone);
                intent.putExtra(WebcamsViewerActivity.EXTRA_WEBCAMS, zoneWebcams);
                startActivity(intent);
                break;
            case R.id.webcam_send_newlink:
                AlertDialog alertDialog = new AlertDialog.Builder(v.getContext()).create(); // Read Update
                alertDialog.setTitle("");
                alertDialog.setMessage(v.getContext().getString(R.string.webcam_report_dialog_message));

                alertDialog.setButton(DialogInterface.BUTTON_NEUTRAL,
                        v.getContext().getString(R.string.webcam_report_dialog_button),
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri
                                        .parse(MeteoSettings.GIT_ISSUE_LINK));
                                v.getContext().startActivity(browserIntent);
                            }
                        });

                alertDialog.show();
                break;
            default:
                break;
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_webcam, container, false);


        webcamWidgetsSettingsManager = new WebcamWidgetsSettingsManager(view.getContext());

        List<String> zones = new ArrayList<>();
        for (ZoneWebcam zone : getWebcams().getZoneWebcams()) {
            zones.add(zone.getDescrizione());
        }
        Collections.sort(zones);

        zoneSpinner = (Spinner) view.findViewById(R.id.webcam_zone_spinner);
        ArrayAdapter<String> zonesAdapter = new ArrayAdapter<>(view.getContext(),
                android.R.layout.simple_spinner_item, zones);
        zonesAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        zoneSpinner.setAdapter(zonesAdapter);
        zoneSpinner.setOnItemSelectedListener(this);

        webcamListView = (ExpandableHeightListView) view.findViewById(R.id.webcam_list_view);
        webcamListView.setOnItemClickListener(this);
        webcamListView.setOnItemLongClickListener(this);

        ImageButton refreshButton = (ImageButton) view.findViewById(R.id.webcam_zone_button);
        refreshButton.setOnClickListener(this);

        Button sendLinkButton = (Button) view.findViewById(R.id.webcam_send_newlink);
        sendLinkButton.setOnClickListener(this);

        return view;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        switch (parent.getId()) {
            case R.id.webcam_list_view:
                Intent intent = new Intent(view.getContext(), WebcamsViewerActivity.class);
                ArrayList<Webcam> webcams = new ArrayList<>();
                webcams.add((Webcam) webcamListView.getItemAtPosition(position));
                intent.putExtra(WebcamsViewerActivity.EXTRA_WEBCAMS, webcams);
                startActivity(intent);
                break;
            default:
                break;
        }

    }

    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
        Webcam webcam = (Webcam) webcamListView.getItemAtPosition(position);
        webcamWidgetsSettingsManager.updateWebcamWidgetIds(webcam.getId());

        webcam.setShowInWidget(!webcam.isShowInWidget());
        int idx = zoneWebcams.indexOf(webcam);
        zoneWebcams.set(idx, webcam);
        webcamListView.invalidateViews();
        return true;
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        switch (parent.getId()) {
            case R.id.webcam_zone_spinner:
                String zone = (String) zoneSpinner.getSelectedItem();
                zoneWebcams = webcams.getWebcams(zone);

                if (zoneWebcams != null) {
                    WebcamListAdapter adapter = new WebcamListAdapter(WebcamFragment.this.view.getContext(),
                            zoneWebcams);
                    webcamListView.setAdapter(adapter);
                    webcamListView.setExpanded(true);
                }
                break;
            default:
                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
    }

    @Override
    public int getTitleResId() {
        return R.string.nav_webcam;
    }
}
