package com.gmail.fattazzo.meteo.exception.handler;

import java.lang.Thread.UncaughtExceptionHandler;
import java.net.UnknownHostException;

import android.content.Context;
import android.os.Looper;
import android.widget.Toast;

/**
 *
 * @author fattazzo
 *
 *         date: 09/giu/2014
 *
 */
public class ExceptionHandler implements UncaughtExceptionHandler {

    /**
     * Rimuove le runtimeException.
     *
     * @param e
     *            eccezione dalla quale rimuovere le runtime
     * @return eccezione con le runtime rimosse
     */
    private static Throwable removeRuntimeException(Throwable e) {
        if (e != null && (e instanceof MeteoRuntimeException || e instanceof RuntimeException)) {
            e = e.getCause();
            return removeRuntimeException(e);
        } else {
            return e;
        }
    }

    private final Context appCtx;

    /**
     * Costruttore.
     *
     * @param context
     *            context
     */
    public ExceptionHandler(final Context context) {
        super();
        this.appCtx = context;
    }

    @Override
    public void uncaughtException(Thread thread, Throwable ex) {
        System.err.println("Exception: " + ex);

        Throwable originalThow = ex;
        ex = removeRuntimeException(ex);
        if (ex == null) {
            ex = originalThow;
        }

        if (ex instanceof UnknownHostException) {
            new Thread(new Runnable() {

                @Override
                public void run() {
                    Looper.prepare();
                    Toast toast = Toast.makeText(appCtx, "Impossibile contattare il sito. Riprovare più tardi",
                            Toast.LENGTH_LONG);
                    toast.show();

                    Looper.loop();
                }
            }).start();
        } else {
            new Thread(new Runnable() {

                @Override
                public void run() {
                    Looper.prepare();
                    Toast toast = Toast.makeText(appCtx, "Servizio attualmente non disponibile. Riprovare più tardi",
                            Toast.LENGTH_LONG);
                    toast.show();

                    Looper.loop();
                }
            }).start();
        }
    }

}
