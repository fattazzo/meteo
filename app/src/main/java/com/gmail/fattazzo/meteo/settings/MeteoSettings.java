package com.gmail.fattazzo.meteo.settings;

/**
 * @author fattazzo
 *         <p>
 *         date: 12/giu/2014
 */
public final class MeteoSettings {

    public static final String ANAGRAFICA_STAZIONI_XML = "http://dati.meteotrentino.it/service.asmx/listaStazioni";
    public static final String DATI_STAZIONI_XML = "http://dati.meteotrentino.it/service.asmx/ultimiDatiStazione?codice=";

    public static final String ANAGRAFICA_STAZIONI_VALANGHE_XML = "http://dati.meteotrentino.it/service.asmx/listaCampiNeve";
    public static final String DATI_STAZIONI_NEVE_XML = "http://dati.meteotrentino.it/service.asmx/tuttiUltimiRilieviNeve";
    public static final String BOLLETTINO_VALANGHE_XML = "http://www.meteotrentino.it/bollettini/today/valanghe_it.xml";

    // globali
    public static final String LANGUAGE = "language";
    public static final String LAST_RUN_VERSION_NAME = "lastRunVersionName";

    // stazioni meteo
    public static final String STAZ_METEO_GRAPH_SHOW_POINT = "stazMeteoGraphShowPoint";
    public static final String STAZ_METEO_GRAPH_POINT_RADIUS = "stazMeteoGraphPointRadius";
    public static final String STAZ_METEO_GRAPH_SHOW_SERIES_BG = "stazMeteoGraphShowSeriesBg";

    // widgets
    public static final String WIDGETS_BACKGROUND = "widgetsBackground";
    public static final String WIDGETS_TEXT_COLOR = "widgetsTextColor";
    // bull
    public static final String WIDGETS_UPDATE_INTERVAL = "widgetsUpdateInterval";
    public static final String WIDGETS_ZONE = "widgetsZone";
    // webcam
    public static final String WIDGETS_WEBCAM_UPDATE_INTERVAL = "widgetsWebcamUpdateInterval";
    public static final String WIDGETS_WEBCAM_IDS = "WIDGETS_WEBCAM_IDS";

    // git
    public static final String GIT_ISSUE_LINK = "https://bitbucket.org/fattazzo/meteo/issues/new";
    public static final String GIT_WIKI_LINK = "https://bitbucket.org/fattazzo/meteo/wiki/Home";

    // icons theme
    public static final String ICONS_THEME = "iconsTheme";

    public static final String PREV_AUDIO_URL = "http://www.meteotrentino.it/meteo.mp3";

    /**
     * Costruttore.
     */
    private MeteoSettings() {
        super();
    }
}
