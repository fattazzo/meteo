package com.gmail.fattazzo.meteo.fragment.valanghe.rilevazioni.sezioni.brinasuperficie;

import java.util.ArrayList;
import java.util.List;

import android.view.View;

import com.gmail.fattazzo.meteo.R;
import com.gmail.fattazzo.meteo.domain.valanghe.dati.DatiStazione;
import com.gmail.fattazzo.meteo.fragment.valanghe.rilevazioni.sezioni.RilevazioneSezione;
import com.gmail.fattazzo.meteo.fragment.valanghe.rilevazioni.sezioni.RilevazioneSezioneListAdapter;
import com.gmail.fattazzo.meteo.fragment.valanghe.rilevazioni.view.AbstractSezioneManager;
import com.gmail.fattazzo.meteo.utils.ExpandableHeightListView;

/**
 *
 * @author fattazzo
 *
 *         date: 04/feb/2016
 *
 */
public class BrinaSuperficieSezioneManager extends AbstractSezioneManager {

    /**
     * Costruttore.
     *
     * @param view
     *            view
     */
    public BrinaSuperficieSezioneManager(final View view) {
        super(view);
    }

    @Override
    protected void clearContent() {
        ExpandableHeightListView condTempoList = (ExpandableHeightListView) view
                .findViewById(R.id.neve_brinasuperficie_list);
        condTempoList.setAdapter(null);
    }

    @Override
    protected int getHeaderViewId() {
        return R.id.anag_stazione_neve_brinasuperficie_layout;
    }

    @Override
    protected int getToggleViewId() {
        return R.id.anag_stazione_neve_brinasuperficieData_layout;
    }

    @Override
    protected void loadContent() {
        List<RilevazioneSezione> brinasuperficieList = new ArrayList<>();
        for (DatiStazione dato : getDatiStazione()) {
            RilevazioneSezione brinasuperficie = new RilevazioneSezione();
            brinasuperficie.setData(dato.getDataMisurazione());
            String descrizione = getLegendaDatiNeveManager().getMapBrinaSuperficie().get(dato.getBrinaSuperficie());
            brinasuperficie.setDescrizione(descrizione != null ? descrizione : "");
            brinasuperficieList.add(brinasuperficie);
        }

        RilevazioneSezioneListAdapter adapter = new RilevazioneSezioneListAdapter(view.getContext(),
                brinasuperficieList);

        ExpandableHeightListView listView = (ExpandableHeightListView) view
                .findViewById(R.id.neve_brinasuperficie_list);
        listView.setAdapter(adapter);
        listView.setExpanded(true);

    }
}
