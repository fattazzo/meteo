package com.gmail.fattazzo.meteo.fragment.stazioni.rilevazioni;

import java.lang.reflect.Field;
import java.util.List;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.Spinner;
import android.widget.TextView;

import com.gmail.fattazzo.meteo.R;
import com.gmail.fattazzo.meteo.db.handlers.MeteoDatabaseHandler;
import com.gmail.fattazzo.meteo.domain.stazioni.anagrafica.StazioneMeteo;
import com.gmail.fattazzo.meteo.domain.stazioni.dati.DatiStazione;
import com.gmail.fattazzo.meteo.fragment.stazioni.rilevazioni.builder.GraficoDatoStazioneBuilder;
import com.gmail.fattazzo.meteo.fragment.stazioni.rilevazioni.builder.TabellaDatoStazioneBuilder;
import com.gmail.fattazzo.meteo.fragment.stazioni.rilevazioni.builder.TipoDatoStazioneLayoutBuilder;
import com.gmail.fattazzo.meteo.parser.manager.MeteoManager;
import com.gmail.fattazzo.meteo.utils.Closure;

/**
 *
 * @author fattazzo
 *
 *         date: 13/lug/2015
 *
 */
public class RilevazioniFragment extends Fragment implements OnItemSelectedListener, OnCheckedChangeListener,
        Closure<DatiStazione>, OnClickListener {

    private View view;

    private MeteoManager meteoManager;

    private MeteoDatabaseHandler databaseHandler;

    private Spinner stazioniSpinner;
    private Spinner tipoDatoSpinner;
    private RadioGroup visualizzazioneRadioGroup;
    private RelativeLayout controlsLayout;

    private DatiStazione datiStazione;

    private TextView titleTextView;

    private final SparseArray<TipoDatoStazioneLayoutBuilder> tipiDatoBuilder = new SparseArray<>();

    {
        tipiDatoBuilder.append(R.id.dati_stazione_visualizzazione_tabella, new TabellaDatoStazioneBuilder());
        tipiDatoBuilder.append(R.id.dati_stazione_visualizzazione_grafico, new GraficoDatoStazioneBuilder());
    }

    @Override
    public void execute(DatiStazione paramDatiStazione) {

        this.datiStazione = paramDatiStazione;

        final RelativeLayout visualizzazioneLayout = (RelativeLayout) view
                .findViewById(R.id.dati_stazione_visualizzazione_layout);
        visualizzazioneLayout.removeAllViews();
        LayoutParams layoutParams = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
        layoutParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
        layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
        layoutParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
        layoutParams.addRule(RelativeLayout.ALIGN_PARENT_TOP);

        TipoDatoStazione tipoDatoStazione = TipoDatoStazione.values()[tipoDatoSpinner.getSelectedItemPosition()];

        TipoDatoStazioneLayoutBuilder layoutBuilder = tipiDatoBuilder.get(visualizzazioneRadioGroup
                .getCheckedRadioButtonId());
        visualizzazioneLayout.addView(layoutBuilder.buildView(view.getContext(), datiStazione, tipoDatoStazione),
                layoutParams);
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        visualizzaDatiStazione();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
        case R.id.dati_stazione_hide_panel_img:
            controlsLayout.setVisibility(controlsLayout.getVisibility() == View.GONE ? View.VISIBLE : View.GONE);
            break;
        default:
            break;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (container != null) {
            container.removeAllViews();
        }

        if (view != null && view.getParent() != null) {
            ((ViewGroup) view.getParent()).removeView(view);
        } else {
            view = inflater.inflate(R.layout.fragment_dati_stazioni, container, false);
        }

        databaseHandler = MeteoDatabaseHandler.getInstance(view.getContext());
        meteoManager = new MeteoManager(view.getContext());

        RelativeLayout visualizzazioneLayout = (RelativeLayout) view
                .findViewById(R.id.dati_stazione_visualizzazione_layout);
        visualizzazioneLayout.removeAllViews();

        titleTextView = (TextView) view.findViewById(R.id.dati_stazione_title);
        stazioniSpinner = (Spinner) view.findViewById(R.id.dati_stazione_spinner);
        tipoDatoSpinner = (Spinner) view.findViewById(R.id.dati_stazione_tipo_dato);
        visualizzazioneRadioGroup = (RadioGroup) view.findViewById(R.id.dati_stazione_tipo_visualizzazione);
        controlsLayout = (RelativeLayout) view.findViewById(R.id.anag_stazione_controls_layout);
        ImageView hideShowControlsImg = (ImageView) view.findViewById(R.id.dati_stazione_hide_panel_img);
        hideShowControlsImg.setOnClickListener(this);

        new AsyncTask<Void, Void, List<StazioneMeteo>>() {

            @Override
            protected List<StazioneMeteo> doInBackground(Void... params) {
                return databaseHandler.caricaStazioniMeteo();
            }

            @Override
            protected void onPostExecute(java.util.List<StazioneMeteo> result) {
                StazioneMeteo stazioneMeteo = new StazioneMeteo();
                stazioneMeteo.setCodice(null);
                stazioneMeteo.setNome("");

                result.add(0, stazioneMeteo);

                ArrayAdapter<StazioneMeteo> dataAdapter = new StazioneMeteoAdapter(view.getContext(), result);
                dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                stazioniSpinner.setAdapter(dataAdapter);
            }
        }.execute();

        stazioniSpinner.setOnItemSelectedListener(this);
        tipoDatoSpinner.setOnItemSelectedListener(this);
        visualizzazioneRadioGroup.setOnCheckedChangeListener(this);

        return view;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        try {
            Field childFragmentManager = Fragment.class.getDeclaredField("mChildFragmentManager");
            childFragmentManager.setAccessible(true);
            childFragmentManager.set(this, null);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View paramVview, int position, long id) {
        visualizzaDatiStazione();

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
    }

    /**
     * Visualizza i dati della stazione selezionata.
     */
    private void visualizzaDatiStazione() {

        titleTextView.setText("");

        RelativeLayout visualizzazioneLayout = (RelativeLayout) view
                .findViewById(R.id.dati_stazione_visualizzazione_layout);
        visualizzazioneLayout.removeAllViews();

        // no faccio niente finchè non viene scelta una stazione meteo
        if (stazioniSpinner.getSelectedItemPosition() > 0) {

            StazioneMeteo stazioneMeteo = (StazioneMeteo) stazioniSpinner.getSelectedItem();
            titleTextView.setText(String.format("%s (%s)", stazioneMeteo.getNome(), tipoDatoSpinner.getSelectedItem().toString()));

            if (datiStazione != null && stazioneMeteo.getCodice().equals(datiStazione.getCodiceStazione())) {
                execute(datiStazione);
            } else {
                meteoManager.caricaDatiStazione(stazioneMeteo.getCodice(), this);
            }
        }
    }

}
