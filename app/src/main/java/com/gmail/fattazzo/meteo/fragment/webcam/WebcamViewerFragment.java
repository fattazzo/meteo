package com.gmail.fattazzo.meteo.fragment.webcam;

import java.util.List;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.LinearLayout;
import android.widget.TabHost;
import android.widget.TextView;

import com.gmail.fattazzo.meteo.R;
import com.gmail.fattazzo.meteo.domain.webcam.Webcam;
import com.gmail.fattazzo.meteo.domain.webcam.WebcamLink;
import com.gmail.fattazzo.meteo.fragment.webcam.component.WebcamComponentViewerFactory;

/**
 * Fragment che visualizza le webcam.
 */
public class WebcamViewerFragment extends Fragment {

    /**
     * Crea una nuova instanza per il fragment.
     *
     * @param webcams
     *            webcam disponibili
     * @param position
     *            indice della webcam da visualizzare
     * @return nuova instanza di {@link WebcamViewerFragment}
     */
    public static WebcamViewerFragment newInstance(List<Webcam> webcams, int position) {
        WebcamViewerFragment fragment = new WebcamViewerFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_FRAGMENT_WEBCAM, webcams.get(position));
        args.putSerializable(ARG_FRAGMENT_WEBCAM_POSITION, position);
        args.putSerializable(ARG_FRAGMENT_WEBCAMS_NUMBER, webcams.size());
        fragment.setArguments(args);
        return fragment;
    }

    private static final String ARG_FRAGMENT_WEBCAM = "argFragmentWebcam";
    private static final String ARG_FRAGMENT_WEBCAM_POSITION = "argFragmentWebcamPosition";
    private static final String ARG_FRAGMENT_WEBCAMS_NUMBER = "argFragmentWebcamsNumber";
    private View rootView;

    private TabHost tabHost;

    /**
     * Costruttore.<br>
     * Utilizzare il metono {@link WebcamViewerFragment#newInstance(List, int)} per creare una nuova instanza.
     */
    public WebcamViewerFragment() {

    }

    /**
     * Aggiunge un nuovo tab al tabhost.
     *
     * @param title
     *            titolo del tab
     * @param link
     *            link della webcam
     */
    private void addNewTab(String title, final WebcamLink link) {

        TabHost.TabSpec spec = tabHost.newTabSpec(title);

        View view = LayoutInflater.from(rootView.getContext()).inflate(R.layout.tabs_bg, (ViewGroup) null);
        TextView tv = (TextView) view.findViewById(R.id.tabsText);
        tv.setText(title);

        spec = spec.setIndicator(view);
        spec = spec.setContent(new TabHost.TabContentFactory() {
            @Override
            public View createTabContent(String tag) {
                View webcamView = WebcamComponentViewerFactory.getView(link, rootView.getContext());

                LinearLayout panel = new LinearLayout(rootView.getContext());
                panel.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
                panel.setOrientation(LinearLayout.VERTICAL);
                panel.addView(webcamView);
                return panel;
            }
        });
        tabHost.addTab(spec);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_webcams_viewer, container, false);

        Webcam webcam = (Webcam) getArguments().getSerializable(ARG_FRAGMENT_WEBCAM);

        tabHost = (TabHost) rootView.findViewById(R.id.webcam_tabhost);
        tabHost.setup();

        int linkIdx = 1;
        for (final WebcamLink link : webcam.getLinks()) {
            addNewTab(String.valueOf(linkIdx++), link);
        }
        tabHost.getTabWidget().setVisibility(webcam.getLinks().size() > 1 ? View.VISIBLE : View.GONE);

        TextView webcamNameTextView = (TextView) rootView.findViewById(R.id.webcam_viewer_name);
        webcamNameTextView.setText(webcam.getDescrizione());

        //int webcamIndex = getArguments().getInt(ARG_FRAGMENT_WEBCAM_POSITION, 0);
        //int webcamsNumber = getArguments().getInt(ARG_FRAGMENT_WEBCAMS_NUMBER, 1);
        //TextView webcamPriorTextView = (TextView) rootView.findViewById(R.id.webcam_viewer_next_webcam);
        //webcamPriorTextView.setVisibility(webcamsNumber > 1 && webcamIndex > 0 ? View.VISIBLE : View.GONE);

        //TextView webcamNextTextView = (TextView) rootView.findViewById(R.id.webcam_viewer_prior_webcam);
        //webcamNextTextView.setVisibility(webcamsNumber > 1 && webcamIndex + 1 < webcamsNumber ? View.VISIBLE
        //        : View.GONE);

        return rootView;
    }
}
