package com.gmail.fattazzo.meteo.fragment.stazioni.rilevazioni;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.gmail.fattazzo.meteo.domain.stazioni.anagrafica.StazioneMeteo;

/**
 *
 * @author fattazzo
 *
 *         date: 18/lug/2014
 *
 */
public class StazioneMeteoAdapter extends ArrayAdapter<StazioneMeteo> {

    private List<StazioneMeteo> itemList;

    /**
     * Costruttore.
     *
     * @param context
     *            context
     * @param itemList
     *            lista delle stazioni meteo
     */
    public StazioneMeteoAdapter(final Context context, final List<StazioneMeteo> itemList) {

        super(context, android.R.layout.simple_spinner_item);
        this.itemList = itemList;
    }

    @Override
    public int getCount() {
        return this.itemList.size();
    }

    @Override
    public TextView getDropDownView(int position, View convertView, ViewGroup parent) {

        TextView v = (TextView) convertView;

        if (v == null) {
            LayoutInflater vi = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = (TextView) vi.inflate(android.R.layout.simple_spinner_item, (ViewGroup) null);
        }

        v.setText(itemList.get(position).getNome());
        return v;
    }

    @Override
    public StazioneMeteo getItem(int position) {
        return itemList.get(position);
    }

    @Override
    public TextView getView(int position, View convertView, ViewGroup parent) {

        TextView v = (TextView) convertView;

        if (v == null) {
            LayoutInflater vi = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = (TextView) vi.inflate(android.R.layout.simple_spinner_item, (ViewGroup) null);
        }

        v.setText(itemList.get(position).getNome());
        return v;
    }

}
