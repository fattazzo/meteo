package com.gmail.fattazzo.meteo.fragment.stazioni.rilevazioni.builder;

import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.formatter.IValueFormatter;
import com.github.mikephil.charting.utils.ViewPortHandler;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class DateChartFormatterFormatter implements IValueFormatter {

    private final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm", Locale.ITALIAN);
    private final DecimalFormat decimalFormat = new DecimalFormat("0.00");


    public DateChartFormatterFormatter() {
    }

    @Override
    public String getFormattedValue(float value, Entry entry, int dataSetIndex, ViewPortHandler viewPortHandler) {
        if (value > 1000) {
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis((long) value);
            return dateFormat.format(new Date((long) value));
        } else {
            return decimalFormat.format(value);
        }
    }
}