package com.gmail.fattazzo.meteo.settings;

import android.content.Context;

/**
 *
 * @author fattazzo
 *
 *         date: 30/lug/2014
 *
 */
public class StazioniMeteoSettingsManager extends MeteoSettingsManager {

    /**
     * Costruttore.
     *
     * @param context
     *            context
     */
    public StazioniMeteoSettingsManager(final Context context) {
        super(context);
    }

    /**
     * @return restituisce il raggio da utilizzare per i punti delle serie
     */
    public int getGraphPointRadius() {
        return getPrefs().getInt(MeteoSettings.STAZ_METEO_GRAPH_POINT_RADIUS, 5)-2;
        // -2 dovuto alla libreria precedente dove i punti avevano il range 1-10. Il punto 5 corrisponde al 3 per l'attuale libreria
    }

    /**
     * @return <code>true</code> se i punti dei valori delle serie devono essere visualizzati.
     */
    public boolean showGraphPoint() {
        return getPrefs().getBoolean(MeteoSettings.STAZ_METEO_GRAPH_SHOW_POINT, Boolean.TRUE);
    }

    /**
     * @return <code>true</code> se il background delle serie deve essere visualizzato.
     */
    public boolean showGraphSeriesBackground() {
        return getPrefs().getBoolean(MeteoSettings.STAZ_METEO_GRAPH_SHOW_SERIES_BG, Boolean.TRUE);
    }
}
