package com.gmail.fattazzo.meteo.fragment.valanghe.rilevazioni.sezioni.condtempo;

import java.util.ArrayList;
import java.util.List;

import android.view.View;

import com.gmail.fattazzo.meteo.R;
import com.gmail.fattazzo.meteo.domain.valanghe.dati.DatiStazione;
import com.gmail.fattazzo.meteo.fragment.valanghe.rilevazioni.sezioni.RilevazioneSezione;
import com.gmail.fattazzo.meteo.fragment.valanghe.rilevazioni.sezioni.RilevazioneSezioneListAdapter;
import com.gmail.fattazzo.meteo.fragment.valanghe.rilevazioni.view.AbstractSezioneManager;
import com.gmail.fattazzo.meteo.utils.ExpandableHeightListView;

/**
 *
 * @author fattazzo
 *
 *         date: 03/feb/2016
 *
 */
public class CondTempoSezioneManager extends AbstractSezioneManager {

    /**
     * Costruttore.
     *
     * @param view
     *            view
     */
    public CondTempoSezioneManager(final View view) {
        super(view);
    }

    @Override
    protected void clearContent() {
        ExpandableHeightListView condTempoList = (ExpandableHeightListView) view.findViewById(R.id.neve_condtempo_list);
        condTempoList.setAdapter(null);
    }

    @Override
    protected int getHeaderViewId() {
        return R.id.anag_stazione_neve_condMeteo_layout;
    }

    @Override
    protected int getToggleViewId() {
        return R.id.anag_stazione_neve_condMeteoData_layout;
    }

    @Override
    public void loadContent() {
        List<RilevazioneSezione> condizioni = new ArrayList<>();
        for (DatiStazione dato : getDatiStazione()) {
            RilevazioneSezione condizioneTempo = new RilevazioneSezione();
            condizioneTempo.setData(dato.getDataMisurazione());
            String descrizione = getLegendaDatiNeveManager().getMapCondMeteo().get(dato.getCondizioniTempo());
            condizioneTempo.setDescrizione(descrizione != null ? descrizione : "");
            condizioni.add(condizioneTempo);
        }

        RilevazioneSezioneListAdapter adapter = new RilevazioneSezioneListAdapter(view.getContext(), condizioni);

        ExpandableHeightListView listView = (ExpandableHeightListView) view.findViewById(R.id.neve_condtempo_list);
        listView.setAdapter(adapter);
        listView.setExpanded(true);
    }

}
