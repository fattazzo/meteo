package com.gmail.fattazzo.meteo.utils;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.os.AsyncTask;
import android.util.Log;

/**
 *
 * @author fattazzo
 *
 *         date: 03/set/2014
 *
 */
public class LoadBitmapTask extends AsyncTask<String, Void, Bitmap> {

    private Options options = null;

    /**
     * Costruttore.
     */
    public LoadBitmapTask() {
        this(null);
    }

    /**
     * Costruttore.
     *
     * @param options
     *            decode options
     */
    public LoadBitmapTask(final BitmapFactory.Options options) {
        super();
        this.options = options;
    }

    @Override
    protected Bitmap doInBackground(String... params) {
        URL newurl = null;
        Bitmap bitmap = null;
        try {
            newurl = new URL(params[0]);
            bitmap = BitmapFactory.decodeStream(newurl.openConnection().getInputStream(), null, options);
            Log.e("TASK", "Load " + newurl);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bitmap;
    }

}
