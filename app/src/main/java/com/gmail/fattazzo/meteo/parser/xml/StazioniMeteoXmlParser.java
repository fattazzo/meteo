package com.gmail.fattazzo.meteo.parser.xml;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import com.gmail.fattazzo.meteo.domain.stazioni.anagrafica.StazioneMeteo;
import com.gmail.fattazzo.meteo.domain.stazioni.anagrafica.Stazioni;
import com.gmail.fattazzo.meteo.domain.stazioni.dati.DatiStazione;
import com.gmail.fattazzo.meteo.domain.stazioni.dati.PrecipitazioneStazione;
import com.gmail.fattazzo.meteo.domain.stazioni.dati.TemperaturaAriaStazione;
import com.gmail.fattazzo.meteo.exception.handler.MeteoRuntimeException;
import com.gmail.fattazzo.meteo.settings.MeteoSettings;
import com.thoughtworks.xstream.XStream;

/**
 *
 * @author fattazzo
 *
 *         date: 13/lug/2015
 *
 */
public class StazioniMeteoXmlParser extends MeteoXmlParser {

    /**
     * Carica l'anagrafica delle stazioni meteo.
     *
     * @return stazioni caricate
     */
    public List<StazioneMeteo> caricaAnagrafica() {

        List<StazioneMeteo> stazioniMeteo = new ArrayList<>();

        XStream xStream = new XStream();
        xStream.ignoreUnknownElements();
        xStream.alias("ArrayOfAnagrafica", Stazioni.class);
        xStream.alias("anagrafica", StazioneMeteo.class);
        xStream.autodetectAnnotations(true);
        xStream.addImplicitCollection(Stazioni.class, "stazioniMeteo");

        Stazioni stazioni = loadXml(MeteoSettings.ANAGRAFICA_STAZIONI_XML, xStream);
        if (stazioni != null && stazioni.getStazioniMeteo() != null) {
            stazioniMeteo = stazioni.getStazioniMeteo();
        }

        return stazioniMeteo;
    }

    /**
     * Carica tutti i dati per la stazione con il codice passato come parametro.
     *
     * @param codiceStazione
     *            codice della stazione
     * @return dati caricati
     */
    public DatiStazione caricaDatiStazione(String codiceStazione) {

        DatiStazione datiStazione;

        URL input;
        try {
            XStream xStream = new XStream();
            xStream.ignoreUnknownElements();
            xStream.alias("datiOggi", DatiStazione.class);
            xStream.alias("temperatura_aria", TemperaturaAriaStazione.class);
            xStream.alias("precipitazione", PrecipitazioneStazione.class);
            xStream.autodetectAnnotations(true);
            xStream.registerConverter(new com.gmail.fattazzo.meteo.parser.xml.converter.DateConverter("yyyy/MM/dd"));

            datiStazione = loadXml(MeteoSettings.DATI_STAZIONI_XML + codiceStazione, xStream);
            datiStazione.setCodiceStazione(codiceStazione);
        } catch (Exception e) {
            try {
                input = new URL(MeteoSettings.DATI_STAZIONI_XML + codiceStazione);
                HttpURLConnection huc = (HttpURLConnection) input.openConnection();
                huc.setConnectTimeout(TIME_OUT);
                huc.setReadTimeout(TIME_OUT);
                huc.setRequestMethod("GET");
                huc.connect();
                int responseCode = huc.getResponseCode();

                // se il file per la stazione esiste e c'è stato un errore nel parse ritorno un null
                switch (responseCode) {
                case HttpURLConnection.HTTP_OK:
                    datiStazione = null;
                    break;
                default:
                    throw new MeteoRuntimeException(e);
                }
            } catch (Exception e1) {
                throw new MeteoRuntimeException(e);
            }
        }

        return datiStazione;
    }

}
