package com.gmail.fattazzo.meteo.settings.widget.webcam;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;

import com.gmail.fattazzo.meteo.settings.MeteoSettings;
import com.gmail.fattazzo.meteo.settings.MeteoSettingsManager;
import com.gmail.fattazzo.meteo.settings.widget.WidgetSettingsManager;
import com.gmail.fattazzo.meteo.widget.providers.MeteoWidgetProvider;

/**
 *
 * @author fattazzo
 *
 *         date: 24/lug/2015
 *
 */
public class WebcamWidgetsSettingsManager extends MeteoSettingsManager implements WidgetSettingsManager {

    private Context context;

    /**
     * Costruttore.
     *
     * @param context
     *            context
     */
    public WebcamWidgetsSettingsManager(final Context context) {
        super(context);
        this.context = context;
    }

    /**
     * @return widget background
     */
    @Override
    public int getBackground() {

        String background = getPrefs().getString(MeteoSettings.WIDGETS_BACKGROUND, "widget_shape_transparent_40_black");

        return context.getResources().getIdentifier(background, "drawable", context.getPackageName());
    }

    /**
     * @return widget text color
     */
    @Override
    public int getTextColor() {
        return getPrefs().getInt(MeteoSettings.WIDGETS_TEXT_COLOR, 0xFFFFFFFF);
    }

    /**
     * @return widget update interval
     */
    @Override
    public int getUpdateInterval() {
        return Integer.parseInt(getPrefs().getString(MeteoSettings.WIDGETS_WEBCAM_UPDATE_INTERVAL, "1800000"));
    }

    /**
     * @return id delle webcam da visualizzare nel widget
     */
    public @NonNull List<Integer> getWebcamWidgetIds() {

        List<Integer> ids = new ArrayList<>();

        String stringIds = getPrefs().getString(MeteoSettings.WIDGETS_WEBCAM_IDS, "");
        String[] splittedIds = stringIds.split(",");

        for (String id : splittedIds) {
            if (!"".equals(id)) {
                ids.add(Integer.valueOf(id));
            }
        }

        return ids;
    }

    /**
     * Aggiorna la lista degli id da visualizzare aggiungendo o togliendo il nuovo id a seconda che questo esista già o
     * no.
     *
     * @param idUpdate
     *            id da sincronizzare
     */
    public void updateWebcamWidgetIds(@NonNull Integer idUpdate) {

        List<Integer> ids = getWebcamWidgetIds();
        int idx = ids.indexOf(idUpdate);
        if (idx == -1) {
            ids.add(idUpdate);
        } else {
            ids.remove(idx);
        }

        String stringIds = "";
        String separator = "";

        for (Integer id : ids) {
            stringIds = stringIds + separator + String.valueOf(id);
            separator = ",";
        }

        getPrefs().edit().putString(MeteoSettings.WIDGETS_WEBCAM_IDS, stringIds).commit();
        Intent intent = new Intent(MeteoWidgetProvider.UPDATE_SETTINGS);
        context.sendBroadcast(intent);
    }

}
