/**
 *
 */
package com.gmail.fattazzo.meteo.db;

import java.util.ArrayList;
import java.util.List;

import com.gmail.fattazzo.meteo.db.structure.StazioneMeteoTable;
import com.gmail.fattazzo.meteo.db.structure.StazioneValangheTable;
import com.gmail.fattazzo.meteo.db.structure.TableStructure;

/**
 * @author Fattazzo
 *
 *         Date 15/mar/2014
 *
 */
public final class MeteoDatabase {

    public static final int DATABASE_VERSION = 2;

    public static final String DATABASE_NAME = "meteo";

    public static List<TableStructure<?>> tables = new ArrayList<>();
    public static final StazioneMeteoTable STAZIONI_METEO_TABLE;

    public static final StazioneValangheTable STAZIONI_VALANGHE_TABLE;

    static {
        STAZIONI_METEO_TABLE = new StazioneMeteoTable();
        STAZIONI_VALANGHE_TABLE = new StazioneValangheTable();

        tables.add(STAZIONI_METEO_TABLE);
        tables.add(STAZIONI_VALANGHE_TABLE);
    }

    /**
     * Create all tables sql statement.
     *
     * @return sql create
     */
    public static String[] createTablesSQL() {
        String[] tablesSQL = new String[tables.size()];
        for (int i = 0; i < tables.size(); i++) {
            tablesSQL[i] = tables.get(i).getCreateTableSQL();

        }
        return tablesSQL;
    }

    /**
     * Delete all tables sql statement.
     *
     * @return sql delete
     */
    public static String[] dropTablesSQL() {
        String[] tablesSQL = new String[tables.size()];
        for (int i = 0; i < tables.size(); i++) {
            tablesSQL[i] = tables.get(i).getDropTableSQL();

        }
        return tablesSQL;
    }

    /**
     * Costruttore.
     */
    private MeteoDatabase() {
        super();
    }
}
