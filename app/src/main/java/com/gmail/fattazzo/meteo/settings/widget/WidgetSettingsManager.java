package com.gmail.fattazzo.meteo.settings.widget;

/**
 *
 * @author fattazzo
 *
 *         date: 24/lug/2015
 *
 */
public interface WidgetSettingsManager {

    /**
     * @return widget background
     */
    int getBackground();

    /**
     * @return widget text color
     */
    int getTextColor();

    /**
     * @return widget update interval
     */
    int getUpdateInterval();
}
