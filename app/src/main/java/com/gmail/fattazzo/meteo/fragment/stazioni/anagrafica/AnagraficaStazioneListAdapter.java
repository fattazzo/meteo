package com.gmail.fattazzo.meteo.fragment.stazioni.anagrafica;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.gmail.fattazzo.meteo.R;
import com.gmail.fattazzo.meteo.domain.stazioni.anagrafica.StazioneMeteo;

/**
 *
 * @author fattazzo
 *
 *         date: 28/lug/2014
 *
 */
public class AnagraficaStazioneListAdapter extends ArrayAdapter<StazioneMeteo> {

    /**
     *
     * @author fattazzo
     *
     *         date: 13/lug/2015
     *
     */
    static class ViewHolderItem {
        protected TextView nomeTextView;
        protected TextView codiceTextView;
    }

    private LayoutInflater layoutInflater = null;

    /**
     * Costruttore.
     *
     * @param context
     *            context
     * @param objects
     *            stazioni meteo
     */
    public AnagraficaStazioneListAdapter(final Context context, final List<StazioneMeteo> objects) {
        super(context, R.layout.anag_stazione_list_adapter, objects);
        this.layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolderItem viewHolder;

        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.anag_stazione_list_adapter, (ViewGroup) null);

            viewHolder = new ViewHolderItem();
            viewHolder.nomeTextView = (TextView) convertView.findViewById(R.id.anag_stazione_nome_label);
            viewHolder.codiceTextView = (TextView) convertView.findViewById(R.id.anag_stazione_codice_label);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolderItem) convertView.getTag();
        }

        StazioneMeteo stazioneMeteo = getItem(position);

        viewHolder.nomeTextView.setText(stazioneMeteo.getNome());
        viewHolder.codiceTextView.setText(stazioneMeteo.getCodice());
        return convertView;
    }

}
