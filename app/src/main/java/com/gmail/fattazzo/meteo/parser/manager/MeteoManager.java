package com.gmail.fattazzo.meteo.parser.manager;

import android.content.Context;

import com.gmail.fattazzo.meteo.R;
import com.gmail.fattazzo.meteo.domain.json.previsione.PrevisioneLocalita;
import com.gmail.fattazzo.meteo.domain.stazioni.anagrafica.StazioneMeteo;
import com.gmail.fattazzo.meteo.domain.stazioni.dati.DatiStazione;
import com.gmail.fattazzo.meteo.domain.valanghe.anagrafica.StazioneValanghe;
import com.gmail.fattazzo.meteo.domain.valanghe.bollettino.Bollettino;
import com.gmail.fattazzo.meteo.parser.BollettinoGeneraleParser;
import com.gmail.fattazzo.meteo.parser.BollettinoGeneraleParser_;
import com.gmail.fattazzo.meteo.parser.xml.BollettinoValangheXmlParser;
import com.gmail.fattazzo.meteo.parser.xml.StazioniMeteoXmlParser;
import com.gmail.fattazzo.meteo.parser.xml.StazioniValangheXmlParser;
import com.gmail.fattazzo.meteo.utils.Closure;

import java.util.List;

/**
 * @author fattazzo
 *         <p>
 *         date: 13/lug/2015
 */
public class MeteoManager {

    private final Context context;

    /**
     * Costruttore.
     *
     * @param context context
     */
    public MeteoManager(final Context context) {
        super();
        this.context = context;

    }

    /**
     * Carica l'anagrafica delle stazioni meteo.
     *
     * @param closure closure da eseguire al caricamento delle stazioni
     */
    public void caricaAnagraficaStazioni(final Closure<List<StazioneMeteo>> closure) {

        new MeteoAsyncTask<Void, List<StazioneMeteo>>(context, R.string.list_stations_loading_dialog, closure) {

            @Override
            public List<StazioneMeteo> executeInBackGround(Void... params) {
                StazioniMeteoXmlParser parser = new StazioniMeteoXmlParser();
                return parser.caricaAnagrafica();
            }
        }.execute();
    }

    /**
     * Carica l'anagrafica delle stazioni valanghe.
     *
     * @param closure closure da eseguire al caricamento delle stazioni
     */
    public void caricaAnagraficaStazioniValanghe(final Closure<List<StazioneValanghe>> closure) {

        new MeteoAsyncTask<Void, List<StazioneValanghe>>(context, R.string.list_stations_loading_dialog, closure) {

            @Override
            public List<StazioneValanghe> executeInBackGround(Void... params) {
                StazioniValangheXmlParser parser = new StazioniValangheXmlParser();
                return parser.caricaAnagrafica();
            }
        }.execute();
    }

    /**
     * Carica il bollettino generale.
     *
     * @param closure           closure da eseguire al caricamento del bollettino
     * @param showLoadingDialog <code>true</code> per visualizzare il dialogo di caricamento
     * @param suppressException se <code>true</code> non rilancia eccezioni ( no network, ecc...)
     */
    public void caricaBollettinoGeneraleLocalita(final Closure<PrevisioneLocalita> closure, final boolean showLoadingDialog,
                                                 final boolean suppressException) {

        new MeteoAsyncTask<Void, PrevisioneLocalita>(context, R.string.forecast_loading_dialog, closure, showLoadingDialog,
                suppressException) {

            @Override
            public PrevisioneLocalita executeInBackGround(Void... params) {
                BollettinoGeneraleParser parser = BollettinoGeneraleParser_.getInstance_(context);

                return parser.caricaPrevisione();
            }
        }.execute();
    }

    /**
     * Carica il bollettino valanghe.
     *
     * @param closure closure da eseguire al caricamento del bollettino
     */
    public void caricaBollettinoValanghe(final Closure<Bollettino> closure) {

        new MeteoAsyncTask<Void, Bollettino>(context, R.string.avalanche_loading_dialog, closure) {

            @Override
            public Bollettino executeInBackGround(Void... params) {
                BollettinoValangheXmlParser parser = new BollettinoValangheXmlParser();

                return parser.caricaBollettino();
            }
        }.execute();
    }

    /**
     * Carica i dati della stazione di riferimento.
     *
     * @param codiceStazione codice della stazione
     * @param closure        closure da eseguire al caricamento del bollettino
     */
    public void caricaDatiStazione(String codiceStazione, final Closure<DatiStazione> closure) {

        new MeteoAsyncTask<String, DatiStazione>(context, R.string.data_station_loading_dialog, closure) {

            @Override
            public DatiStazione executeInBackGround(String... params) {
                StazioniMeteoXmlParser stazioniMeteoXmlParser = new StazioniMeteoXmlParser();
                return stazioniMeteoXmlParser.caricaDatiStazione(params[0]);
            }

        }.execute(codiceStazione);
    }

    /**
     * Carica i dati neve delle stazioni.
     *
     * @param closure closure da eseguire al caricamento delle stazioni
     */
    public void caricaDatiStazioniNeve(
            final Closure<List<com.gmail.fattazzo.meteo.domain.valanghe.dati.DatiStazione>> closure) {

        new MeteoAsyncTask<Void, List<com.gmail.fattazzo.meteo.domain.valanghe.dati.DatiStazione>>(context,
                R.string.snow_stations_data__loading_dialog, closure) {

            @Override
            public List<com.gmail.fattazzo.meteo.domain.valanghe.dati.DatiStazione> executeInBackGround(Void... params) {
                StazioniValangheXmlParser parser = new StazioniValangheXmlParser();
                return parser.caricaDatiStazioneNeve();
            }
        }.execute();
    }

}
