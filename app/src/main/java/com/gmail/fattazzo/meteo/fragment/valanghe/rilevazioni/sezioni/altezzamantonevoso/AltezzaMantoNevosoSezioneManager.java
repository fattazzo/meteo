package com.gmail.fattazzo.meteo.fragment.valanghe.rilevazioni.sezioni.altezzamantonevoso;

import android.view.View;

import com.gmail.fattazzo.meteo.R;
import com.gmail.fattazzo.meteo.domain.valanghe.dati.DatiStazione;
import com.gmail.fattazzo.meteo.fragment.valanghe.rilevazioni.sezioni.RilevazioneSezione;
import com.gmail.fattazzo.meteo.fragment.valanghe.rilevazioni.sezioni.RilevazioneSezioneListAdapter;
import com.gmail.fattazzo.meteo.fragment.valanghe.rilevazioni.view.AbstractSezioneManager;
import com.gmail.fattazzo.meteo.utils.ExpandableHeightListView;

import java.util.ArrayList;
import java.util.List;

/**
 * @author fattazzo
 *         <p/>
 *         date: 03/feb/2016
 */
public class AltezzaMantoNevosoSezioneManager extends AbstractSezioneManager {

    /**
     * Costruttore.
     *
     * @param view view
     */
    public AltezzaMantoNevosoSezioneManager(final View view) {
        super(view);
    }

    @Override
    protected void clearContent() {
        ExpandableHeightListView condTempoList = (ExpandableHeightListView) view
                .findViewById(R.id.neve_altezzamantonevoso_list);
        condTempoList.setAdapter(null);
    }

    @Override
    protected int getHeaderViewId() {
        return R.id.anag_stazione_neve_altezzamantonevoso_layout;
    }

    @Override
    protected int getToggleViewId() {
        return R.id.anag_stazione_neve_altezzamantonevosoData_layout;
    }

    @Override
    protected void loadContent() {
        List<RilevazioneSezione> altezzaMantaNevosoList = new ArrayList<>();
        for (DatiStazione dato : getDatiStazione()) {
            RilevazioneSezione altezzaMantaNevoso = new RilevazioneSezione();
            altezzaMantaNevoso.setData(dato.getDataMisurazione());
            String descrizione = getLegendaDatiNeveManager().parseAltezzaMantoNevoso(dato.getAltezzaNeve());
            altezzaMantaNevoso.setDescrizione(descrizione);
            altezzaMantaNevosoList.add(altezzaMantaNevoso);
        }

        RilevazioneSezioneListAdapter adapter = new RilevazioneSezioneListAdapter(view.getContext(), altezzaMantaNevosoList);

        ExpandableHeightListView listView = (ExpandableHeightListView) view.findViewById(R.id.neve_altezzamantonevoso_list);
        listView.setAdapter(adapter);
        listView.setExpanded(true);

    }
}
