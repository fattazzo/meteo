package com.gmail.fattazzo.meteo.domain.webcam;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

/**
 *
 * @author fattazzo
 *
 *         date: 13/lug/2015
 *
 */
@XStreamAlias("webcamList")
public class Webcams {

    @XStreamImplicit(itemFieldName = "zone")
    private List<ZoneWebcam> zoneWebcams;

    {
        zoneWebcams = new ArrayList<>();
    }

    /**
     * Restituisce tutte le webcam.
     *
     * @return webcam
     */
    public List<Webcam> getWebcams() {
        List<Webcam> webcams = new ArrayList<>();
        for (ZoneWebcam zoneWebcam : zoneWebcams) {
            webcams.addAll(zoneWebcam.getWebcams());
        }

        Collections.sort(webcams);
        return webcams;
    }

    /**
     * Restituisce tutte le webcam della zona richiesta.
     *
     * @param zone
     *            zona
     * @return webcam
     */
    public List<Webcam> getWebcams(String zone) {
        for (ZoneWebcam zoneWebcam : zoneWebcams) {
            if (zoneWebcam.getDescrizione().equals(zone)) {
                List<Webcam> webcams = zoneWebcam.getWebcams();
                Collections.sort(webcams);
                return webcams;
            }
        }

        return new ArrayList<>();
    }

    /**
     * @return the zoneWebcams
     */
    public List<ZoneWebcam> getZoneWebcams() {
        return zoneWebcams;
    }

    /**
     * @param zoneWebcams
     *            the zoneWebcams to set
     */
    public void setZoneWebcams(List<ZoneWebcam> zoneWebcams) {
        this.zoneWebcams = zoneWebcams;
    }

}
