package com.gmail.fattazzo.meteo.domain.stazioni.anagrafica;

import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 *
 * @author fattazzo
 *
 *         date: 13/lug/2015
 *
 */
public class Stazioni {

    @XStreamAlias("anagrafica")
    private List<StazioneMeteo> stazioniMeteo;

    /**
     * @return the stazioniMeteo
     */
    public List<StazioneMeteo> getStazioniMeteo() {
        return stazioniMeteo;
    }

    /**
     * @param stazioniMeteo
     *            the stazioniMeteo to set
     */
    public void setStazioniMeteo(List<StazioneMeteo> stazioniMeteo) {
        this.stazioniMeteo = stazioniMeteo;
    }

}
