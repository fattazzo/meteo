package com.gmail.fattazzo.meteo.domain.webcam;

import java.io.Serializable;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;

/**
 *
 * @author fattazzo
 *
 *         date: 13/lug/2015
 *
 */
@XStreamAlias("link")
public class WebcamLink implements Serializable {

    /**
     *
     * @author fattazzo
     *
     *         date: 13/lug/2015
     *
     */
    public enum ViewerType {
        IMAGE, WEBVIEW
    }

    private static final long serialVersionUID = 47099054208386533L;

    @XStreamAlias("value")
    @XStreamAsAttribute
    private String link;

    @XStreamAlias("viewer")
    @XStreamAsAttribute
    private String viewer;

    /**
     * @return the link
     */
    public String getLink() {
        return link;
    }

    /**
     * @return the viewer
     */
    public String getViewer() {
        return viewer;
    }

    /**
     * @return the viewer type
     */
    public ViewerType getViewerType() {

        ViewerType viewerType = ViewerType.WEBVIEW;
        if (viewer != null && "image".equals(viewer)) {
            viewerType = ViewerType.IMAGE;
        }
        return viewerType;
    }

    /**
     * @param link
     *            the link to set
     */
    public void setLink(String link) {
        this.link = link;
    }

    /**
     * @param viewer
     *            the viewer to set
     */
    public void setViewer(String viewer) {
        this.viewer = viewer;
    }

    /*
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "WebcamLink [link=" + link + ", viewer=" + viewer + "]";
    }
}
