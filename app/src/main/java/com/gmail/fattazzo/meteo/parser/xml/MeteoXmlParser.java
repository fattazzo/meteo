package com.gmail.fattazzo.meteo.parser.xml;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import com.gmail.fattazzo.meteo.exception.handler.MeteoRuntimeException;
import com.thoughtworks.xstream.XStream;

/**
 *
 * @author fattazzo
 *
 *         date: 13/lug/2015
 *
 */
public abstract class MeteoXmlParser {

    protected static final int TIME_OUT = 15000; // 15sec

    /**
     * Carica l'input stream dall url specificato.
     *
     * @param stringUrl
     *            url
     * @return input stream caricato
     * @throws Exception
     *             eccezione generica sul caricamento dell'input stream
     */
    public InputStream getInputStreamFromURL(String stringUrl) throws Exception {
        URL input = new URL(stringUrl);
        HttpURLConnection conn = (HttpURLConnection) input.openConnection();
        conn.setConnectTimeout(TIME_OUT);
        conn.setReadTimeout(TIME_OUT);

        return conn.getInputStream();
    }

    /**
     * Carica l'xml in base all'url e all'xStream definito.
     *
     * @param xmlUrl
     *            url del file xml
     * @param xStream
     *            xStream
     * @param <T>
     *            tipo di dato caricato
     * @return dato caricato in base al parse del file xml tramite l'xStream
     */
    @SuppressWarnings("unchecked")
    public <T> T loadXml(String xmlUrl, XStream xStream) {

        T result = null;

        try {
            InputStream inputStream = getInputStreamFromURL(xmlUrl);

            result = (T) xStream.fromXML(inputStream);
        } catch (Exception e) {
            throw new MeteoRuntimeException(e);
        }

        return result;
    }
}
