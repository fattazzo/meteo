package com.gmail.fattazzo.meteo.fragment.stazioni.rilevazioni;

/**
 *
 * @author fattazzo
 *
 *         date: 18/lug/2014
 *
 */
public enum TipoDatoStazione {
    TEMPERATURA, PRECIPITAZIONE
}
