package com.gmail.fattazzo.meteo.fragment.valanghe.anagrafica;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import android.database.ContentObserver;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;

import com.gmail.fattazzo.meteo.R;
import com.gmail.fattazzo.meteo.db.handlers.MeteoDatabaseHandler;
import com.gmail.fattazzo.meteo.db.structure.StazioneValangheTable;
import com.gmail.fattazzo.meteo.domain.valanghe.anagrafica.StazioneValanghe;
import com.gmail.fattazzo.meteo.parser.manager.MeteoManager;
import com.gmail.fattazzo.meteo.utils.Closure;
import com.gmail.fattazzo.meteo.utils.ExpandableHeightListView;

/**
 *
 * @author fattazzo
 *
 *         date: 17/lug/2014
 *
 */
public class AnagraficaFragment extends Fragment implements OnClickListener {

    private View view;

    private MeteoManager meteoManager;
    private MeteoDatabaseHandler databaseHandler;

    private List<StazioneValanghe> stazioniValanghe = null;

    /**
     * Carica le stazioni meteo.
     */
    private void loadStazioniValanghe() {

        int countStazioni = databaseHandler.getCountStazioniValanghe();

        // se non ho ancora stazioni valanghe nel database le carico
        if (countStazioni == 0) {
            meteoManager.caricaAnagraficaStazioniValanghe(new Closure<List<StazioneValanghe>>() {

                @Override
                public void execute(List<StazioneValanghe> stazioni) {
                    if (stazioni != null) {
                        databaseHandler.salvaStazioniValanghe(stazioni);
                    }
                }
            });
        } else {
            loadTableData();
        }
    }

    /**
     * Carica tutte le stazioni meteo nell'adapter della tabella.
     */
    private void loadTableData() {
        new AsyncTask<Void, StazioneValanghe, Void>() {

            private ExpandableHeightListView listView;

            @Override
            protected Void doInBackground(Void... params) {
                stazioniValanghe = databaseHandler.caricaStazioniValanghe();
                for (StazioneValanghe stazione : stazioniValanghe) {
                    publishProgress(stazione);
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void result) {
                ((AnagraficaStazioneValangheListAdapter) listView.getAdapter()).addAll(stazioniValanghe);
            }

            @Override
            protected void onPreExecute() {
                AnagraficaStazioneValangheListAdapter adapter = new AnagraficaStazioneValangheListAdapter(
                        view.getContext(), new ArrayList<StazioneValanghe>());

                listView = (ExpandableHeightListView) view.findViewById(R.id.anag_valanghe_list);
                listView.setAdapter(adapter);
                listView.setExpanded(true);
            }
        }.execute();
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
        case R.id.anag_valanghe_refresh_button:
            databaseHandler.cancellaStazioniValanghe();
            loadStazioniValanghe();
            break;

        default:
            break;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (container != null) {
            container.removeAllViews();
        }

        if (view != null && view.getParent() != null) {
            ((ViewGroup) view.getParent()).removeView(view);
        } else {
            view = inflater.inflate(R.layout.fragment_anagrafica_stazioni_valanghe, container, false);
        }

        meteoManager = new MeteoManager(view.getContext());
        databaseHandler = MeteoDatabaseHandler.getInstance(view.getContext());

        registerStazioniValangheObserver();

        loadStazioniValanghe();

        Button refreshButton = (Button) view.findViewById(R.id.anag_valanghe_refresh_button);
        refreshButton.setOnClickListener(this);

        return view;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        try {
            Field childFragmentManager = Fragment.class.getDeclaredField("mChildFragmentManager");
            childFragmentManager.setAccessible(true);
            childFragmentManager.set(this, null);
        } catch (NoSuchFieldException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Registra un observer per catturare tutti gli eventi sulla tabella delle stazioni meteo.
     */
    private void registerStazioniValangheObserver() {
        view.getContext().getContentResolver()
                .registerContentObserver(StazioneValangheTable.URI, false, new ContentObserver(new Handler()) {
                    @Override
                    public void onChange(boolean selfChange) {
                        loadTableData();
                    }
                });
    }
}
