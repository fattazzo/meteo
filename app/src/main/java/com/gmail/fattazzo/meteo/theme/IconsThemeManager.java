package com.gmail.fattazzo.meteo.theme;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.widget.ImageView;

import com.gmail.fattazzo.meteo.settings.MeteoSettingsManager;
import com.gmail.fattazzo.meteo.utils.LoadBitmapTask;
import com.gmail.fattazzo.meteo.utils.LoadImagefromUrl;

/**
 * @author fattazzo
 *         <p/>
 *         date: 14/ott/2014
 */
public final class IconsThemeManager {

    public static final String TRENTINO_MAP_PNG = "trentino.png";

    /**
     * @param context context
     * @return instanza di {@link IconsThemeManager}.
     */
    public static IconsThemeManager getInstance(Context context) {
        if (iconsThemeMamager == null) {
            iconsThemeMamager = new IconsThemeManager(context);
        }
        return iconsThemeMamager;
    }

    private static IconsThemeManager iconsThemeMamager;

    private Context context;

    private Map<String, Bitmap> cachedIcons;

    private MeteoSettingsManager meteoSettingsManager;

    /**
     * Costruttore.
     *
     * @param context context
     */
    private IconsThemeManager(final Context context) {
        this.context = context;

        this.cachedIcons = new HashMap<>();
        this.meteoSettingsManager = new MeteoSettingsManager(context);
    }

    /**
     * Applica la bitmap in asincrono caricandola dall'url.
     *
     * @param imageView image view
     * @param iconUrl   url dell'icona
     */
    public void applyIconAsync(ImageView imageView, String iconUrl) {

        try {
            String iconName = iconUrl.substring(iconUrl.lastIndexOf("/") + 1);

            imageView.setImageBitmap(getBitmapFromCache(iconName));
        } catch (Exception e) {
            new LoadImagefromUrl().execute(imageView, iconUrl);
        }
    }

    /**
     * Applica alla view l'immagine della mappa del trentino.
     *
     * @param imageView image view
     */
    public void applyTrentinoImage(ImageView imageView) {

        try {
            imageView.setImageBitmap(getBitmapFromCache(TRENTINO_MAP_PNG));
        } catch (Exception e) {
            imageView.setImageBitmap(null);
        }
    }

    /**
     * Ripulisce la cache delle icone caricate.
     */
    public void clearCache() {
        cachedIcons = new HashMap<>();
    }

    /**
     * Carica la bitmap dell'icona dalla cache.
     *
     * @param iconName nome icona
     * @return bitmap
     * @throws Exception eccezione generica
     */
    private Bitmap getBitmapFromCache(String iconName) throws Exception {

        // verifico se ho l'icona in cache
        if (cachedIcons.get(iconName) == null) {
            InputStream is = context.getResources().getAssets().open("iconsTheme/" + meteoSettingsManager.getIconsThemePath() + "/" + iconName);
            Bitmap image = BitmapFactory.decodeStream(is);

            // metto l'icona in cache
            cachedIcons.put(iconName, image);
        }

        return cachedIcons.get(iconName);
    }

    /**
     * Restituisce l'icona in base all'url.
     *
     * @param iconUrl url dell'icona
     * @return bitmap creata
     */
    public Bitmap getIcon(String iconUrl) {

        Bitmap bitmapResult;

        try {
            String iconName = iconUrl.substring(iconUrl.lastIndexOf("/") + 1);
            bitmapResult = getBitmapFromCache(iconName);
        } catch (Exception e) {
            try {
                LoadBitmapTask loadBitbamTask = (LoadBitmapTask) new LoadBitmapTask().execute(iconUrl);
                bitmapResult = loadBitbamTask.get();
            } catch (Exception e1) {
                bitmapResult = null;
            }
        }

        return bitmapResult;
    }

    /**
     * Restituisce l'icona della mappa det trentino.
     *
     * @return bitmap creata
     */
    public Bitmap getTrentinoIcon() {

        Bitmap bitmapResult;

        try {
            bitmapResult = getBitmapFromCache(TRENTINO_MAP_PNG);
        } catch (Exception e) {
            bitmapResult = null;
        }

        return bitmapResult;
    }

}
