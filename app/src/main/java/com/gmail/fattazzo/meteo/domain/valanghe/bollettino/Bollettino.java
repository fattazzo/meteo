package com.gmail.fattazzo.meteo.domain.valanghe.bollettino;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 *
 * @author fattazzo
 *
 *         date: 10/feb/2016
 *
 */
public class Bollettino {

    @XStreamAlias("giornoEmissione")
    private String giornoEmissione;

    @XStreamAlias("prossimaEmissione")
    private String prossimaEmissione;

    @XStreamAlias("codicePrevisore")
    private String codicePrevisore;

    @XStreamAlias("evoluzioneTempo")
    private String evoluzioneTempo;

    @XStreamAlias("situazione")
    private String situazione;

    @XStreamAlias("mappapericolo")
    private String mappaPericolo;

    @XStreamAlias("mappaneve")
    private String mappaNeve;

    @XStreamAlias("puntipericolosi")
    private String puntiPericolosi;

    @XStreamAlias("ultimanevicata")
    private String ultimaNevicata;

    @XStreamAlias("vento")
    private String vento;

    @XStreamAlias("evoluzioneprox")
    private String evoluzioneProx;

    private List<Giorno> giorni;

    /**
     * @return the codicePrevisore
     */
    public String getCodicePrevisore() {
        return codicePrevisore;
    }

    /**
     * @return the evoluzioneProx
     */
    public String getEvoluzioneProx() {
        return evoluzioneProx;
    }

    /**
     * @return the evoluzioneTempo
     */
    public String getEvoluzioneTempo() {
        return evoluzioneTempo;
    }

    /**
     * @return the giorni
     */
    public List<Giorno> getGiorni() {
        return giorni;
    }

    /**
     * @param locale
     *            locale
     * @return the giornoEmissione
     */
    public String getGiornoEmissione(Locale locale) {
        DateFormat dateFormat = new SimpleDateFormat("EEEE dd MMMM yyyy", Locale.ITALIAN);
        try {
            Date data = dateFormat.parse(giornoEmissione);
            return new SimpleDateFormat("EEEE dd MMMM yyyy", locale).format(data);
        } catch (ParseException e) {
            return giornoEmissione;
        }
    }

    /**
     * @return the mappaNeve
     */
    public String getMappaNeve() {
        return mappaNeve;
    }

    /**
     * @return the mappaPericolo
     */
    public String getMappaPericolo() {
        return mappaPericolo;
    }

    /**
     * @return the prossimaEmissione
     */
    public String getProssimaEmissione() {
        return prossimaEmissione;
    }

    /**
     * @return the puntiPericolosi
     */
    public String getPuntiPericolosi() {
        return puntiPericolosi;
    }

    /**
     * @return the situazione
     */
    public String getSituazione() {
        return situazione;
    }

    /**
     * @param locale
     *            locale
     * @return the ultimaNevicata
     */
    public String getUltimaNevicata(Locale locale) {
        DateFormat dateFormat = new SimpleDateFormat("dd MMMM yyyy", Locale.ITALIAN);
        try {
            Date data = dateFormat.parse(ultimaNevicata);
            return new SimpleDateFormat("dd MMMM yyyy", locale).format(data);
        } catch (ParseException e) {
            return ultimaNevicata;
        }
    }

    /**
     * @return the vento
     */
    public String getVento() {
        return vento;
    }

    /**
     * @param codicePrevisore
     *            the codicePrevisore to set
     */
    public void setCodicePrevisore(String codicePrevisore) {
        this.codicePrevisore = codicePrevisore;
    }

    /**
     * @param evoluzioneProx
     *            the evoluzioneProx to set
     */
    public void setEvoluzioneProx(String evoluzioneProx) {
        this.evoluzioneProx = evoluzioneProx;
    }

    /**
     * @param evoluzioneTempo
     *            the evoluzioneTempo to set
     */
    public void setEvoluzioneTempo(String evoluzioneTempo) {
        this.evoluzioneTempo = evoluzioneTempo;
    }

    /**
     * @param giorni
     *            the giorni to set
     */
    public void setGiorni(List<Giorno> giorni) {
        this.giorni = giorni;
    }

    /**
     * @param giornoEmissione
     *            the giornoEmissione to set
     */
    public void setGiornoEmissione(String giornoEmissione) {
        this.giornoEmissione = giornoEmissione;
    }

    /**
     * @param mappaNeve
     *            the mappaNeve to set
     */
    public void setMappaNeve(String mappaNeve) {
        this.mappaNeve = mappaNeve;
    }

    /**
     * @param mappaPericolo
     *            the mappaPericolo to set
     */
    public void setMappaPericolo(String mappaPericolo) {
        this.mappaPericolo = mappaPericolo;
    }

    /**
     * @param prossimaEmissione
     *            the prossimaEmissione to set
     */
    public void setProssimaEmissione(String prossimaEmissione) {
        this.prossimaEmissione = prossimaEmissione;
    }

    /**
     * @param puntiPericolosi
     *            the puntiPericolosi to set
     */
    public void setPuntiPericolosi(String puntiPericolosi) {
        this.puntiPericolosi = puntiPericolosi;
    }

    /**
     * @param situazione
     *            the situazione to set
     */
    public void setSituazione(String situazione) {
        this.situazione = situazione;
    }

    /**
     * @param ultimaNevicata
     *            the ultimaNevicata to set
     */
    public void setUltimaNevicata(String ultimaNevicata) {
        this.ultimaNevicata = ultimaNevicata;
    }

    /**
     * @param vento
     *            the vento to set
     */
    public void setVento(String vento) {
        this.vento = vento;
    }
}
