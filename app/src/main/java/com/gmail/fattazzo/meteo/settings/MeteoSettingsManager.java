/**
 *
 */
package com.gmail.fattazzo.meteo.settings;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * @author Fattazzo
 *         <p>
 *         Date 12/lug/2014
 */
public class MeteoSettingsManager {

    private final SharedPreferences prefs;

    /**
     * Costruttore.
     *
     * @param context context
     */
    public MeteoSettingsManager(final Context context) {
        super();
        this.prefs = PreferenceManager.getDefaultSharedPreferences(context);
    }

    /**
     * @return restituisce l'ultimo nome della versione utilizzata
     */
    public String getLastRunVersionName() {

        return getPrefs().getString(MeteoSettings.LAST_RUN_VERSION_NAME, "0");
    }

    /**
     * @return restituisce il path da utilizzare per caricare le icone
     */
    public String getIconsThemePath() {
        return getPrefs().getString(MeteoSettings.ICONS_THEME, "meteo_trentino");
    }

    /**
     * @return the prefs
     */
    public SharedPreferences getPrefs() {
        return prefs;
    }
}
