package com.gmail.fattazzo.meteo.parser.xml;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;

import android.content.Context;
import android.util.Log;

import com.gmail.fattazzo.meteo.domain.webcam.Webcam;
import com.gmail.fattazzo.meteo.domain.webcam.WebcamLink;
import com.gmail.fattazzo.meteo.domain.webcam.Webcams;
import com.gmail.fattazzo.meteo.domain.webcam.ZoneWebcam;
import com.gmail.fattazzo.meteo.settings.widget.webcam.WebcamWidgetsSettingsManager;
import com.thoughtworks.xstream.XStream;

/**
 *
 * @author fattazzo
 *
 *         date: 13/lug/2015
 *
 */
public class WebcamXmlParser {

    public static final String TAG = "WebcamXmlParser";

    /**
     * Carica le webcam presenti.
     *
     * @param context
     *            context
     * @return webcam caricate
     */
    public Webcams caricaWebcam(Context context) {

        Webcams webcams = null;

        InputStream stream = null;
        InputStreamReader inputReader = null;
        try {
            stream = context.getAssets().open("webcam.xml");
            inputReader = new InputStreamReader(stream);

            XStream xStream = new XStream();
            xStream.processAnnotations(Webcams.class);
            xStream.processAnnotations(ZoneWebcam.class);
            xStream.processAnnotations(Webcam.class);
            xStream.processAnnotations(WebcamLink.class);
            webcams = (Webcams) xStream.fromXML(inputReader);

        } catch (Exception e) {
            Log.e(TAG, "Errore durante la letture del file delle webcam");
            webcams = new Webcams();
        } finally {
            try {
                if (stream != null) {
                    stream.close();
                }
                if (inputReader != null) {
                    inputReader.close();
                }
            } catch (IOException e) {
                Log.e(TAG, "Errore durante la chiusura del file");
            }
        }

        WebcamWidgetsSettingsManager webcamWidgetsSettingsManager = new WebcamWidgetsSettingsManager(context);
        List<Integer> webcamWidgetIds = webcamWidgetsSettingsManager.getWebcamWidgetIds();
        for (Webcam webcam : webcams.getWebcams()) {
            webcam.setShowInWidget(webcamWidgetIds.indexOf(webcam.getId()) != -1);
        }

        return webcams;
    }
}
