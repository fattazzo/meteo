package com.gmail.fattazzo.meteo.domain.stazioni.anagrafica;

import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 *
 * @author fattazzo
 *
 *         date: 13/lug/2015
 *
 */
public class StazioneMeteo {

    @XStreamAlias("codice")
    private String codice;

    @XStreamAlias("nome")
    private String nome;

    @XStreamAlias("nomebreve")
    private String nomeBreve;

    @XStreamAlias("quota")
    private Integer quota;

    @XStreamAlias("latitudine")
    private Double latitudine;

    @XStreamAlias("longitudine")
    private Double longitudine;

    @XStreamAlias("est")
    private Double est;

    @XStreamAlias("north")
    private Double nord;

    /**
     * @return the codice
     */
    public String getCodice() {
        return codice;
    }

    /**
     * @return the est
     */
    public Double getEst() {
        return est;
    }

    /**
     * @return the latitudine
     */
    public Double getLatitudine() {
        return latitudine;
    }

    /**
     * @return the longitudine
     */
    public Double getLongitudine() {
        return longitudine;
    }

    /**
     * @return the nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * @return the nomeBreve
     */
    public String getNomeBreve() {
        return nomeBreve;
    }

    /**
     * @return the nord
     */
    public Double getNord() {
        return nord;
    }

    /**
     * @return the quota
     */
    public Integer getQuota() {
        return quota;
    }

    /**
     * @param codice
     *            the codice to set
     */
    public void setCodice(String codice) {
        this.codice = codice;
    }

    /**
     * @param est
     *            the est to set
     */
    public void setEst(Double est) {
        this.est = est;
    }

    /**
     * @param latitudine
     *            the latitudine to set
     */
    public void setLatitudine(Double latitudine) {
        this.latitudine = latitudine;
    }

    /**
     * @param longitudine
     *            the longitudine to set
     */
    public void setLongitudine(Double longitudine) {
        this.longitudine = longitudine;
    }

    /**
     * @param nome
     *            the nome to set
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * @param nomeBreve
     *            the nomeBreve to set
     */
    public void setNomeBreve(String nomeBreve) {
        this.nomeBreve = nomeBreve;
    }

    /**
     * @param nord
     *            the nord to set
     */
    public void setNord(Double nord) {
        this.nord = nord;
    }

    /**
     * @param quota
     *            the quota to set
     */
    public void setQuota(Integer quota) {
        this.quota = quota;
    }

}
