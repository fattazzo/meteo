package com.gmail.fattazzo.meteo.fragment.valanghe.rilevazioni.sezioni;

import java.util.Date;

/**
 *
 * @author fattazzo
 *
 *         date: 03/feb/2016
 *
 */
public class RilevazioneSezione {

    private Date data;

    private String descrizione;

    /**
     * @return the data
     */
    public Date getData() {
        return data;
    }

    /**
     * @return the descrizione
     */
    public String getDescrizione() {
        return descrizione;
    }

    /**
     * @param data
     *            the data to set
     */
    public void setData(Date data) {
        this.data = data;
    }

    /**
     * @param descrizione
     *            the descrizione to set
     */
    public void setDescrizione(String descrizione) {
        this.descrizione = descrizione;
    }

}
