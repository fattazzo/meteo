package com.gmail.fattazzo.meteo.utils;

import java.util.Locale;

import android.content.Context;

/**
 *
 * @author fattazzo
 *
 *         date: 01/ago/2014
 *
 */
public final class ResourceUtils {

    /**
     * Carica dal file xml il valore della risorsa con il nome richiesto.
     *
     * @param context
     *            context
     * @param resourceName
     *            nome della risorsa
     * @return valore della risorsa. Se non viene trovato viene restituito il nome della risorsa passato come parametro
     */
    public static String getString(Context context, String resourceName) {

        // recupero l'id del valore
        resourceName = resourceName.toLowerCase(Locale.ITALIAN).replaceAll(" ", "_");
        int id = context.getResources().getIdentifier(resourceName, "string", context.getPackageName());

        String result = resourceName;

        // se ho trovato l'id carico la risorsa
        if (id != 0) {
            result = context.getString(id);
        }

        return result;
    }

    /**
     * Costruttore.
     */
    private ResourceUtils() {

    }

}
