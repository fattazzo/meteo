package com.gmail.fattazzo.meteo.fragment.valanghe.rilevazioni.sezioni.temperaturaaria;

import android.view.View;

import com.gmail.fattazzo.meteo.R;
import com.gmail.fattazzo.meteo.domain.valanghe.dati.DatiStazione;
import com.gmail.fattazzo.meteo.fragment.valanghe.rilevazioni.sezioni.RilevazioneSezione;
import com.gmail.fattazzo.meteo.fragment.valanghe.rilevazioni.sezioni.RilevazioneSezioneListAdapter;
import com.gmail.fattazzo.meteo.fragment.valanghe.rilevazioni.view.AbstractSezioneManager;
import com.gmail.fattazzo.meteo.utils.ExpandableHeightListView;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author fattazzo
 *
 *         date: 03/feb/2016
 *
 */
public class TemperaturaAriaSezioneManager extends AbstractSezioneManager {

    /**
     * Costruttore.
     *
     * @param view
     *            view
     */
    public TemperaturaAriaSezioneManager(final View view) {
        super(view);
    }

    @Override
    protected void clearContent() {
        ExpandableHeightListView condTempoList = (ExpandableHeightListView) view
                .findViewById(R.id.neve_temperaturaaria_list);
        condTempoList.setAdapter(null);
    }

    @Override
    protected int getHeaderViewId() {
        return R.id.anag_stazione_neve_temperaturaaria_layout;
    }

    @Override
    protected int getToggleViewId() {
        return R.id.anag_stazione_neve_temperaturaariaData_layout;
    }

    @Override
    protected void loadContent() {
        List<RilevazioneSezione> temperaturaAriaList = new ArrayList<>();
        for (DatiStazione dato : getDatiStazione()) {
            RilevazioneSezione temperaturaAria = new RilevazioneSezione();
            temperaturaAria.setData(dato.getDataMisurazione());
            String descrizione = getLegendaDatiNeveManager().parseTemperatura(dato.getTemperaturaAria());
            temperaturaAria.setDescrizione(descrizione);
            temperaturaAriaList.add(temperaturaAria);
        }

        RilevazioneSezioneListAdapter adapter = new RilevazioneSezioneListAdapter(view.getContext(), temperaturaAriaList);

        ExpandableHeightListView listView = (ExpandableHeightListView) view.findViewById(R.id.neve_temperaturaaria_list);
        listView.setAdapter(adapter);
        listView.setExpanded(true);

    }
}
