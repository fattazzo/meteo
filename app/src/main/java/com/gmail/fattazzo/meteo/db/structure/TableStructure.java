/**
 *
 */
package com.gmail.fattazzo.meteo.db.structure;

import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;

/**
 *
 * @author fattazzo
 *
 *         date: 17/lug/2014
 *
 * @param <T>
 *            oggetto gestito dalla tabella
 */
public interface TableStructure<T> {

    /**
     * Crea il content values per l'oggetto gestito dalla tabella.
     *
     * @param object
     *            oggetto della tabella
     * @return {@link ContentValues}
     */
    ContentValues createContentValues(T object);

    /**
     * Resituisce tutti i nomi delle colonne della tabella.
     *
     * @return nomi colonne
     */
    String[] getColumns();

    /**
     * Sql di creazione della tabella.
     *
     * @return sql
     */
    String getCreateTableSQL();

    /**
     * Sql di drop della tabella.
     *
     * @return sql
     */
    String getDropTableSQL();

    /**
     * Sql di insert della tabella.
     *
     * @return sql
     */
    String getInsertTableSQL();

    /**
     * @return nome della tabella
     */
    String getTableName();

    /**
     * @return uri della tabella
     */
    Uri getUri();

    /**
     * Esegue il parse del cursor per creare l'oggetto della tabella.
     *
     * @param cursor
     *            cursor
     * @return oggetto
     */
    T parseCursor(Cursor cursor);
}
