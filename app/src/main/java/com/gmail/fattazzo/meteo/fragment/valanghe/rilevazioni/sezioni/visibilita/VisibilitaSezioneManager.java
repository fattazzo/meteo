package com.gmail.fattazzo.meteo.fragment.valanghe.rilevazioni.sezioni.visibilita;

import java.util.ArrayList;
import java.util.List;

import android.view.View;

import com.gmail.fattazzo.meteo.R;
import com.gmail.fattazzo.meteo.domain.valanghe.dati.DatiStazione;
import com.gmail.fattazzo.meteo.fragment.valanghe.rilevazioni.sezioni.RilevazioneSezione;
import com.gmail.fattazzo.meteo.fragment.valanghe.rilevazioni.sezioni.RilevazioneSezioneListAdapter;
import com.gmail.fattazzo.meteo.fragment.valanghe.rilevazioni.view.AbstractSezioneManager;
import com.gmail.fattazzo.meteo.utils.ExpandableHeightListView;

/**
 *
 * @author fattazzo
 *
 *         date: 03/feb/2016
 *
 */
public class VisibilitaSezioneManager extends AbstractSezioneManager {

    /**
     * Costruttore.
     *
     * @param view
     *            view
     */
    public VisibilitaSezioneManager(final View view) {
        super(view);
    }

    @Override
    protected void clearContent() {
        ExpandableHeightListView condTempoList = (ExpandableHeightListView) view
                .findViewById(R.id.neve_visibilita_list);
        condTempoList.setAdapter(null);
    }

    @Override
    protected int getHeaderViewId() {
        return R.id.anag_stazione_neve_visibilita_layout;
    }

    @Override
    protected int getToggleViewId() {
        return R.id.anag_stazione_neve_visibilitaData_layout;
    }

    @Override
    protected void loadContent() {
        List<RilevazioneSezione> visibilitaList = new ArrayList<>();
        for (DatiStazione dato : getDatiStazione()) {
            RilevazioneSezione visibilita = new RilevazioneSezione();
            visibilita.setData(dato.getDataMisurazione());
            String descrizione = getLegendaDatiNeveManager().getMapVisibilita().get(dato.getVisibilita());
            visibilita.setDescrizione(descrizione != null ? descrizione : "");
            visibilitaList.add(visibilita);
        }

        RilevazioneSezioneListAdapter adapter = new RilevazioneSezioneListAdapter(view.getContext(), visibilitaList);

        ExpandableHeightListView listView = (ExpandableHeightListView) view.findViewById(R.id.neve_visibilita_list);
        listView.setAdapter(adapter);
        listView.setExpanded(true);

    }
}
