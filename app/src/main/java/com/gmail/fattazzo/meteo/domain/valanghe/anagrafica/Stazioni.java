package com.gmail.fattazzo.meteo.domain.valanghe.anagrafica;

import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 *
 * @author fattazzo
 *
 *         date: 01/feb/2016
 *
 */
public class Stazioni {

    @XStreamAlias("anagrafica_neve")
    private List<StazioneValanghe> stazioniValanghe;

    /**
     * @return the stazioniValanghe
     */
    public List<StazioneValanghe> getStazioniValanghe() {
        return stazioniValanghe;
    }

    /**
     * @param stazioniValanghe
     *            the stazioniValanghe to set
     */
    public void setStazioniValanghe(List<StazioneValanghe> stazioniValanghe) {
        this.stazioniValanghe = stazioniValanghe;
    }

}
