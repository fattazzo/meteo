package com.gmail.fattazzo.meteo.domain.stazioni.dati;

import java.util.Date;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;
import com.thoughtworks.xstream.annotations.XStreamConverter;

/**
 *
 * @author fattazzo
 *
 *         date: 13/lug/2015
 *
 */
public class PrecipitazioneStazione implements IDatoStazione {

    @XStreamAlias("UM")
    @XStreamAsAttribute
    private String unitaMisura;

    @XStreamConverter(value = com.gmail.fattazzo.meteo.parser.xml.converter.DateConverter.class, strings = { "yyyy-MM-dd'T'HH:mm:ss" })
    private Date data;

    @XStreamAlias("pioggia")
    private double valore;

    /**
     * @return the data
     */
    @Override
    public Date getData() {
        return data;
    }

    /**
     * @return the unitaMisura
     */
    @Override
    public String getUnitaMisura() {
        return unitaMisura;
    }

    /**
     * @return the valore
     */
    @Override
    public double getValore() {
        return valore;
    }

    /**
     * @param data
     *            the data to set
     */
    public void setData(Date data) {
        this.data = data;
    }

    /**
     * @param unitaMisura
     *            the unitaMisura to set
     */
    public void setUnitaMisura(String unitaMisura) {
        this.unitaMisura = unitaMisura;
    }

    /**
     * @param valore
     *            the valore to set
     */
    public void setValore(double valore) {
        this.valore = valore;
    }
}
