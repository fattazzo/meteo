package com.gmail.fattazzo.meteo.fragment.stazioni.rilevazioni.builder;

import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class DateXAxisValueFormatter implements IAxisValueFormatter {

    private final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm", Locale.ITALIAN);

    private String[] mValues;

    public DateXAxisValueFormatter() {
    }

    @Override
    public String getFormattedValue(float value, AxisBase axis) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis((long) value);
        return dateFormat.format(new Date((long) value));
    }


}