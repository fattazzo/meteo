package com.gmail.fattazzo.meteo.utils;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.widget.ImageView;

/**
 *
 * @author fattazzo<br>
 *
 *         date: 13/lug/2014<br>
 * <br>
 *
 *         Classe che si occupa di caricare in modo asincrono una immagine da un {@link URL} specificato e settarla in
 *         una {@link ImageView}.<br>
 *
 *         Uso:<br>
 *         new LoadImagefromUrl().execute(imageView, "http://urldellimmagine.estensione");
 */
public class LoadImagefromUrl extends AsyncTask<Object, Void, Bitmap> {

    private ImageView imageView = null;

    @Override
    protected Bitmap doInBackground(Object... params) {
        this.imageView = (ImageView) params[0];
        String url = (String) params[1];
        System.out.println(url);
        return loadBitmap(url);
    }

    /**
     * Carica l'immagine dell'url specificato.
     *
     * @param url
     *            url dell'immagine
     * @return immagine caricata
     */
    public Bitmap loadBitmap(String url) {
        URL newurl = null;
        Bitmap bitmap = null;
        try {
            newurl = new URL(url);
            bitmap = BitmapFactory.decodeStream(newurl.openConnection().getInputStream());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {

            e.printStackTrace();
        }
        return bitmap;
    }

    @Override
    protected void onPostExecute(Bitmap result) {
        super.onPostExecute(result);
        imageView.setImageBitmap(result);
    }
}
