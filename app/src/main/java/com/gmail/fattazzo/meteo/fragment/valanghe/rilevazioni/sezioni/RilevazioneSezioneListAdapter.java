package com.gmail.fattazzo.meteo.fragment.valanghe.rilevazioni.sezioni;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.gmail.fattazzo.meteo.R;

/**
 *
 * @author fattazzo
 *
 *         date: 03/feb/2016
 *
 */
public class RilevazioneSezioneListAdapter extends ArrayAdapter<RilevazioneSezione> {

    /**
     *
     * @author fattazzo
     *
     *         date: 03/feb/2016
     *
     */
    static class ViewHolderItem {
        protected TextView dataTextView;
        protected TextView descrizioneTextView;
    }

    private DateFormat dateFormat = new SimpleDateFormat("dd/MM", Locale.getDefault());

    private LayoutInflater layoutInflater = null;

    /**
     * Costruttore.
     *
     * @param context
     *            context
     * @param objects
     *            rilevazione
     */
    public RilevazioneSezioneListAdapter(final Context context, final List<RilevazioneSezione> objects) {
        super(context, R.layout.neve_tabella_list_adapter, objects);
        this.layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolderItem viewHolder;
        View v = convertView;

        if (v == null) {
            v = layoutInflater.inflate(R.layout.neve_tabella_list_adapter, (ViewGroup) null);

            viewHolder = new ViewHolderItem();
            viewHolder.descrizioneTextView = (TextView) v.findViewById(R.id.neve_tabella_descrizione_label);
            viewHolder.dataTextView = (TextView) v.findViewById(R.id.neve_tabella_codice_label);
            v.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolderItem) v.getTag();
        }

        RilevazioneSezione rilevazione = getItem(position);

        String dataStr;
        try {
            dataStr = dateFormat.format(rilevazione.getData());
        } catch (Exception e) {
            dataStr = "/";
        }
        viewHolder.dataTextView.setText(dataStr);
        viewHolder.descrizioneTextView.setText(rilevazione.getDescrizione());
        return v;
    }

}
