/**
 *
 */
package com.gmail.fattazzo.meteo.fragment.stazioni.rilevazioni.builder;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.gmail.fattazzo.meteo.R;
import com.gmail.fattazzo.meteo.domain.stazioni.dati.IDatoStazione;

/**
 *
 * @author fattazzo
 *
 *         date: 28/lug/2014
 *
 */
public class TabellaDatoListAdapter extends ArrayAdapter<IDatoStazione> {

    /**
     *
     * @author fattazzo
     *
     *         date: 13/lug/2015
     *
     */
    static class ViewHolderItem {
        protected TextView dataTextView;
        protected TextView valoreTextView;
    }

    private DateFormat dateFormat = null;
    private DecimalFormat decimalFormat = null;

    private LayoutInflater layoutInflater = null;

    /**
     * Costruttore.
     *  @param context
     *            context
     * @param objects
     *            dati stazione
     * @param paramDateFormat
 *            formato della data
     * @param paramDecimalFormat decimal format
     *
     */
    public TabellaDatoListAdapter(final Context context, final List<IDatoStazione> objects,
                                  final DateFormat paramDateFormat, final DecimalFormat paramDecimalFormat) {
        super(context, R.layout.dato_stazione_list_adapter, objects);
        this.dateFormat = paramDateFormat;
        this.decimalFormat = paramDecimalFormat;
        this.layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolderItem viewHolder;
        View v = convertView;

        if (v == null) {
            v = layoutInflater.inflate(R.layout.dato_stazione_list_adapter, (ViewGroup) null);

            viewHolder = new ViewHolderItem();
            viewHolder.dataTextView = (TextView) v.findViewById(R.id.dato_stazione_nome_label);
            viewHolder.valoreTextView = (TextView) v.findViewById(R.id.dato_stazione_codice_label);
            v.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolderItem) v.getTag();
        }

        IDatoStazione datoStazione = getItem(position);

        viewHolder.dataTextView.setText(dateFormat.format(datoStazione.getData()));
        viewHolder.valoreTextView.setText(decimalFormat.format(datoStazione.getValore()));

        return v;
    }

}
