package com.gmail.fattazzo.meteo.domain.valanghe.bollettino;

import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 *
 * @author fattazzo
 *
 *         date: 10/feb/2016
 *
 */
public class Giorno {

    @XStreamAlias("data")
    private String data;

    @XStreamAlias("mappaPericolo")
    private String mappapericolo;

    @XStreamAlias("statocielo")
    private String statoCielo;

    @XStreamAlias("iconaurl")
    private String iconaUrl;

    @XStreamAlias("zerotermico")
    private String zeroTermico;

    @XStreamAlias("situazione")
    private String situazione;

    /**
     * @return the data
     */
    public String getData() {
        return data;
    }

    /**
     * @return the iconaUrl
     */
    public String getIconaUrl() {
        return iconaUrl;
    }

    /**
     * @return the mappapericolo
     */
    public String getMappapericolo() {
        return mappapericolo;
    }

    /**
     * @return the situazione
     */
    public String getSituazione() {
        return situazione;
    }

    /**
     * @return the statoCielo
     */
    public String getStatoCielo() {
        return statoCielo;
    }

    /**
     * @return the zeroTermico
     */
    public String getZeroTermico() {
        return zeroTermico;
    }

    /**
     * @param data
     *            the data to set
     */
    public void setData(String data) {
        this.data = data;
    }

    /**
     * @param iconaUrl
     *            the iconaUrl to set
     */
    public void setIconaUrl(String iconaUrl) {
        this.iconaUrl = iconaUrl;
    }

    /**
     * @param mappapericolo
     *            the mappapericolo to set
     */
    public void setMappapericolo(String mappapericolo) {
        this.mappapericolo = mappapericolo;
    }

    /**
     * @param situazione
     *            the situazione to set
     */
    public void setSituazione(String situazione) {
        this.situazione = situazione;
    }

    /**
     * @param statoCielo
     *            the statoCielo to set
     */
    public void setStatoCielo(String statoCielo) {
        this.statoCielo = statoCielo;
    }

    /**
     * @param zeroTermico
     *            the zeroTermico to set
     */
    public void setZeroTermico(String zeroTermico) {
        this.zeroTermico = zeroTermico;
    }
}
