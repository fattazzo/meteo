package com.gmail.fattazzo.meteo.fragment.webcam;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import com.gmail.fattazzo.meteo.R;
import com.gmail.fattazzo.meteo.domain.webcam.Webcam;

import java.util.List;

/**
 * @author fattazzo
 *         <p/>
 *         date: 13/lug/2015
 */
public class WebcamListAdapter extends ArrayAdapter<Webcam> {

    /**
     * Costruttore.
     *
     * @param context context
     * @param objects lista delle webcam
     */
    public WebcamListAdapter(final Context context, final List<Webcam> objects) {
        super(context, R.layout.webcam_layout, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;

        if (v == null) {
            LayoutInflater vi = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = vi.inflate(R.layout.webcam_layout, (ViewGroup) null);
        }
        Webcam webcam = getItem(position);

        TextView descriptionTextView = (TextView) v.findViewById(R.id.webcam_description_text);
        descriptionTextView.setText(webcam.getDescrizione());
        descriptionTextView.setTypeface(null, webcam.isShowInWidget() ? Typeface.BOLD_ITALIC : Typeface.NORMAL);

        ImageButton websiteButton = (ImageButton) v.findViewById(R.id.webcam_website_button);
        String website = webcam.getWebsite();
        websiteButton.setVisibility(website != null && !website.isEmpty() ? View.VISIBLE : View.GONE);
        websiteButton.setTag(webcam.getWebsite());
        websiteButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse((String) arg0.getTag()));
                getContext().startActivity(browserIntent);
            }
        });

        // RelativeLayout rootLayout = (RelativeLayout) v.findViewById(R.id.webcam_panel);
        // if (webcam.isShowInWidget()) {
        // rootLayout.setBackgroundColor(R.drawable.bg_pattern);
        // } else {
        // rootLayout.setBackgroundColor(getContext().getResources().getColor(android.R.color.transparent));
        // }

        return v;
    }

}
