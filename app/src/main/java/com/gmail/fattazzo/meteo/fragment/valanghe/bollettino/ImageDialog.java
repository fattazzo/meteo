package com.gmail.fattazzo.meteo.fragment.valanghe.bollettino;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.gmail.fattazzo.meteo.R;

/**
 *
 * @author fattazzo
 *
 *         date: 18/feb/2016
 *
 */
public final class ImageDialog extends Dialog {

    private final Drawable drawable;

    /**
     * Dialog per la visualizzazione di una immagine.
     *
     * @param context
     *            context
     * @param drawable
     *            immagine
     */
    public ImageDialog(final Context context, final Drawable drawable) {
        super(context, R.style.DialogNoTitle);
        this.drawable = drawable;
    }

    @Override
    public void onCreate(final Bundle unused) {
        DisplayMetrics metrics = new DisplayMetrics();
        WindowManager wm = (WindowManager) getContext().getSystemService(Context.WINDOW_SERVICE);
        wm.getDefaultDisplay().getMetrics(metrics);

        int width = Math.min(metrics.widthPixels, metrics.heightPixels);

        ImageView myImage = new ImageView(getContext());
        myImage.setImageDrawable(drawable);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(width, width);
        myImage.setLayoutParams(layoutParams);
        setContentView(myImage);

        android.view.WindowManager.LayoutParams params = getWindow().getAttributes();
        params.height = width;
        params.width = width;
        getWindow().setAttributes(params);
    }
}
