package com.gmail.fattazzo.meteo.fragment.valanghe.rilevazioni.view;

/**
 *
 * @author fattazzo
 *
 *         date: 03/feb/2016
 *
 */
public interface IRilevazioneSezioneViewManager {

    /**
     * Pulisce il contenuto della sezione.
     */
    void clear();

    /**
     * Visualizza o nasconde il contenuto della sezione.
     */
    void toggleContent();

}
