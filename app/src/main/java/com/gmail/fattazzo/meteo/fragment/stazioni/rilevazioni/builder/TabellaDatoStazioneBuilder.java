package com.gmail.fattazzo.meteo.fragment.stazioni.rilevazioni.builder;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import android.content.Context;
import android.os.AsyncTask;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.gmail.fattazzo.meteo.R;
import com.gmail.fattazzo.meteo.domain.stazioni.dati.DatiStazione;
import com.gmail.fattazzo.meteo.domain.stazioni.dati.IDatoStazione;
import com.gmail.fattazzo.meteo.fragment.stazioni.rilevazioni.TipoDatoStazione;
import com.gmail.fattazzo.meteo.utils.ExpandableHeightListView;

/**
 *
 * @author fattazzo
 *
 *         date: 18/lug/2014
 *
 */
public class TabellaDatoStazioneBuilder extends TipoDatoStazioneLayoutBuilder {

    private ExpandableHeightListView listView;

    /**
     * Costruttore.
     *
     */
    public TabellaDatoStazioneBuilder() {
        super();
    }

    @SuppressWarnings("unchecked")
    @Override
    public View buildView(final Context context, DatiStazione datiStazione, TipoDatoStazione tipoDatoStazione) {
        RelativeLayout layout = getLayout(context);

        if (datiStazione == null) {
            showEmptyDataText(context);
        } else {
            TextView umTextView = (TextView) layout.findViewById(R.id.dati_stazione_um_label);
            listView = (ExpandableHeightListView) layout.findViewById(R.id.dati_stazioni_list);

            List<IDatoStazione> datiStazioneList = new ArrayList<>();
            switch (tipoDatoStazione) {
            case PRECIPITAZIONE:
                datiStazioneList.addAll(datiStazione.getPrecipitazioni());
                break;
            case TEMPERATURA:
                datiStazioneList.addAll(datiStazione.getTemperature());
                break;
            default:
                break;
            }

            String um = datiStazioneList.isEmpty() ? "" : datiStazioneList.get(0).getUnitaMisura();
            umTextView.setText(um);

            new AsyncTask<List<IDatoStazione>, Void, List<IDatoStazione>>() {

                @Override
                protected List<IDatoStazione> doInBackground(List<IDatoStazione>... params) {

                    List<IDatoStazione> list = params[0];

                    // sorto la collection im modo tale di avere i valori più recenti per primi
                    Collections.sort(list, new Comparator<IDatoStazione>() {

                        @Override
                        public int compare(IDatoStazione lhs, IDatoStazione rhs) {
                            return lhs.getData().compareTo(rhs.getData()) * -1;
                        }
                    });
                    return list;
                }

                @Override
                protected void onPostExecute(List<IDatoStazione> result) {
                    ((TabellaDatoListAdapter) listView.getAdapter()).addAll(result);
                }

                @Override
                protected void onPreExecute() {
                    TabellaDatoListAdapter adapter = new TabellaDatoListAdapter(context,
                            new ArrayList<IDatoStazione>(), getDateFormat(),
                            getDecimalFormat());
                    listView.setAdapter(adapter);
                    listView.setExpanded(true);
                }
            }.execute(datiStazioneList);

        }

        return layout;
    }

    @Override
    public Integer getLayoutResourceId() {
        return R.layout.dato_stazione_meteo_tabella;
    }

}
