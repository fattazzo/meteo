package com.gmail.fattazzo.meteo.parser.manager;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Looper;
import android.widget.Toast;

import com.gmail.fattazzo.meteo.R;
import com.gmail.fattazzo.meteo.utils.Closure;

/**
 *
 * @author fattazzo
 *
 *         date: 17/lug/2014
 *
 * @param <K>
 *            parametri
 * @param <T>
 *            result
 */
public abstract class MeteoAsyncTask<K, T> extends AsyncTask<K, Void, T> {

    private Context context;

    private Closure<T> postExecuteClosure;

    private Exception exception;
    private boolean suppressException;

    private ProgressDialog dialog = null;

    /**
     * Costruttore.
     *
     * @param context
     *            contesto
     * @param messageResourceId
     *            id della risorsa per il messaggio da visualizzare nel progress dialog
     */
    public MeteoAsyncTask(final Context context, final int messageResourceId) {
        this(context, messageResourceId, null, true, false);
    }

    /**
     * Costruttore.
     *
     * @param context
     *            contesto
     * @param messageResourceId
     *            id della risorsa per il messaggio da visualizzare nel progress dialog
     * @param onPostExecuteClosure
     *            closure eseguita sull onPostExecute del task
     */
    public MeteoAsyncTask(final Context context, final int messageResourceId, final Closure<T> onPostExecuteClosure) {
        this(context, messageResourceId, onPostExecuteClosure, true, false);
    }

    /**
     * Costruttore.
     *
     * @param context
     *            contesto
     * @param messageResourceId
     *            id della risorsa per il messaggio da visualizzare nel progress dialog
     * @param onPostExecuteClosure
     *            closure eseguita sull onPostExecute del task
     * @param showLoadingDialog
     *            <code>true</code> per visualizzare il dialogo di caricamento
     * @param suppressException
     *            se <code>true</code> non rilancia eccezioni ( no network, ecc...)
     */
    public MeteoAsyncTask(final Context context, final int messageResourceId, final Closure<T> onPostExecuteClosure,
            final boolean showLoadingDialog, final boolean suppressException) {
        super();

        this.context = context;

        this.suppressException = suppressException;

        if (showLoadingDialog) {
            dialog = new ProgressDialog(this.context);
            dialog.setTitle(context.getString(R.string.wait));
            dialog.setMessage(this.context.getText(messageResourceId));
            dialog.setCancelable(false);
        }

        this.postExecuteClosure = onPostExecuteClosure;
    }

    @Override
    protected T doInBackground(K... params) {

        T result = null;
        try {
            result = executeInBackGround(params);
        } catch (Exception e) {
            exception = e;
        }

        return result;
    }

    /**
     * Eseguito durante la doInBackGround del task.
     *
     * @param params
     *            parametri
     * @return T
     */
    public abstract T executeInBackGround(K... params);

    @Override
    protected void onPostExecute(T result) {
        super.onPostExecute(result);
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }

        if (postExecuteClosure != null) {
            postExecuteClosure.execute(result);
        }

        if (!suppressException && exception != null) {
            new Thread(new Runnable() {

                @Override
                public void run() {
                    Looper.prepare();
                    Toast toast = Toast.makeText(context, context.getString(R.string.service_error), Toast.LENGTH_LONG);
                    toast.show();
                    Looper.loop();
                }
            }).start();
        }
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if (dialog != null) {
            dialog.show();
        }
    }

}
