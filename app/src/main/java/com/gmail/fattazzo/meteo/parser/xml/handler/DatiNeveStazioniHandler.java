package com.gmail.fattazzo.meteo.parser.xml.handler;

import java.util.ArrayList;
import java.util.List;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.gmail.fattazzo.meteo.domain.valanghe.dati.DatiStazione;

/**
 *
 * @author fattazzo
 *
 *         date: 02/feb/2016
 *
 */
public class DatiNeveStazioniHandler extends DefaultHandler {
    private List<DatiStazione> datiStazioni;
    private DatiStazione currentStazione;
    private StringBuilder builder;

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        super.characters(ch, start, length);
        builder.append(ch, start, length);
    }

    @Override
    public void endElement(String uri, String localName, String name) throws SAXException {
        super.endElement(uri, localName, name);
        if (this.currentStazione != null) {
            if (DatiStazione.XMLPROP.contains(localName)) {
                currentStazione.setValue(localName, builder.toString());
            } else if (localName.equalsIgnoreCase(DatiStazione.XML_BEAN_NAME)) {
                datiStazioni.add(currentStazione);
            }
            builder.setLength(0);
        }
    }

    /**
     * @return the datiStazioni
     */
    public List<DatiStazione> getDatiStazioni() {
        return datiStazioni;
    }

    @Override
    public void startDocument() throws SAXException {
        super.startDocument();
        datiStazioni = new ArrayList<>();
        builder = new StringBuilder();
    }

    @Override
    public void startElement(String uri, String localName, String name, Attributes attributes) throws SAXException {
        super.startElement(uri, localName, name, attributes);
        if (localName.equalsIgnoreCase(DatiStazione.XML_BEAN_NAME)) {
            this.currentStazione = new DatiStazione();
        }
    }
}
