package com.gmail.fattazzo.meteo.fragment.valanghe.rilevazioni.sezioni.ventoquotatipo;

import java.util.ArrayList;
import java.util.List;

import android.view.View;

import com.gmail.fattazzo.meteo.R;
import com.gmail.fattazzo.meteo.domain.valanghe.dati.DatiStazione;
import com.gmail.fattazzo.meteo.fragment.valanghe.rilevazioni.sezioni.RilevazioneSezione;
import com.gmail.fattazzo.meteo.fragment.valanghe.rilevazioni.sezioni.RilevazioneSezioneListAdapter;
import com.gmail.fattazzo.meteo.fragment.valanghe.rilevazioni.view.AbstractSezioneManager;
import com.gmail.fattazzo.meteo.utils.ExpandableHeightListView;

/**
 *
 * @author fattazzo
 *
 *         date: 04/feb/2016
 *
 */
public class VentoQuotaTipoSezioneManager extends AbstractSezioneManager {

    /**
     * Costruttore.
     *
     * @param view
     *            view
     */
    public VentoQuotaTipoSezioneManager(final View view) {
        super(view);
    }

    @Override
    protected void clearContent() {
        ExpandableHeightListView condTempoList = (ExpandableHeightListView) view.findViewById(R.id.neve_vqt_list);
        condTempoList.setAdapter(null);
    }

    @Override
    protected int getHeaderViewId() {
        return R.id.anag_stazione_neve_vqt_layout;
    }

    @Override
    protected int getToggleViewId() {
        return R.id.anag_stazione_neve_vqtData_layout;
    }

    @Override
    protected void loadContent() {
        List<RilevazioneSezione> vqtList = new ArrayList<>();
        for (DatiStazione dato : getDatiStazione()) {
            RilevazioneSezione vqt = new RilevazioneSezione();
            vqt.setData(dato.getDataMisurazione());
            String descrizione = getLegendaDatiNeveManager().getMapVentoQuotaTipo().get(dato.getVentoQuotaTipo());
            vqt.setDescrizione(descrizione != null ? descrizione : "");
            vqtList.add(vqt);
        }

        RilevazioneSezioneListAdapter adapter = new RilevazioneSezioneListAdapter(view.getContext(), vqtList);

        ExpandableHeightListView listView = (ExpandableHeightListView) view.findViewById(R.id.neve_vqt_list);
        listView.setAdapter(adapter);
        listView.setExpanded(true);

    }
}
