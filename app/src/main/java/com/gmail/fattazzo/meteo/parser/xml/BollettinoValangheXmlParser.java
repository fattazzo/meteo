package com.gmail.fattazzo.meteo.parser.xml;

import com.gmail.fattazzo.meteo.domain.valanghe.bollettino.Bollettino;
import com.gmail.fattazzo.meteo.domain.valanghe.bollettino.Giorno;
import com.gmail.fattazzo.meteo.settings.MeteoSettings;
import com.thoughtworks.xstream.XStream;

/**
 *
 * @author fattazzo
 *
 *         date: 10/feb/2016
 *
 */
public class BollettinoValangheXmlParser extends MeteoXmlParser {

    /**
     * Carica il bollettino valanghe.
     *
     * @return previsione caricata
     */
    public Bollettino caricaBollettino() {

        XStream xStream = new XStream();
        xStream.ignoreUnknownElements();
        xStream.alias("previsione", Bollettino.class);
        xStream.alias("giorno", Giorno.class);
        xStream.autodetectAnnotations(true);

        return loadXml(MeteoSettings.BOLLETTINO_VALANGHE_XML, xStream);
    }
}
