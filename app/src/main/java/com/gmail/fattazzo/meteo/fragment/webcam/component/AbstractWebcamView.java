package com.gmail.fattazzo.meteo.fragment.webcam.component;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView.ScaleType;
import android.widget.ProgressBar;

import com.gmail.fattazzo.meteo.R;

/**
 *
 * @author fattazzo
 *
 *         date: 15/07/15
 *
 */
public abstract class AbstractWebcamView extends FrameLayout {

    protected String urlToLoad;

    private ProgressBar progressBar;

    /**
     * Costruttore.
     *
     * @param urlToLoad
     *            url da caricare
     * @param context
     *            context
     */
    public AbstractWebcamView(final String urlToLoad, final Context context) {
        super(context);
        this.urlToLoad = urlToLoad;

        init();
    }

    /**
     * Button per la ricarica del contenuto della webcam.
     *
     * @return {@link ImageButton}
     */
    private ImageButton createReloadButton() {

        ImageButton reloadButton = new ImageButton(getContext());
        reloadButton.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.refresh48));
        reloadButton.setScaleType(ScaleType.CENTER_CROP);
        reloadButton.setBackgroundColor(ContextCompat.getColor(getContext(), android.R.color.transparent));
        reloadButton.setAlpha(0.7f);
        reloadButton.setClickable(true);

        int size = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 60, getResources().getDisplayMetrics());
        reloadButton.setLayoutParams(new FrameLayout.LayoutParams(size, size, Gravity.BOTTOM | Gravity.RIGHT));
        reloadButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                loadWebcam();
            }
        });

        return reloadButton;
    }

    /**
     * @return the progressBar
     */
    public ProgressBar getProgressBar() {
        return progressBar;
    }

    /**
     * @return {@link View} che contiene il contenuto da visualizzare della webcam.
     */
    public abstract View getWebcamView();

    /**
     * Inizializza la webview.
     */
    private void init() {
        setLayoutParams(new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT));

        addView(getWebcamView());

        progressBar = new ProgressBar(getContext());
        progressBar.setLayoutParams(new FrameLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT, Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL));
        progressBar.setIndeterminate(true);
        addView(progressBar);

        addView(createReloadButton());

        loadWebcam();
    }

    /**
     * Carica il contenuto della webcam.
     */
    public abstract void loadWebcam();
}
