package com.gmail.fattazzo.meteo.fragment.stazioni.anagrafica;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;

import com.gmail.fattazzo.meteo.R;
import com.gmail.fattazzo.meteo.db.handlers.MeteoDatabaseHandler;
import com.gmail.fattazzo.meteo.domain.stazioni.anagrafica.StazioneMeteo;
import com.gmail.fattazzo.meteo.parser.manager.MeteoManager;
import com.gmail.fattazzo.meteo.utils.Closure;
import com.gmail.fattazzo.meteo.utils.ExpandableHeightListView;

import java.lang.reflect.Field;
import java.util.List;

/**
 * @author fattazzo
 *         <p/>
 *         date: 17/lug/2014
 */
public class AnagraficaFragment extends Fragment implements OnClickListener {

    private View view;

    private MeteoManager meteoManager;
    private MeteoDatabaseHandler databaseHandler;

    private List<StazioneMeteo> stazioniMeteo = null;

    /**
     * Carica le stazioni meteo.
     */
    private void loadStazioniMeteo() {

        int countStazioni = databaseHandler.getCountStazioni();

        // se non ho ancora stazioni meteo nel database le carico
        if (countStazioni == 0) {
            meteoManager.caricaAnagraficaStazioni(new Closure<List<StazioneMeteo>>() {

                @Override
                public void execute(List<StazioneMeteo> stazioni) {
                    if (stazioni != null) {
                        databaseHandler.salvaStazioniMeteo(stazioni);
                    }
                    loadTableData();
                }
            });
        } else {
            loadTableData();
        }
    }

    /**
     * Carica tutte le stazioni meteo nell'adapter della tabella.
     */
    private void loadTableData() {

        if (stazioniMeteo == null || stazioniMeteo.isEmpty()) {
            stazioniMeteo = databaseHandler.caricaStazioniMeteo();
        }

        AnagraficaStazioneListAdapter adapter = new AnagraficaStazioneListAdapter(view.getContext(),
                stazioniMeteo);

        ExpandableHeightListView listView = (ExpandableHeightListView) view.findViewById(R.id.anag_stazioni_list);
        listView.setAdapter(adapter);
        listView.setExpanded(true);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.anag_stazioni_refresh_button:
                databaseHandler.cancellaStazioniMeteo();
                stazioniMeteo = null;
                loadStazioniMeteo();
                break;

            default:
                break;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (container != null) {
            container.removeAllViews();
        }

        if (view != null && view.getParent() != null) {
            ((ViewGroup) view.getParent()).removeView(view);
        } else {
            view = inflater.inflate(R.layout.fragment_anagrafica_stazioni, container, false);
        }

        meteoManager = new MeteoManager(view.getContext());
        databaseHandler = MeteoDatabaseHandler.getInstance(view.getContext());

        loadStazioniMeteo();

        Button refreshButton = (Button) view.findViewById(R.id.anag_stazioni_refresh_button);
        refreshButton.setOnClickListener(this);

        return view;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        try {
            Field childFragmentManager = Fragment.class.getDeclaredField("mChildFragmentManager");
            childFragmentManager.setAccessible(true);
            childFragmentManager.set(this, null);
        } catch (NoSuchFieldException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }
}
