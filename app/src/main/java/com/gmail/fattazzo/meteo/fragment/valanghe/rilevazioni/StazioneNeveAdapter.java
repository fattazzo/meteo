package com.gmail.fattazzo.meteo.fragment.valanghe.rilevazioni;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.gmail.fattazzo.meteo.domain.valanghe.anagrafica.StazioneValanghe;

/**
 *
 * @author fattazzo
 *
 *         date: 02/feb/2016
 *
 */
public class StazioneNeveAdapter extends ArrayAdapter<StazioneValanghe> {

    private List<StazioneValanghe> itemList;

    /**
     * Costruttore.
     *
     * @param context
     *            context
     * @param itemList
     *            lista delle stazioni neve
     */
    public StazioneNeveAdapter(final Context context, final List<StazioneValanghe> itemList) {
        super(context, android.R.layout.simple_spinner_item);
        this.itemList = itemList;
    }

    @Override
    public int getCount() {
        return this.itemList.size();
    }

    @Override
    public TextView getDropDownView(int position, View convertView, ViewGroup parent) {

        TextView v = (TextView) convertView;

        if (v == null) {
            LayoutInflater vi = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = (TextView) vi.inflate(android.R.layout.simple_spinner_item, (ViewGroup) null);
        }

        v.setText(itemList.get(position).getNome());
        return v;
    }

    @Override
    public StazioneValanghe getItem(int position) {
        return itemList.get(position);
    }

    @Override
    public TextView getView(int position, View convertView, ViewGroup parent) {

        TextView v = (TextView) convertView;

        if (v == null) {
            LayoutInflater vi = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = (TextView) vi.inflate(android.R.layout.simple_spinner_item, (ViewGroup) null);
        }

        v.setText(itemList.get(position).getNome());
        return v;
    }

}
