package com.gmail.fattazzo.meteo.db.structure;

import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;

import com.gmail.fattazzo.meteo.domain.stazioni.anagrafica.StazioneMeteo;

/**
 *
 * @author fattazzo
 *
 *         date: 13/lug/2015
 *
 */
public class StazioneMeteoTable implements TableStructure<StazioneMeteo> {

    public static final String CODICE = "codice";
    public static final String NOME = "nome";
    public static final String NOME_BREVE = "nomeBreve";
    public static final String QUOTA = "quota";
    public static final String LATITUDINE = "latitudine";
    public static final String LONGITUDINE = "longitudine";
    public static final String EST = "est";
    public static final String NORD = "nord";

    private static final String[] COLUMNS = new String[] { CODICE, NOME, NOME_BREVE, QUOTA, LATITUDINE, LONGITUDINE,
            EST, NORD };

    public static final Uri URI = Uri.parse("sqlite://com.gmail.fattazzo.meteo/table/stazioniMeteo");

    @Override
    public ContentValues createContentValues(final StazioneMeteo stazioneMeteo) {
        ContentValues values = new ContentValues();
        values.put(CODICE, stazioneMeteo.getCodice());
        values.put(NOME, stazioneMeteo.getNome());
        values.put(NOME_BREVE, stazioneMeteo.getNomeBreve());
        values.put(QUOTA, stazioneMeteo.getQuota());
        values.put(LATITUDINE, stazioneMeteo.getLatitudine());
        values.put(LONGITUDINE, stazioneMeteo.getLongitudine());
        values.put(EST, stazioneMeteo.getEst());
        values.put(NORD, stazioneMeteo.getNord());

        return values;
    }

    @Override
    public String[] getColumns() {
        return COLUMNS;
    }

    @Override
    public String getCreateTableSQL() {
        return "CREATE TABLE " + getTableName() + "(" + CODICE + " INTEGER TEXT KEY," + NOME + " TEXT," + NOME_BREVE
                + " TEXT," + QUOTA + " INTEGER," + LATITUDINE + " DOUBLE," + LONGITUDINE + " DOUBLE," + EST
                + " DOUBLE," + NORD + " DOUBLE" + ")";
    }

    @Override
    public String getDropTableSQL() {
        return "DROP TABLE IF EXISTS " + getTableName();
    }

    @Override
    public String getInsertTableSQL() {
        return "INSERT INTO " + getTableName() + "(" + CODICE + "," + NOME + "," + NOME_BREVE
                + "," + QUOTA + "," + LATITUDINE + "," + LONGITUDINE + "," + EST
                + "," + NORD + ") VALUES (?,?,?,?,?,?,?,?)";
    }

    @Override
    public String getTableName() {
        return "stazioniMeteo";
    }

    @Override
    public Uri getUri() {
        return URI;
    }

    @Override
    public StazioneMeteo parseCursor(final Cursor cursor) {
        StazioneMeteo stazioneMeteo = new StazioneMeteo();
        stazioneMeteo.setCodice(cursor.getString(0));
        stazioneMeteo.setNome(cursor.getString(1));
        stazioneMeteo.setNomeBreve(cursor.getString(2));
        stazioneMeteo.setQuota(Integer.parseInt(cursor.getString(3)));
        stazioneMeteo.setLatitudine(cursor.getDouble(4));
        stazioneMeteo.setLongitudine(cursor.getDouble(5));
        stazioneMeteo.setEst(cursor.getDouble(6));
        stazioneMeteo.setNord(cursor.getDouble(7));
        return stazioneMeteo;
    }

}
