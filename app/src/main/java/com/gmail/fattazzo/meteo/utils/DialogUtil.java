package com.gmail.fattazzo.meteo.utils;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.Build;

/**
 * @author fattazzo
 *         <p>
 *         date: 02/02/17
 */
public final class DialogUtil {

    /**
     * Crea il dialog con titolo e layout richiesto.
     *
     * @param title titolo
     * @param layout_id layout
     * @return dialogo creato
     */
    public static Dialog createDialog(String title, int layout_id, Context context) {
        Dialog dialog;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            dialog = new AlertDialog.Builder(context)
                    .setTitle(title)
                    .setView(layout_id)
                    .create();
        } else {
            dialog = new Dialog(context);
            dialog.setContentView(layout_id);
            dialog.setTitle(title);
        }
        return dialog;
    }

    /**
     * Costruttore
     */
    private DialogUtil() {

    }
}
