package com.gmail.fattazzo.meteo.fragment;

import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;

import com.gmail.fattazzo.meteo.R;
import com.gmail.fattazzo.meteo.activity.SplashActivity;
import com.gmail.fattazzo.meteo.fragment.home.HomeFragment_;
import com.gmail.fattazzo.meteo.fragment.v4.PreferenceFragment;
import com.gmail.fattazzo.meteo.preferences.imagewidget.ZoneWidgetPreference;
import com.gmail.fattazzo.meteo.preferences.numberpicker.NumberPickerDialogPreference;
import com.gmail.fattazzo.meteo.settings.MeteoSettings;
import com.gmail.fattazzo.meteo.theme.IconsThemeManager;
import com.gmail.fattazzo.meteo.utils.FragmentUtils;
import com.gmail.fattazzo.meteo.widget.providers.MeteoWidgetProvider;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * @author fattazzo
 *         <p/>
 *         date: 09/giu/2014
 */
public class SettingsFragment extends PreferenceFragment implements OnSharedPreferenceChangeListener {

    private static final List<String> PREF_WIDGETS = new ArrayList<>();

    static {
        PREF_WIDGETS.add(MeteoSettings.WIDGETS_BACKGROUND);
        PREF_WIDGETS.add(MeteoSettings.WIDGETS_TEXT_COLOR);
        PREF_WIDGETS.add(MeteoSettings.WIDGETS_UPDATE_INTERVAL);
        PREF_WIDGETS.add(MeteoSettings.WIDGETS_ZONE);
        PREF_WIDGETS.add(MeteoSettings.WIDGETS_WEBCAM_UPDATE_INTERVAL);
    }

    /**
     * Verifica se la chiave fa parte delle settings relative ai widgets. In questo caso viene lanciato un evento per
     * far si che tutti i widgets di aggiornino.
     *
     * @param key chiave delle settings
     */
    private void checkAndThrowWidgetSettingsChange(String key) {

        if (PREF_WIDGETS.contains(key)) {
            Intent intent = new Intent(MeteoWidgetProvider.UPDATE_SETTINGS);
            getActivity().sendBroadcast(intent);
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Load the preferences from an XML resource
        addPreferencesFromResource(R.xml.preferences);

        getPreferenceManager().getSharedPreferences().registerOnSharedPreferenceChangeListener(SettingsFragment.this);

        // init dei summary delle preference
        Set<String> keyPref = getPreferenceManager().getSharedPreferences().getAll().keySet();
        for (String key : keyPref) {
            updatePreference(getPreferenceManager().findPreference(key));
        }

    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        updatePreference(findPreference(key));

        // nel caso in cui venga cambiata la lingua chiedo se riavviare l'app per applicare i cambiamenti
        if (MeteoSettings.LANGUAGE.equals(key)) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle(R.string.change_language_dialog_title).setMessage(R.string.change_language_dialog_message)
                    .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {
                            Intent mStartActivity = new Intent(getActivity(), SplashActivity.class);
                            int mPendingIntentId = 123456;
                            PendingIntent mPendingIntent = PendingIntent.getActivity(getActivity(), mPendingIntentId,
                                    mStartActivity, PendingIntent.FLAG_CANCEL_CURRENT);
                            AlarmManager mgr = (AlarmManager) getActivity().getSystemService(Context.ALARM_SERVICE);
                            mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 100, mPendingIntent);
                            System.exit(0);
                        }
                    }).setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int id) {
                    // non faccio niente
                }
            });
            builder.create().show();
        } else if (MeteoSettings.ICONS_THEME.equals(key)) {
            IconsThemeManager.getInstance(getActivity()).clearCache();
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle(R.string.change_icons_theme_dialog_title).setMessage(R.string.change_icons_theme_dialog_message)
                    .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {
                            Intent mStartActivity = new Intent(getActivity(), SplashActivity.class);
                            int mPendingIntentId = 123456;
                            PendingIntent mPendingIntent = PendingIntent.getActivity(getActivity(), mPendingIntentId,
                                    mStartActivity, PendingIntent.FLAG_CANCEL_CURRENT);
                            AlarmManager mgr = (AlarmManager) getActivity().getSystemService(Context.ALARM_SERVICE);
                            mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 100, mPendingIntent);
                            System.exit(0);
                        }
                    }).setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int id) {
                    // non faccio niente
                }
            });
            builder.create().show();
        }

        checkAndThrowWidgetSettingsChange(key);
    }

    /**
     * Aggiorna le preference.
     *
     * @param preference preference da aggiornare
     */
    private void updatePreference(Preference preference) {
        if (preference instanceof ListPreference) {
            ListPreference listPreference = (ListPreference) preference;
            listPreference.setSummary(listPreference.getEntry());
        } else if (preference instanceof NumberPickerDialogPreference) {
            NumberPickerDialogPreference dialogPreference = (NumberPickerDialogPreference) preference;
            int value = dialogPreference.getValue();
            dialogPreference.setSummary(getActivity()
                    .getString(R.string.weather_stations_pref_graph_points_radius_desc)
                    + "("
                    + String.valueOf(value)
                    + ")");
        } else if (preference instanceof EditTextPreference) {
            EditTextPreference textPreference = (EditTextPreference) preference;
            String value = textPreference.getText();
            int inputType = textPreference.getEditText().getInputType();
            if (((inputType & InputType.TYPE_NUMBER_VARIATION_PASSWORD) != 0)
                    || ((inputType & InputType.TYPE_TEXT_VARIATION_PASSWORD) != 0)) {
                value = value.replaceAll(".", "*");
            }
            textPreference.setSummary(value);
        } else if (preference instanceof ZoneWidgetPreference) {
            ZoneWidgetPreference zoneWidgetPreference = (ZoneWidgetPreference) preference;
            zoneWidgetPreference.setSummary(getActivity().getString(R.string.widgets_pref_zone_summary) + " "
                    + zoneWidgetPreference.getZoneValue());
        }
    }

    public void backPressed() {
        FragmentUtils.Companion.replace((AppCompatActivity) getActivity(), HomeFragment_.builder().build());
    }
}
