package com.gmail.fattazzo.meteo.domain.webcam;

import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

/**
 *
 * @author fattazzo
 *
 *         date: 13/lug/2015
 *
 */
@XStreamAlias("zone")
public class ZoneWebcam {

    @XStreamAlias("description")
    @XStreamAsAttribute
    private String descrizione;

    @XStreamImplicit(itemFieldName = "webcam")
    private List<Webcam> webcams;

    /**
     * @return the descrizione
     */
    public String getDescrizione() {
        return descrizione;
    }

    /**
     * @return the webcams
     */
    public List<Webcam> getWebcams() {
        return webcams;
    }

    /**
     * @param descrizione
     *            the descrizione to set
     */
    public void setDescrizione(String descrizione) {
        this.descrizione = descrizione;
    }

    /**
     * @param webcams
     *            the webcams to set
     */
    public void setWebcams(List<Webcam> webcams) {
        this.webcams = webcams;
    }

    @Override
    public String toString() {
        return "ZoneWebcam [descrizione=" + descrizione + "]";
    }
}
