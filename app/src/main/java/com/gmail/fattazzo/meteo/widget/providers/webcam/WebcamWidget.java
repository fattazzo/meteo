package com.gmail.fattazzo.meteo.widget.providers.webcam;

import com.gmail.fattazzo.meteo.domain.webcam.Webcam;

/**
 *
 * @author fattazzo
 *
 *         date: 24/lug/2015
 *
 */
public class WebcamWidget {

    private int id;

    private String descrizione;

    private int idLink;

    private String link;

    /**
     * Costruttore.
     *
     * @param webcam
     *            webcam
     */
    public WebcamWidget(final Webcam webcam) {
        super();
        this.id = webcam.getId();
        this.descrizione = webcam.getDescrizione();
    }

    /**
     * @return the descrizione
     */
    public String getDescrizione() {
        return descrizione;
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @return the idLink
     */
    public int getIdLink() {
        return idLink;
    }

    /**
     * @return the link
     */
    public String getLink() {
        return link;
    }

    /**
     * @param idLink
     *            the idLink to set
     */
    public void setIdLink(int idLink) {
        this.idLink = idLink;
    }

    /**
     * @param link
     *            the link to set
     */
    public void setLink(String link) {
        this.link = link;
    }
}
