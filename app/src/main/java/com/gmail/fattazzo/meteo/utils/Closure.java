package com.gmail.fattazzo.meteo.utils;

/**
 *
 * @author Fattazzo
 *
 *         Date 14/giu/2014
 *
 * @param <T>
 *            type
 */
public interface Closure<T> {

    /**
     * Execute method.
     *
     * @param paramObject
     *            object
     */
    void execute(T paramObject);
}
