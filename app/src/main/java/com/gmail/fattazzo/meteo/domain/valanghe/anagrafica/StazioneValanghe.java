package com.gmail.fattazzo.meteo.domain.valanghe.anagrafica;

import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 *
 * @author fattazzo
 *
 *         date: 01/feb/2016
 *
 */
public class StazioneValanghe {

    @XStreamAlias("codice")
    private String codice;

    @XStreamAlias("nome")
    private String nome;

    @XStreamAlias("nomebreve")
    private String nomeBreve;

    @XStreamAlias("quota")
    private Integer quota;

    @XStreamAlias("utmlat")
    private Double latitudine;

    @XStreamAlias("utmlon")
    private Double longitudine;

    /**
     * @return the codice
     */
    public String getCodice() {
        return codice;
    }

    /**
     * @return the latitudine
     */
    public Double getLatitudine() {
        return latitudine;
    }

    /**
     * @return the longitudine
     */
    public Double getLongitudine() {
        return longitudine;
    }

    /**
     * @return the nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * @return the nomeBreve
     */
    public String getNomeBreve() {
        return nomeBreve;
    }

    /**
     * @return the quota
     */
    public Integer getQuota() {
        return quota;
    }

    /**
     * @param codice
     *            the codice to set
     */
    public void setCodice(String codice) {
        this.codice = codice;
    }

    /**
     * @param latitudine
     *            the latitudine to set
     */
    public void setLatitudine(Double latitudine) {
        this.latitudine = latitudine;
    }

    /**
     * @param longitudine
     *            the longitudine to set
     */
    public void setLongitudine(Double longitudine) {
        this.longitudine = longitudine;
    }

    /**
     * @param nome
     *            the nome to set
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * @param nomeBreve
     *            the nomeBreve to set
     */
    public void setNomeBreve(String nomeBreve) {
        this.nomeBreve = nomeBreve;
    }

    /**
     * @param quota
     *            the quota to set
     */
    public void setQuota(Integer quota) {
        this.quota = quota;
    }

}
