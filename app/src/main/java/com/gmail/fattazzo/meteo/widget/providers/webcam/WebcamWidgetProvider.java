package com.gmail.fattazzo.meteo.widget.providers.webcam;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.widget.RemoteViews;

import com.gmail.fattazzo.meteo.R;
import com.gmail.fattazzo.meteo.domain.webcam.Webcams;
import com.gmail.fattazzo.meteo.parser.xml.WebcamXmlParser;
import com.gmail.fattazzo.meteo.settings.widget.webcam.WebcamWidgetsSettingsManager;
import com.gmail.fattazzo.meteo.utils.LoadBitmapTask;
import com.gmail.fattazzo.meteo.widget.providers.MeteoWidgetProvider;

/**
 *
 * @author fattazzo
 *
 *         date: 24/lug/2015
 *
 */
public class WebcamWidgetProvider extends MeteoWidgetProvider {

    public static final String WEBCAM_CURRENT_ID = "webcamCurrentId";
    public static final String WEBCAM_CURRENT_LINK_INDEX = "webcamCurrentLinkIndex";

    private WebcamWidgetsSettingsManager widgetsSettingsManager;

    private WebcamXmlParser webcamXmlParser;

    @Override
    protected void doUpdate(RemoteViews remoteViews, Context context, AppWidgetManager appWidgetManager,
            int[] appWidgetIds, boolean forRevalidate) {

        if (appWidgetIds != null && appWidgetIds.length > 0) {

            Intent intent = new Intent(context, this.getClass());
            intent.setAction(MeteoWidgetProvider.WIDGET_REVALIDATE);
            intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, appWidgetIds);
            PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, intent,
                    PendingIntent.FLAG_UPDATE_CURRENT);
            remoteViews.setOnClickPendingIntent(R.id.widget_sync, pendingIntent);

            Webcams webcam = getWebcamXmlParser().caricaWebcam(context);
            WebcamWidget webcamWidget;
            if (forRevalidate) {
                webcamWidget = WebcamWidgetManager.getCurrentWebcam(context, webcam.getWebcams());
            } else {
                webcamWidget = WebcamWidgetManager.getNextWebcam(context, webcam);
            }

            for (final int widgetId : appWidgetIds) {

                if (webcamWidget != null) {
                    // riduco la dimensione dell'immagine altrimenti incorro in problemi di OutOfMemory
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inSampleSize = 2;
                    LoadBitmapTask loadBitbamTask = (LoadBitmapTask) new LoadBitmapTask(options).execute(webcamWidget
                            .getLink());

                    Bitmap bitmapResult;
                    try {
                        bitmapResult = loadBitbamTask.get();
                    } catch (Exception e) {
                        bitmapResult = null;
                    }

                    remoteViews.setBitmap(R.id.widget_webcam_image, "setImageBitmap", bitmapResult);
                    remoteViews.setTextViewText(R.id.widget_webcam_title_text, webcamWidget.getDescrizione());
                } else {
                    remoteViews.setBitmap(R.id.widget_webcam_image, "setImageBitmap", null);
                    remoteViews.setTextViewText(R.id.widget_webcam_title_text, "Nessuna webcam configurata.");
                }

                appWidgetManager.updateAppWidget(widgetId, remoteViews);
            }
        }
    }

    @Override
    protected int getOpenAppResourceView() {
        return 0;
    }

    @Override
    protected int getRefreshResourceView() {
        // utilizzo il next button per andare alla prossima webcam facendo eseguire un refresh al widget
        return R.id.widget_webcam_next_button;
    }

    @Override
    protected int getRemoteViewsLayoutResource() {
        return R.layout.widget_webcam_4x4;
    }

    /**
     * @return webcam parser
     */
    public WebcamXmlParser getWebcamXmlParser() {
        if (webcamXmlParser == null) {
            webcamXmlParser = new WebcamXmlParser();
        }

        return webcamXmlParser;
    }

    @Override
    protected WebcamWidgetsSettingsManager getWidgetsSettingsManager(Context context) {
        if (widgetsSettingsManager == null) {
            widgetsSettingsManager = new WebcamWidgetsSettingsManager(context);
        }

        return widgetsSettingsManager;
    }

    @Override
    protected void updateBackground(RemoteViews remoteViews, int backgroundColor) {
        remoteViews.setInt(R.id.widget_webcam_root_panel, "setBackgroundResource", backgroundColor);
    }

    @Override
    protected void updateTextColor(RemoteViews remoteViews, int textColor) {
        remoteViews.setTextColor(R.id.widget_webcam_title_text, textColor);
    }

}
