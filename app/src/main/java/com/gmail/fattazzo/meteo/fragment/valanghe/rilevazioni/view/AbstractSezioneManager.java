package com.gmail.fattazzo.meteo.fragment.valanghe.rilevazioni.view;

import java.util.List;

import android.view.View;

import com.gmail.fattazzo.meteo.domain.valanghe.dati.DatiStazione;
import com.gmail.fattazzo.meteo.fragment.valanghe.rilevazioni.LegendaDatiNeveManager;
import com.gmail.fattazzo.meteo.preferences.ApplicationPreferencesManager;
import com.gmail.fattazzo.meteo.preferences.ApplicationPreferencesManager_;
import com.gmail.fattazzo.meteo.settings.MeteoSettingsManager;
import com.gmail.fattazzo.meteo.utils.Fx;

/**
 *
 * @author fattazzo
 *
 *         date: 03/feb/2016
 *
 */
public abstract class AbstractSezioneManager implements IRilevazioneSezioneViewManager {

    protected final View view;

    private View toggleView;

    private boolean loadedContent = false;

    private List<DatiStazione> datiStazione;

    private LegendaDatiNeveManager legendaDatiNeveManager;

    /**
     * Costruttore.
     *
     * @param view
     *            view
     */
    public AbstractSezioneManager(final View view) {
        super();
        this.view = view;
    }

    @Override
    public void clear() {

        clearContent();

        loadedContent = false;

        hide();

    }

    /**
     * Cancella il contenuto della sezione.
     */
    protected abstract void clearContent();

    /**
     * @return the datiStazione
     */
    public List<DatiStazione> getDatiStazione() {
        return datiStazione;
    }

    /**
     * @return id della risorsa utilizzata per visualizzare/nascondere la sezione
     */
    protected abstract int getHeaderViewId();

    /**
     * @return the legendaDatiNeveManager
     */
    public LegendaDatiNeveManager getLegendaDatiNeveManager() {
        if (legendaDatiNeveManager == null) {
            ApplicationPreferencesManager preferencesManager = ApplicationPreferencesManager_.getInstance_(view.getContext());
            legendaDatiNeveManager = LegendaDatiNeveManager.getInstance(view.getContext(),
                    preferencesManager.getAppLocale());

        }

        return legendaDatiNeveManager;
    }

    /**
     * @return view da visualizzare/nascondere
     */
    private View getToggleView() {
        if (toggleView == null) {
            toggleView = view.findViewById(getToggleViewId());
        }

        return toggleView;
    }

    /**
     * @return id della view da visualizzare/nascondere
     */
    protected abstract int getToggleViewId();

    /**
     * Nasconde la view dei dati della sezione.
     */
    private void hide() {
        Fx.slideUp(getToggleView());
    }

    /**
     * Carica i dati da visualizzare.
     */
    protected abstract void loadContent();

    /**
     * @param datiStazione
     *            the datiStazione to set
     */
    public void setDatiStazione(List<DatiStazione> datiStazione) {
        this.datiStazione = datiStazione;
    }

    /**
     * Visualizza la view dei dati della sezione.
     */
    private void show() {
        Fx.slideDown(getToggleView());
    }

    @Override
    public void toggleContent() {

        if (getToggleView().isShown()) {
            hide();
        } else {
            if (!loadedContent) {
                loadContent();
                loadedContent = true;
            }
            show();
        }

    }

}
