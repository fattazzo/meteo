package com.gmail.fattazzo.meteo.fragment.valanghe.rilevazioni.sezioni.ventoquotafenomeni;

import java.util.ArrayList;
import java.util.List;

import android.view.View;

import com.gmail.fattazzo.meteo.R;
import com.gmail.fattazzo.meteo.domain.valanghe.dati.DatiStazione;
import com.gmail.fattazzo.meteo.fragment.valanghe.rilevazioni.sezioni.RilevazioneSezione;
import com.gmail.fattazzo.meteo.fragment.valanghe.rilevazioni.sezioni.RilevazioneSezioneListAdapter;
import com.gmail.fattazzo.meteo.fragment.valanghe.rilevazioni.view.AbstractSezioneManager;
import com.gmail.fattazzo.meteo.utils.ExpandableHeightListView;

/**
 *
 * @author fattazzo
 *
 *         date: 04/feb/2016
 *
 */
public class VentoQuotaFenomeniSezioneManager extends AbstractSezioneManager {

    /**
     * Costruttore.
     *
     * @param view
     *            view
     */
    public VentoQuotaFenomeniSezioneManager(final View view) {
        super(view);
    }

    @Override
    protected void clearContent() {
        ExpandableHeightListView condTempoList = (ExpandableHeightListView) view.findViewById(R.id.neve_vqf_list);
        condTempoList.setAdapter(null);
    }

    @Override
    protected int getHeaderViewId() {
        return R.id.anag_stazione_neve_vqf_layout;
    }

    @Override
    protected int getToggleViewId() {
        return R.id.anag_stazione_neve_vqfData_layout;
    }

    @Override
    protected void loadContent() {
        List<RilevazioneSezione> vqfList = new ArrayList<>();
        for (DatiStazione dato : getDatiStazione()) {
            RilevazioneSezione vqf = new RilevazioneSezione();
            vqf.setData(dato.getDataMisurazione());
            String descrizione = getLegendaDatiNeveManager().getMapVentoQuotaFenomeni().get(
                    dato.getVentoQuotaLocalizzazione());
            vqf.setDescrizione(descrizione != null ? descrizione : "");
            vqfList.add(vqf);
        }

        RilevazioneSezioneListAdapter adapter = new RilevazioneSezioneListAdapter(view.getContext(), vqfList);

        ExpandableHeightListView listView = (ExpandableHeightListView) view.findViewById(R.id.neve_vqf_list);
        listView.setAdapter(adapter);
        listView.setExpanded(true);

    }
}
