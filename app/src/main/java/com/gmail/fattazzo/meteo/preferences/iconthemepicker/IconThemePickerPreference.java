package com.gmail.fattazzo.meteo.preferences.iconthemepicker;

import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Resources;
import android.graphics.BitmapFactory;
import android.preference.ListPreference;
import android.preference.PreferenceManager;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.gmail.fattazzo.meteo.R;
import com.gmail.fattazzo.meteo.settings.MeteoSettings;
import com.gmail.fattazzo.meteo.utils.Closure;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author fattazzo
 *         <p/>
 *         date: 23/02/16
 */
public class IconThemePickerPreference extends ListPreference {

    private static final String TAG = "IconThemePreference";

    public static final String ICON_NAME_DEMO1 = "ico21_1_0.png";
    public static final String ICON_NAME_DEMO2 = "ico17_0_1.png";
    public static final String ICON_NAME_DEMO3 = "ico18_0_0.png";


    private Context context;
    private ImageView icon1;
    private ImageView icon2;
    private ImageView icon3;

    private CharSequence[] iconName;
    private List<IconThemeItem> icons;
    private SharedPreferences preferences;
    private Resources resources;
    private String selectedIconPath, defaultIconFile;
    private TextView summary;

    public IconThemePickerPreference(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;

        resources = context.getResources();
        preferences = PreferenceManager.getDefaultSharedPreferences(context);

        defaultIconFile = "meteo_trentino";
    }

    private String getEntry(String value) {
        String[] entries = resources.getStringArray(R.array.iconThemeName);
        String[] values = resources.getStringArray(R.array.iconThemePath);
        int index = Arrays.asList(values).indexOf(value);
        try {
            return entries[index];
        } catch (Exception e) {
            return entries[0];
        }

    }

    @Override
    protected void onBindView(View view) {
        super.onBindView(view);

        selectedIconPath = preferences.getString(
                MeteoSettings.ICONS_THEME, defaultIconFile);

        icon1 = (ImageView) view.findViewById(R.id.iconSelected1);
        icon2 = (ImageView) view.findViewById(R.id.iconSelected2);
        icon3 = (ImageView) view.findViewById(R.id.iconSelected3);
        updateIcon();

        summary = (TextView) view.findViewById(R.id.icon_theme_pref_summary);
        summary.setText(getEntry(selectedIconPath));

    }

    @Override
    protected void onDialogClosed(boolean positiveResult) {
        super.onDialogClosed(positiveResult);

        if (icons != null) {
            for (int i = 0; i < iconName.length; i++) {
                IconThemeItem item = icons.get(i);
                if (item.isChecked()) {

                    Editor editor = preferences.edit();
                    editor.putString(
                            MeteoSettings.ICONS_THEME,
                            item.getPath());
                    editor.apply();

                    selectedIconPath = item.getPath();
                    updateIcon();

                    summary.setText(item.getName());

                    break;
                }
            }
        }

    }

    @Override
    protected void onPrepareDialogBuilder(Builder builder) {

        builder.setNegativeButton("Cancel", null);
        builder.setPositiveButton(null, null);

        iconName = getEntries();
        CharSequence[] iconPath = getEntryValues();

        if (iconName == null || iconPath == null
                || iconName.length != iconPath.length) {
            throw new IllegalStateException(
                    "ListPreference requires an entries array "
                            + "and an entryValues array which are both the same length");
        }

        String selectedIcon = preferences.getString(
                MeteoSettings.ICONS_THEME,
                "meteo_trentino");

        icons = new ArrayList<>();

        for (int i = 0; i < iconName.length; i++) {
            boolean isSelected = selectedIcon.equals(iconPath[i]);
            IconThemeItem item = new IconThemeItem(iconName[i], iconPath[i], isSelected);
            icons.add(item);
        }

        IconThemeListPreferenceAdapter adapter = new IconThemeListPreferenceAdapter(
                context, R.layout.icon_theme_item_picker, icons);
        adapter.setOnThemeSelectedClosure(new Closure<Void>() {
            @Override
            public void execute(Void paramObject) {
                getDialog().dismiss();
            }
        });
        builder.setAdapter(adapter, null);

    }

    private void updateIcon() {
        try {
            InputStream iconStream = resources.getAssets().open("iconsTheme/" + selectedIconPath + "/" + ICON_NAME_DEMO1);
            icon1.setImageBitmap(BitmapFactory.decodeStream(iconStream));
            icon1.setTag(selectedIconPath);

            iconStream = resources.getAssets().open("iconsTheme/" + selectedIconPath + "/" + ICON_NAME_DEMO2);
            icon2.setImageBitmap(BitmapFactory.decodeStream(iconStream));
            icon2.setTag(selectedIconPath);

            iconStream = resources.getAssets().open("iconsTheme/" + selectedIconPath + "/" + ICON_NAME_DEMO3);
            icon3.setImageBitmap(BitmapFactory.decodeStream(iconStream));
            icon3.setTag(selectedIconPath);
        } catch (IOException e) {
            Log.e(TAG,"Impossibile caricare l'icona nel path iconsTheme/" + selectedIconPath);
        }
    }

}