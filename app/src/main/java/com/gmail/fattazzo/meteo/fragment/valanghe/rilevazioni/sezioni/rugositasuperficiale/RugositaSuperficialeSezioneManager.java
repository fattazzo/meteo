package com.gmail.fattazzo.meteo.fragment.valanghe.rilevazioni.sezioni.rugositasuperficiale;

import java.util.ArrayList;
import java.util.List;

import android.view.View;

import com.gmail.fattazzo.meteo.R;
import com.gmail.fattazzo.meteo.domain.valanghe.dati.DatiStazione;
import com.gmail.fattazzo.meteo.fragment.valanghe.rilevazioni.sezioni.RilevazioneSezione;
import com.gmail.fattazzo.meteo.fragment.valanghe.rilevazioni.sezioni.RilevazioneSezioneListAdapter;
import com.gmail.fattazzo.meteo.fragment.valanghe.rilevazioni.view.AbstractSezioneManager;
import com.gmail.fattazzo.meteo.utils.ExpandableHeightListView;

/**
 *
 * @author fattazzo
 *
 *         date: 04/feb/2016
 *
 */
public class RugositaSuperficialeSezioneManager extends AbstractSezioneManager {

    /**
     * Costruttore.
     *
     * @param view
     *            view
     */
    public RugositaSuperficialeSezioneManager(final View view) {
        super(view);
    }

    @Override
    protected void clearContent() {
        ExpandableHeightListView condTempoList = (ExpandableHeightListView) view
                .findViewById(R.id.neve_rugositasuperficiale_list);
        condTempoList.setAdapter(null);
    }

    @Override
    protected int getHeaderViewId() {
        return R.id.anag_stazione_neve_rugositasuperficiale_layout;
    }

    @Override
    protected int getToggleViewId() {
        return R.id.anag_stazione_neve_rugositasuperficialeData_layout;
    }

    @Override
    protected void loadContent() {
        List<RilevazioneSezione> rugositasuperficialeList = new ArrayList<>();
        for (DatiStazione dato : getDatiStazione()) {
            RilevazioneSezione rugositasuperficiale = new RilevazioneSezione();
            rugositasuperficiale.setData(dato.getDataMisurazione());
            String descrizione = getLegendaDatiNeveManager().getMapRugositaSuperficiale().get(
                    dato.getRugositaSuperficiale());
            rugositasuperficiale.setDescrizione(descrizione != null ? descrizione : "");
            rugositasuperficialeList.add(rugositasuperficiale);
        }

        RilevazioneSezioneListAdapter adapter = new RilevazioneSezioneListAdapter(view.getContext(),
                rugositasuperficialeList);

        ExpandableHeightListView listView = (ExpandableHeightListView) view
                .findViewById(R.id.neve_rugositasuperficiale_list);
        listView.setAdapter(adapter);
        listView.setExpanded(true);

    }
}
