package com.gmail.fattazzo.meteo.fragment.valanghe.rilevazioni;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Pattern;

import android.content.Context;
import android.content.res.AssetManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.util.DisplayMetrics;

import com.gmail.fattazzo.meteo.R;

/**
 *
 * @author fattazzo
 *
 *         date: 02/feb/2016
 *
 */
public class LegendaDatiNeveManager {

    private static LegendaDatiNeveManager instance = null;

    private static final String SEPARATOR = "|";

    /**
     * @param context
     *            context
     * @param locale
     *            locale
     * @return instance of {@link LegendaDatiNeveManager}
     */
    public static LegendaDatiNeveManager getInstance(Context context, Locale locale) {
        if (instance == null) {
            instance = new LegendaDatiNeveManager(context, locale);
        }
        return instance;
    }

    private Map<String, String> mapCondMeteo = null;
    private Map<String, String> mapNuvolosita = null;
    private Map<String, String> mapVisibilita = null;
    private Map<String, String> mapVentoQuotaTipo = null;
    private Map<String, String> mapVentoQuotaFenomeni = null;
    private Map<String, String> mapStratoSuperficiale = null;
    private Map<String, String> mapRugositaSuperficiale = null;
    private Map<String, String> mapBrinaSuperficie = null;

    private Context context;
    private Locale locale;
    private Resources resources = null;

    /**
     * Costruttore.
     *
     * @param context
     *            context
     * @param locale
     *            locale
     */
    protected LegendaDatiNeveManager(final Context context, final Locale locale) {
        this.context = context;
        this.locale = locale;
    }

    /**
     * Carica l'array con id specificato e ritorna la mappa dei sui valori.
     *
     * @param arrayId
     *            id array
     * @return map
     */
    private Map<String, String> getMap(int arrayId) {
        String[] nArray = this.getResources().getStringArray(arrayId);

        return parseArray(nArray);
    }

    /**
     * @return the mapBrinaSuperficie
     */
    public Map<String, String> getMapBrinaSuperficie() {
        if (mapBrinaSuperficie == null) {
            mapBrinaSuperficie = getMap(R.array.snow_data_b);
        }
        return mapBrinaSuperficie;
    }

    /**
     * @return the mapCondMeteo
     */
    public Map<String, String> getMapCondMeteo() {
        if (mapCondMeteo == null) {
            mapCondMeteo = getMap(R.array.snow_data_ww);
        }
        return mapCondMeteo;
    }

    /**
     * @return the mapNuvolosita
     */
    public Map<String, String> getMapNuvolosita() {
        if (mapNuvolosita == null) {
            mapNuvolosita = getMap(R.array.snow_data_n);
        }
        return mapNuvolosita;
    }

    /**
     * Esegue il parse della temperatura.
     * Per temperature negative si aggiunge il valore 50.
     * Alcuni esempi:
     * -5,00° = 55
     * -5,30° = 55
     * -12,70° = 63
     * +0,00° = 00
     * +1,20° = 01
     * +1,80° = 02
     *
     * Rilievo non possibile = //
     * Casi particolari da 0,0° fino a +0,4° si cifra 00, da -0,1° fino a -0,4° si cifra 50.
     *
     * @param temperatura dato della temperatura
     * @return parse
     */
    public String parseTemperatura(String temperatura) {
        String result = this.getResources().getString(R.string.snow_section_air_temperature_nodata);

        if(temperatura != null && !"//".equals(temperatura)) {
            // Si dovrebbero applicare le formule descritte nel javadoc del metodo per ricavare la temperatura ma
            // nel file sono riportate normalmente
            result = temperatura.trim() + "°C";
        }

        return result;
    }

    /**
     * Viene letta sull'asta nivometrica e codificata in centimetri.
     *
     * Esempi:
     * 5 cm = 005
     * 8,3 cm = 008
     * 120 cm = 120
     *
     * @param altezzaString altezza in formato string
     * @return parse
     */
    public String parseAltezzaMantoNevoso(String altezzaString) {

        if(altezzaString == null || altezzaString.trim().isEmpty()) {
            return  this.getResources().getString(R.string.snow_section_air_temperature_nodata);
        }

        Integer altezza;
        try {
            altezza = Integer.parseInt(altezzaString);
        } catch (Exception e) {
            altezza = 0;
        }
        return altezza.intValue() + " cm";
    }

    /**
     * Neve fresca caduta fra due osservazioni successive e misurata sulla tavoletta.
     * La tavoletta va ripulita dopo ogni misura.
     *
     * Esempi:
     * 20cm = 020
     * 120cm = 120
     * meno di 0,5cm (tracce) = 999
     *
     * Rilievo non possibile = ///
     *
     * Se nelle ore precedenti è piovuto sulla neve, la prima cifra del gruppo sarà 8.
     *
     * Esempi:
     * pioggia su 40cm di neve fresca = 840
     * con meno di 0.5cm (tracce) = 899
     * con più di 98cm di neve fresca = 8//
     *
     * il valore si indicherà nel testo in chiaro
     * assenza di neve fresca ma piove sulla neve = 800
     *
     * @param altezzaString altezza in formato string
     * @return parse
     */
    public String parseAltezzaNeveFresca(String altezzaString) {

        if(altezzaString == null || altezzaString.trim().isEmpty() || "///".equals(altezzaString.trim())) {
            return  this.getResources().getString(R.string.snow_section_air_temperature_nodata);
        }

        if("8//".equals(altezzaString.trim())) {
            return "Pioggia con più di 98cm di neve fresca";
        }

        if("800".equals(altezzaString.trim())) {
            return "Assenza di neve fresca ma piove sulla neve";
        }

        Integer altezza;
        try {
            altezza = Integer.parseInt(altezzaString);
        } catch (Exception e) {
            altezza = 0;
        }

        if(altezza == 999) {
            return "Meno di 0,5cm";
        } else  if(altezza == 899) {
            return "Pioggia con meno di 0,5cm di neve fresca";
        } else if(altezza > 800) {
            return "Pioggia su " + (altezza-800)  +"cm di neve fresca";
        } else {
            return (altezza-(altezza==0 ? 0 :800))  +"cm di neve fresca";
        }
    }

    /**
     * @return the mapRugositaSuperficiale
     */
    public Map<String, String> getMapRugositaSuperficiale() {
        if (mapRugositaSuperficiale == null) {
            mapRugositaSuperficiale = getMap(R.array.snow_data_s);
        }
        return mapRugositaSuperficiale;
    }

    /**
     * @return the mapStratoSuperficiale
     */
    public Map<String, String> getMapStratoSuperficiale() {
        if (mapStratoSuperficiale == null) {
            mapStratoSuperficiale = getMap(R.array.snow_data_cs);
        }
        return mapStratoSuperficiale;
    }

    /**
     * @return the mapVentoQuotaFenomeni
     */
    public Map<String, String> getMapVentoQuotaFenomeni() {
        if (mapVentoQuotaFenomeni == null) {
            mapVentoQuotaFenomeni = getMap(R.array.snow_data_vq2);
        }
        return mapVentoQuotaFenomeni;
    }

    /**
     * @return the mapVentoQuotaTipo
     */
    public Map<String, String> getMapVentoQuotaTipo() {
        if (mapVentoQuotaTipo == null) {
            mapVentoQuotaTipo = getMap(R.array.snow_data_vq1);
        }
        return mapVentoQuotaTipo;
    }

    /**
     * @return the mapVisibilita
     */
    public Map<String, String> getMapVisibilita() {
        if (mapVisibilita == null) {
            mapVisibilita = getMap(R.array.snow_data_v);
        }
        return mapVisibilita;
    }

    /**
     * @return the resources
     */
    public Resources getResources() {
        if (resources == null) {
            Resources standardResources = context.getResources();
            AssetManager assets = standardResources.getAssets();
            DisplayMetrics metrics = standardResources.getDisplayMetrics();
            Configuration config = new Configuration(standardResources.getConfiguration());
            config.locale = locale;
            resources = new Resources(assets, metrics, config);
        }
        return resources;
    }

    /**
     * Trasforma l'array dei valori della legenda in una mappa.
     *
     * @param arrayOfValues
     *            array
     * @return mappa
     */
    private Map<String, String> parseArray(String[] arrayOfValues) {
        Map<String, String> resultMap = new HashMap<>();

        for (String arrayOfValue : arrayOfValues) {
            String[] valueSplit = arrayOfValue.split(Pattern.quote(SEPARATOR));
            resultMap.put(valueSplit[0], valueSplit[1]);
        }

        return resultMap;
    }
}
