package com.gmail.fattazzo.meteo.utils;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.view.View;

/**
 * @author fattazzo
 *         <p/>
 *         date: 02/feb/2016
 */
public class Fx {

    /**
     * Costruttore.
     */
    private Fx() {

    }

    /**
     * Esegue lo slide down della view.
     *
     * @param v view
     */
    public static void slideDown(View v) {

        v.animate().setListener(null);
        v.setVisibility(View.VISIBLE);
        v.setAlpha(0.0f);
        v.animate().translationY(0).alpha(1.0f);
    }

    /**
     * Esegue lo slide up della view.
     *
     * @param v view
     */
    public static void slideUp(final View v) {

        v.animate().translationY(0).alpha(0.0f).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                v.setVisibility(View.GONE);
            }
        });
    }
}
