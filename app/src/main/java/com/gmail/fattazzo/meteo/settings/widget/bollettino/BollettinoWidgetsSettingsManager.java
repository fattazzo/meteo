package com.gmail.fattazzo.meteo.settings.widget.bollettino;

import android.content.Context;

import com.gmail.fattazzo.meteo.preferences.imagewidget.ZoneWidgetPreference;
import com.gmail.fattazzo.meteo.settings.MeteoSettings;
import com.gmail.fattazzo.meteo.settings.MeteoSettingsManager;
import com.gmail.fattazzo.meteo.settings.widget.WidgetSettingsManager;

/**
 *
 * @author fattazzo
 *
 *         date: 26/ago/2014
 *
 */
public class BollettinoWidgetsSettingsManager extends MeteoSettingsManager implements WidgetSettingsManager {

    private Context context;

    /**
     * Costruttore.
     *
     * @param context
     *            context
     */
    public BollettinoWidgetsSettingsManager(final Context context) {
        super(context);
        this.context = context;
    }

    /**
     * @return widget background
     */
    @Override
    public int getBackground() {

        String background = getPrefs().getString(MeteoSettings.WIDGETS_BACKGROUND, "widget_shape_transparent_40_black");

        return context.getResources().getIdentifier(background, "drawable", context.getPackageName());
    }

    /**
     * @return widget text color
     */
    @Override
    public int getTextColor() {
        return getPrefs().getInt(MeteoSettings.WIDGETS_TEXT_COLOR, 0xFFFFFFFF);
    }

    /**
     * @return widget update interval
     */
    @Override
    public int getUpdateInterval() {
        return Integer.parseInt(getPrefs().getString(MeteoSettings.WIDGETS_UPDATE_INTERVAL, "21600000"));
    }

    /**
     * @return widget text color
     */
    public String getZone() {
        return getPrefs().getString(MeteoSettings.WIDGETS_ZONE, ZoneWidgetPreference.DEFAULT_VALUE);
    }
}
