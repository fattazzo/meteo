package com.gmail.fattazzo.meteo.domain.json.previsione

import com.google.gson.annotations.SerializedName
import java.io.Serializable
import java.util.*

/**
 *
 * @author fattazzo
 *
 * date: 26/ott/2017
 */
class Giorno : Serializable {

    var idPrevisioneGiorno: Int? = null

    @SerializedName("giorno")
    var data: Date? = null

    var idIcona: Int? = null

    var icona: String? = null

    var descIcona: String? = null

    @SerializedName("testoGiorno")
    var testo: String? = null

    @SerializedName("tMinGiorno")
    var temperaturaMin: Int? = null

    @SerializedName("tMaxGiorno")
    var temperaturaMax: Int? = null

    var fasce: List<Fascia>? = null
}