package com.gmail.fattazzo.meteo.fragment

import android.text.method.LinkMovementMethod
import android.widget.TextView
import com.gmail.fattazzo.meteo.R
import com.gmail.fattazzo.meteo.parser.versioni.VersioniManager
import org.androidannotations.annotations.AfterViews
import org.androidannotations.annotations.Click
import org.androidannotations.annotations.EFragment
import org.androidannotations.annotations.ViewById

/**
 *
 * @author fattazzo
 *
 * date: 09/giu/2014
 */
@EFragment(R.layout.fragment_about)
open class AboutFragment : BaseFragment() {

    private var versioniManager: VersioniManager? = null

    @ViewById(R.id.about_project_link_label)
    lateinit internal var projectLabel: TextView

    @ViewById(R.id.about_bug_tracker)
    lateinit internal var bugTrackerLabel: TextView

    override fun getTitleResId(): Int {
        return R.string.nav_about
    }

    @Click
    fun aboutVersionClicked() {
        versioniManager!!.showVersionChangelog()
    }

    @AfterViews
    fun initViews() {
        versioniManager = VersioniManager(context)

        projectLabel.movementMethod = LinkMovementMethod.getInstance()

        bugTrackerLabel.movementMethod = LinkMovementMethod.getInstance()
    }

}
