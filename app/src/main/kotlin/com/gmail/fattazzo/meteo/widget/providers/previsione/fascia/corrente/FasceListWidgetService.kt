package com.gmail.fattazzo.meteo.widget.providers.previsione.fascia.corrente

import android.content.Intent
import android.widget.RemoteViewsService

class FasceListWidgetService : RemoteViewsService() {

    override fun onGetViewFactory(intent: Intent): RemoteViewsFactory {
        return FasceListRemoteViewsFactory(this.applicationContext, intent)
    }

}