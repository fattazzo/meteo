package com.gmail.fattazzo.meteo.widget.providers.previsione.fascia.corrente

import com.gmail.fattazzo.meteo.domain.json.previsione.Fascia
import java.util.*

/**
 * @author fattazzo
 *         <p/>
 *         date: 02/11/17
 */
class FasciaWidget(var data: Date?, var fascia: Fascia, var temperaturaMin: Int?, var temperaturaMax: Int?) {
}