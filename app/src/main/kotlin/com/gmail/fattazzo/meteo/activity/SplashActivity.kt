package com.gmail.fattazzo.meteo.activity

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.graphics.Typeface
import android.os.Bundle
import android.os.Handler
import android.preference.PreferenceManager
import android.util.TypedValue
import android.widget.TextView
import com.gmail.fattazzo.meteo.R
import com.gmail.fattazzo.meteo.preferences.ApplicationPreferencesManager_
import com.gmail.fattazzo.meteo.widget.providers.MeteoWidgetProvider

/**
 * @author fattazzo
 *
 *
 * date: 09/giu/2014
 */
@SuppressLint("DefaultLocale")
class SplashActivity : Activity() {

    /**
     * Inizializzazione delle lingua.
     */
    private fun initLanguage() {
        val preferencesManager = ApplicationPreferencesManager_.getInstance_(this)

        val res = this.resources
        val dm = res.displayMetrics
        val conf = res.configuration
        conf.locale = preferencesManager.getAppLocale()
        res.updateConfiguration(conf, dm)

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (savedInstanceState == null) {
            initLanguage()

            // initialize default settings for all preferences
            PreferenceManager.setDefaultValues(this, R.xml.preferences, false)
        }

        setContentView(R.layout.splash)
        val intent = Intent(MeteoWidgetProvider.UPDATE_SETTINGS)
        sendBroadcast(intent)

        val myTypeface = Typeface.createFromAsset(assets, "fonts/anarchistic.ttf")
        val titleTextView = findViewById<TextView>(R.id.about_link_bug_tracker)
        titleTextView.typeface = myTypeface
        titleTextView.setTextColor(resources.getColor(android.R.color.black))
        val size = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 50f, resources.displayMetrics).toInt()
        titleTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, size.toFloat())
    }

    override fun onResume() {
        super.onResume()

        Handler().postDelayed({
            MainActivity_.intent(this).start()
            this@SplashActivity.finish()
        }, 1000)
    }
}
