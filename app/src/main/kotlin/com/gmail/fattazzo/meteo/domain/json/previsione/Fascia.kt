package com.gmail.fattazzo.meteo.domain.json.previsione

import com.google.gson.annotations.SerializedName
import java.io.Serializable

/**
 * @author fattazzo
 *         <p/>
 *         date: 26/10/17
 */
open class Fascia : Serializable {

    var idPrevisioneFascia: Int? = null

    var fascia: String? = null

    @SerializedName("fasciaPer")
    var descrizione: String? = null

    @SerializedName("fasciaOre")
    var ore: String? = null

    var icona: String? = null

    var descIcona: String? = null

    @SerializedName("idPrecProb")
    var idProbabilitaPrecipitazioni: String? = null

    @SerializedName("descPrecProb")
    var descProbabilitaPrecipitazioni: String? = null

    @SerializedName("idPrecInten")
    var idIntensitaPrecipitazioni: String? = null

    @SerializedName("descPrecInten")
    var descIntensitaPrecipitazioni: String? = null

    @SerializedName("idTempProb")
    var idProbabilitaTemporali: String? = null

    @SerializedName("descTempProb")
    var descProbabilitaTemporali: String? = null

    @SerializedName("idVentoIntQuota")
    var idVentoIntensitaQuota: String? = null

    @SerializedName("descVentoIntQuota")
    var descVentoIntensitaQuota: String? = null

    @SerializedName("idVentoDirQuota")
    var idVentoDirezioneQuota: String? = null

    @SerializedName("descVentoDirQuota")
    var descVentoDirezioneQuota: String? = null

    @SerializedName("idVentoIntValle")
    var idVentoIntensitaValle: String? = null

    @SerializedName("descVentoIntValle")
    var descVentoIntensitaValle: String? = null

    @SerializedName("idVentoDirValle")
    var idVentoDirezioneValle: String? = null

    @SerializedName("descVentoDirValle")
    var descVentoDirezioneValle: String? = null

    var iconaVentoQuota: String? = null

    var zeroTermico: Int? = null
}