package com.gmail.fattazzo.meteo.parser.versioni

import android.content.Context
import android.content.pm.PackageManager.NameNotFoundException
import android.preference.PreferenceManager
import android.text.Html
import android.util.Log
import com.afollestad.materialdialogs.MaterialDialog
import com.gmail.fattazzo.meteo.R
import com.gmail.fattazzo.meteo.settings.MeteoSettings
import com.gmail.fattazzo.meteo.settings.MeteoSettingsManager
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStream
import java.io.InputStreamReader

/**
 *
 * @author fattazzo
 *
 * date: 13/lug/2015
 */
class VersioniManager
/**
 * Costruttore.
 *
 * @param context
 * context
 */
(private val context: Context) {

    /**
     * @return restituisce l'ultimo numero di versione dell'app
     */
    // non potrà essere rilanciata in quanto il nome versione sarà sempre presente nel manifest
    private val lastAppVersion: String
        get() {
            return try {
                context.packageManager.getPackageInfo(context.packageName, 0).versionName
            } catch (e: NameNotFoundException) {
                "-1"
            }

        }

    /**
     * @return restituisce la lista del changelog delle versioni
     */
    private fun caricaVersioni(): String {

        var result: String

        val meteoSettingsManager = MeteoSettingsManager(context)
        val lastVersionName = meteoSettingsManager.lastRunVersionName

        var stream: InputStream? = null
        var inputReader: InputStreamReader? = null
        var reader: BufferedReader? = null
        try {
            stream = context.assets.open("Changelog.txt")
            inputReader = InputStreamReader(stream!!)
            reader = BufferedReader(inputReader)

            result = parseReader(reader, lastVersionName)
        } catch (e: IOException) {
            result = "Changelog non disponibile"
        } finally {
            try {
                if (stream != null) {
                    stream.close()
                }
                if (inputReader != null) {
                    inputReader.close()
                }
                if (reader != null) {
                    reader.close()
                }
            } catch (e: IOException) {
                Log.e(TAG, "Errore durante la chiusura del file")
            }

        }

        return "<html>$result</html>"
    }

    /**
     * @return `true` se l'ultima versione utilizzata dell'app non coincide con l'ultima rilasciata
     */
    fun checkShowVersionChangelog(): Boolean {

        val lastAppVersion = lastAppVersion

        val meteoSettingsManager = MeteoSettingsManager(context)
        val lastVersionName = meteoSettingsManager.lastRunVersionName

        return lastVersionName != lastAppVersion
    }

    /**
     * Esegue il parse del reader delle versioni e mette in grassetto tutte le voci delle versioni maggiori di quella
     * passata ocme parametro.
     *
     * @param reader
     * reader
     * @param lastVersionName
     * versione di riferimento
     * @return changelog
     */
    private fun parseReader(reader: BufferedReader, lastVersionName: String): String {
        var result = StringBuilder(1000)

        try {
            // metto il testo in bold finchè la versione è maggiore dell'ultima utilizzata
            result.append("<b>")

            var line: String? = null
            while (({ line = reader.readLine(); line }()) != null) {

                if (line!!.startsWith("versione")) {
                    val lineSplit = line!!.split(" ".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                    if (lineSplit[1] == lastVersionName) {
                        result.append("</b>")
                    }

                    result.append("<u>")
                    result.append(line)
                    result.append("</u>")
                } else {
                    result.append(line)
                }
                result.append("<br>")

            }
        } catch (e: Exception) {
            result = StringBuilder("Changelog non disponibile")
        }

        return result.toString()
    }

    /**
     * Visualizza il dialog contenente il changelog delle versioni.
     */
    fun showVersionChangelog() {

        MaterialDialog.Builder(context)
                .icon(context.resources.getDrawable(R.drawable.info))
                .title("Note di rilascio")
                .content(Html.fromHtml(caricaVersioni()))
                .positiveText(android.R.string.ok)
                .positiveColorRes(R.color.colorPrimary)
                .show()

        val prefs = PreferenceManager.getDefaultSharedPreferences(context)
        val editor = prefs.edit()
        editor.putString(MeteoSettings.LAST_RUN_VERSION_NAME, lastAppVersion)
        editor.apply()
    }

    companion object {

        private val TAG = "VersioniManager"
    }
}
