package com.gmail.fattazzo.meteo.preferences

import android.annotation.SuppressLint
import org.androidannotations.annotations.EBean
import org.androidannotations.annotations.sharedpreferences.Pref
import java.util.*

/**
 * @author fattazzo
 *         <p/>
 *         date: 27/10/17
 */
@EBean(scope = EBean.Scope.Singleton)
open class ApplicationPreferencesManager {

    @Pref
    lateinit internal var pref: ApplicationPreferences_

    /**
     * @return restuisce il locale utilizzato dall'applicazione
     */
    @SuppressLint("DefaultLocale")
    fun getAppLocale(): Locale {
        val language = pref.language().getOr(Locale.getDefault().getLanguage())

        return Locale(language!!.toLowerCase())
    }

    /**
     * @return restituisce l'ultimo nome della versione utilizzata
     */
    fun getLastRunVersionName(): String {

        return pref.lastRunVersionName().getOr("0")
    }
}