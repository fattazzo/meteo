package com.gmail.fattazzo.meteo.parser

import com.gmail.fattazzo.meteo.Config
import com.gmail.fattazzo.meteo.domain.json.bollettino.probabilistico.BollettinoProbabilistico
import com.gmail.fattazzo.meteo.parser.xml.MeteoXmlParser
import com.google.gson.Gson
import org.androidannotations.annotations.EBean
import java.io.BufferedReader
import java.io.InputStreamReader

/**
 * @author fattazzo
 *         <p/>
 *         date: 26/10/17
 */
@EBean(scope = EBean.Scope.Singleton)
open class BollettinoProbabilisticoParser : MeteoXmlParser() {

    open fun caricaBollettino(): BollettinoProbabilistico {

        val stream = getInputStreamFromURL(Config.BOLLETTINO_PROBABILISTICO_URL)

        return Gson().fromJson<BollettinoProbabilistico>(BufferedReader(InputStreamReader(stream)), BollettinoProbabilistico::class.java)
    }
}