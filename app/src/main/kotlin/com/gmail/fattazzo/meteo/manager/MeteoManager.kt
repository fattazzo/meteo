package com.gmail.fattazzo.meteo.manager

import android.content.Context
import android.widget.Toast
import com.gmail.fattazzo.meteo.R
import com.gmail.fattazzo.meteo.domain.json.bollettino.probabilistico.BollettinoProbabilistico
import com.gmail.fattazzo.meteo.domain.json.previsione.PrevisioneLocalita
import com.gmail.fattazzo.meteo.parser.BollettinoGeneraleParser
import com.gmail.fattazzo.meteo.parser.BollettinoProbabilisticoParser
import org.androidannotations.annotations.Bean
import org.androidannotations.annotations.EBean
import org.androidannotations.annotations.RootContext
import org.androidannotations.annotations.UiThread
import java.lang.Exception

/**
 * @author fattazzo
 *         <p/>
 *         date: 27/10/17
 */
@EBean(scope = EBean.Scope.Singleton)
open class MeteoManager {

    @RootContext
    lateinit internal var context: Context

    @Bean
    lateinit internal var bollettinoGeneraleParser: BollettinoGeneraleParser
    @Bean
    lateinit internal var bollettinoProbabilisticoParser: BollettinoProbabilisticoParser

    @JvmOverloads
    fun caricaPrevisioneLocalita(suppressException: Boolean = false): PrevisioneLocalita? {
        try {
            return bollettinoGeneraleParser.caricaPrevisione()
        } catch (e: Exception) {
            if (!suppressException) {
                showErrorException()
            }
        }

        return null
    }

    fun caricaBollettinoProbabilistico(): BollettinoProbabilistico? {
        try {
            return bollettinoProbabilisticoParser.caricaBollettino()
        } catch (e: Exception) {
            showErrorException()
        }

        return null
    }

    @UiThread(propagation = UiThread.Propagation.REUSE)
    open fun showErrorException() {
        Toast.makeText(context, context.getString(R.string.service_error), Toast.LENGTH_LONG).show()
    }
}