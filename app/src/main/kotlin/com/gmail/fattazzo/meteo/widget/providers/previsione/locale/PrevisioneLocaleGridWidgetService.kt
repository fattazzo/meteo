package com.gmail.fattazzo.meteo.widget.providers.previsione.locale

import android.content.Intent
import android.widget.RemoteViewsService

class PrevisioneLocaleGridWidgetService : RemoteViewsService() {

    override fun onGetViewFactory(intent: Intent): RemoteViewsService.RemoteViewsFactory {
        return PrevisioneLocaleGridRemoteViewsFactory(this.applicationContext, intent)
    }

}