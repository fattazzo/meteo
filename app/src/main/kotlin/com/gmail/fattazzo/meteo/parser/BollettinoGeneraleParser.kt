package com.gmail.fattazzo.meteo.parser

import com.gmail.fattazzo.meteo.Config
import com.gmail.fattazzo.meteo.domain.json.previsione.PrevisioneLocalita
import com.gmail.fattazzo.meteo.parser.xml.MeteoXmlParser
import com.google.gson.Gson
import org.androidannotations.annotations.EBean
import java.io.BufferedReader
import java.io.InputStreamReader

/**
 * @author fattazzo
 *         <p/>
 *         date: 26/10/17
 */
@EBean(scope = EBean.Scope.Singleton)
open class BollettinoGeneraleParser : MeteoXmlParser() {

    open fun caricaPrevisione(): PrevisioneLocalita {

        val stream = getInputStreamFromURL(Config.PREVISIONE_LOCALITA_URL)

        return Gson().fromJson<PrevisioneLocalita>(BufferedReader(InputStreamReader(stream)), PrevisioneLocalita::class.java)
    }
}