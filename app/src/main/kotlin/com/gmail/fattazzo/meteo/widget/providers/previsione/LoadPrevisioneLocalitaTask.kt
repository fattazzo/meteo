package com.gmail.fattazzo.meteo.widget.providers.previsione

import android.os.AsyncTask
import com.gmail.fattazzo.meteo.domain.json.previsione.PrevisioneLocalita
import com.gmail.fattazzo.meteo.manager.MeteoManager

/**
 * @author fattazzo
 *         <p/>
 *         date: 02/11/17
 */
class LoadPrevisioneLocalitaTask(private val meteoManager: MeteoManager): AsyncTask<Void, Void, PrevisioneLocalita>() {

    override fun doInBackground(vararg p0: Void?): PrevisioneLocalita? {
        try {
            return meteoManager.caricaPrevisioneLocalita(true)
        } catch (e: Exception) {
            return null
        }
    }
}