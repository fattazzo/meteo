package com.gmail.fattazzo.meteo.fragment.home

import android.content.Context
import android.support.v7.widget.CardView
import android.util.AttributeSet
import android.view.View
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.gmail.fattazzo.meteo.R
import com.gmail.fattazzo.meteo.domain.json.previsione.Giorno
import com.gmail.fattazzo.meteo.utils.AnimationUtils
import com.squareup.picasso.Picasso
import org.androidannotations.annotations.Click
import org.androidannotations.annotations.EViewGroup
import org.androidannotations.annotations.ViewById
import java.text.SimpleDateFormat
import java.util.*


/**
 * @author fattazzo
 *         <p/>
 *         date: 26/10/17
 */
@EViewGroup(R.layout.item_giorno)
open class GiornoView : CardView {

    @ViewById
    lateinit internal var dataTV: TextView

    @ViewById
    lateinit internal var descrizioneTV: TextView

    @ViewById
    lateinit internal var temperatureMinTV: TextView

    @ViewById
    lateinit internal var temperatureMaxTV: TextView

    @ViewById
    lateinit internal var iconaImageView: ImageView

    @ViewById
    lateinit internal var detailButton: ImageButton

    @ViewById
    lateinit internal var detailLayout: LinearLayout

    constructor(context: Context?) : super(context)
    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    fun bind(giorno: Giorno) {

        val dateFormat = SimpleDateFormat("EEEE dd/MM/yyyy", Locale(Locale.getDefault().language.toLowerCase()))

        dataTV.text = dateFormat.format(giorno.data).capitalize()
        descrizioneTV.text = giorno.testo.orEmpty().capitalize()

        temperatureMinTV.text = String.format("%d°C", giorno.temperaturaMin)
        temperatureMaxTV.text = String.format("%d°C", giorno.temperaturaMax)

        detailButton.setImageResource(R.drawable.arrow_down)

        if (!giorno.icona.isNullOrBlank()) {
            Picasso.with(context).load(giorno.icona).into(iconaImageView)
        }

        val layoutParams = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
        layoutParams.setMargins(20, 20, 20, 0)
        for (fascia in giorno.fasce.orEmpty()) {
            val fasciaView = FasciaView_.build(context)
            fasciaView.bind(fascia)
            detailLayout.addView(fasciaView, layoutParams)
        }
    }

    @Click
    fun rootLayoutClicked() {
        if (detailLayout.visibility == View.GONE) openDetail() else closeDetail()
    }

    fun closeDetail() {
        detailButton.startAnimation(AnimationUtils.rotateAnimation(180f, 0f))

        detailLayout.animation = AnimationUtils.fadeAnimation(true)
        detailLayout.animate()
        detailLayout.visibility = View.GONE
    }

    fun openDetail() {
        detailButton.startAnimation(AnimationUtils.rotateAnimation(0f, 180f))

        detailLayout.animation = AnimationUtils.fadeAnimation(false)
        detailLayout.visibility = View.VISIBLE
        detailLayout.animate()

    }
}