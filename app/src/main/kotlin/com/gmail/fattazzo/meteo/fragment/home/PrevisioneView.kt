package com.gmail.fattazzo.meteo.fragment.home

import android.content.Context
import android.support.constraint.ConstraintLayout
import android.support.v7.widget.CardView
import android.util.AttributeSet
import android.view.View
import android.webkit.WebView
import android.widget.ImageButton
import android.widget.TextView
import com.gmail.fattazzo.meteo.R
import com.gmail.fattazzo.meteo.domain.json.previsione.PrevisioneLocalita
import com.gmail.fattazzo.meteo.utils.AnimationUtils
import org.androidannotations.annotations.Click
import org.androidannotations.annotations.EViewGroup
import org.androidannotations.annotations.ViewById
import java.text.SimpleDateFormat
import java.util.*

/**
 * @author fattazzo
 *
 *
 * date: 26/10/17
 */
@EViewGroup(R.layout.item_previsione_localita)
open class PrevisioneView : CardView {

    @ViewById
    lateinit internal var dataTV: TextView
    @ViewById
    lateinit internal var evoluzioneTempoTV: TextView

    @ViewById
    lateinit internal var detailLayout: ConstraintLayout

    @ViewById
    lateinit internal var infoImageView: ImageButton

    @ViewById
    lateinit internal var titolareTV: TextView

    @ViewById
    lateinit internal var editoreTV: TextView

    @ViewById
    lateinit internal var citazioneWebView: WebView

    private var previsione: PrevisioneLocalita? = null

    constructor(context: Context) : super(context)

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    fun bind(previsione: PrevisioneLocalita?) {
        this.previsione = previsione
        this.visibility = if (previsione == null) View.GONE else View.VISIBLE

        infoImageView.setImageResource(R.drawable.arrow_down)

        if (previsione != null) {
            dataTV.text = SimpleDateFormat("dd/MM/yyyy", Locale.ITALIAN).format(previsione.dataPubblicazione)
            evoluzioneTempoTV.text = previsione.evoluzioneBreve.orEmpty().capitalize()

            titolareTV.text = previsione.nomeTitolare
            editoreTV.text = previsione.nomeEditore

            citazioneWebView.loadData("<html><center>" + previsione.citazioneHtml + "</center></html>", "text/html", "UTF-8")
        }
    }

    @Click
    fun rootLayoutClicked() {
        if (detailLayout.visibility == View.GONE) openDetail() else closeDetail()
    }

    fun closeDetail() {
        infoImageView.startAnimation(AnimationUtils.rotateAnimation(180f, 0f))

        detailLayout.animation = AnimationUtils.fadeAnimation(true)
        detailLayout.animate()
        detailLayout.visibility = View.GONE
    }

    fun openDetail() {
        infoImageView.startAnimation(AnimationUtils.rotateAnimation(0f, 180f))

        detailLayout.animation = AnimationUtils.fadeAnimation(false)
        detailLayout.visibility = View.VISIBLE
        detailLayout.animate()

    }
}
