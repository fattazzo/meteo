package com.gmail.fattazzo.meteo.widget.providers.previsione.fascia.corrente

import android.app.PendingIntent
import android.appwidget.AppWidgetManager
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.widget.RemoteViews
import com.gmail.fattazzo.meteo.R
import com.gmail.fattazzo.meteo.activity.SplashActivity
import com.gmail.fattazzo.meteo.widget.providers.MeteoWidgetProvider
import org.androidannotations.annotations.EReceiver
import java.text.SimpleDateFormat
import java.util.*


/**
 * @author fattazzo
 *         <p/>
 *         date: 02/11/17
 */
@EReceiver
open class FasceProvider : MeteoWidgetProvider() {

    override fun doUpdate(remoteViews: RemoteViews, context: Context?, appWidgetManager: AppWidgetManager, appWidgetIds: IntArray?, forRevalidate: Boolean) {

        val calendar = Calendar.getInstance()

        if (appWidgetIds != null && appWidgetIds.isNotEmpty()) {

            val dateFormatter = SimpleDateFormat("HH:mm", Locale.getDefault())
            for (widgetId in appWidgetIds) {

                val intent = Intent(context, FasceListWidgetService::class.java)
                intent.putExtra(FasceListRemoteViewsFactory.EXTRA_DATA, dateFormatter.format(calendar.time))
                intent.data = Uri.parse(intent.toUri(Intent.URI_INTENT_SCHEME))
                remoteViews.setRemoteAdapter(R.id.fasceListView, intent)
                remoteViews.setEmptyView(R.id.fasceListView, R.id.errorTV)

                val showResultIntent = Intent(context, this.javaClass)
                showResultIntent.action = OPEN_APP_ACTION
                val showResultPendingIntent = PendingIntent.getBroadcast(context, 0, showResultIntent, PendingIntent.FLAG_UPDATE_CURRENT)
                remoteViews.setPendingIntentTemplate(R.id.fasceListView, showResultPendingIntent)

                appWidgetManager.updateAppWidget(widgetId, remoteViews)
            }
        }
    }

    override fun onReceive(context: Context, intent: Intent) {
        if (intent.action == OPEN_APP_ACTION) {
            val intentActivity = Intent(context, SplashActivity::class.java)
            intentActivity.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            context.startActivity(intentActivity)
        }
        super.onReceive(context, intent)
    }

    override fun getOpenAppResourceView(): Int {
        return 0
    }

    override fun getRemoteViewsLayoutResource(): Int {
        return R.layout.widget_fasce
    }

    override fun updateTextColor(remoteViews: RemoteViews?, textColor: Int) {

    }

    override fun onUpdate(context: Context?, appWidgetManager: AppWidgetManager?, appWidgetIds: IntArray?) {
        println("FasceProvider onUpdate")
        super.onUpdate(context, appWidgetManager, appWidgetIds)
    }

    companion object {
        val OPEN_APP_ACTION = "openAppAction"
    }
}