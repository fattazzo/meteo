package com.gmail.fattazzo.meteo.fragment.wiki

import android.webkit.WebView
import android.webkit.WebViewClient
import com.gmail.fattazzo.meteo.R
import com.gmail.fattazzo.meteo.fragment.BaseFragment
import com.gmail.fattazzo.meteo.settings.MeteoSettings
import org.androidannotations.annotations.*
import org.jsoup.Jsoup
import java.io.IOException

/**
 * @author fattazzo
 *
 *
 * date: 26/02/16
 */
@EFragment(R.layout.fragment_wiki)
open class WikiFragment : BaseFragment() {

    @ViewById
    lateinit var wikiWebView: WebView

    override fun getTitleResId(): Int {
        return R.string.nav_guida
    }

    @AfterViews
    internal fun initViews() {
        wikiWebView.webViewClient = object : WebViewClient() {
            override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
                return true
            }
        }
        wikiWebView.settings.builtInZoomControls = true

        openIndeterminateDialog("Caricamento guida in corso...")
        downloadWiki()
    }

    @Background
    internal open fun downloadWiki() {
        val result = try {
            val doc = Jsoup.connect(MeteoSettings.GIT_WIKI_LINK).get()
            val elements = doc.select(SECTION_WIKI_ID)
            elements.html()
        } catch (e: IOException) {
            ""
        }

        loadInWebView(result)
    }

    /**
     * Remove a tag from html but not content.
     *
     * @param html html
     * @return html purged
     */
    private fun purgeLinkFromHtml(html: String): String {
        return html.replace("(?i)<a[^>]*>".toRegex(), "")
    }

    @UiThread(propagation = UiThread.Propagation.REUSE)
    internal open fun loadInWebView(html: String) {
        try {
            wikiWebView.loadData(purgeLinkFromHtml(html), "text/html", "UTF-8")
        } finally {
            closeIndeterminateDialog()
        }
    }

    companion object {

        private val SECTION_WIKI_ID = "section[id=wiki-content]"
    }
}
