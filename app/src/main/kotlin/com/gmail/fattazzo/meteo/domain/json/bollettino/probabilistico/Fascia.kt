package com.gmail.fattazzo.meteo.domain.json.bollettino.probabilistico

import com.google.gson.annotations.SerializedName

/**
 * @author fattazzo
 *         <p/>
 *         date: 27/10/17
 */
class Fascia {

    @SerializedName("fascia")
    var nome: String? = null

    @SerializedName("fasciaOre")
    var ore: String? = null

    var fenomeni: List<Fenomeno>? = null
}