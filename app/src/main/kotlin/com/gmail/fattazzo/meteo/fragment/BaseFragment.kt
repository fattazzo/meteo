package com.gmail.fattazzo.meteo.fragment

import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import com.afollestad.materialdialogs.MaterialDialog
import com.gmail.fattazzo.meteo.R
import com.gmail.fattazzo.meteo.fragment.home.HomeFragment_
import com.gmail.fattazzo.meteo.utils.FragmentUtils
import org.androidannotations.annotations.EFragment
import org.androidannotations.annotations.UiThread


/**
 * @author fattazzo
 *         <p/>
 *         date: 26/10/17
 */
@EFragment
abstract class BaseFragment : Fragment() {

    private var dialog: MaterialDialog? = null

    @UiThread(propagation = UiThread.Propagation.REUSE)
    open fun openIndeterminateDialog(title: String) {
        dialog = MaterialDialog.Builder(activity)
                .title(title)
                .content(R.string.wait)
                .progress(true, 0)
                .cancelable(false)
                .progressIndeterminateStyle(true)
                .build()
        dialog!!.show()
    }

    @UiThread(propagation = UiThread.Propagation.REUSE)
    open fun closeIndeterminateDialog() {
        if (dialog != null && dialog!!.isShowing) {
            dialog!!.dismiss()
        }
    }

    open fun backPressed() {
        FragmentUtils.replace(activity as AppCompatActivity, HomeFragment_.builder().build())
    }

    abstract fun getTitleResId(): Int
}