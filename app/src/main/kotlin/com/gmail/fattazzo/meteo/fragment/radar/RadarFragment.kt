package com.gmail.fattazzo.meteo.fragment.radar

import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.os.Handler
import android.widget.FrameLayout
import android.widget.ImageSwitcher
import android.widget.ImageView
import android.widget.LinearLayout
import com.gmail.fattazzo.meteo.R
import com.gmail.fattazzo.meteo.fragment.BaseFragment
import com.gmail.fattazzo.meteo.utils.LoadBitmapTask
import org.androidannotations.annotations.*
import org.jsoup.Jsoup
import java.util.*

/**
 * @author fattazzo
 *
 *
 * date: 13/lug/2015
 */
@OptionsMenu(R.menu.radar_menu)
@EFragment(R.layout.fragment_radar)
open class RadarFragment : BaseFragment() {

    internal var imageSwitcher: ImageSwitcher? = null

    private val images = ArrayList<Bitmap>()

    private val mHandler = Handler()
    private var mUpdateResults: Runnable? = null
    private var timerAnimate = Timer()

    private var curIndex = 0

    override fun getTitleResId(): Int {
        return R.string.nav_radar
    }

    @AfterViews
    internal fun initView() {
        imageSwitcher = view!!.findViewById(R.id.imageSwitcher)
        imageSwitcher!!.setFactory {
            val imageView = ImageView(context)
            imageView.scaleType = ImageView.ScaleType.FIT_CENTER
            imageView.layoutParams = FrameLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT)
            imageView
        }

        reloadRadar()
    }

    @AfterInject
    internal fun init() {
        mUpdateResults = Runnable { this.animateandSlideShow() }
    }

    fun animateandSlideShow() {
        if (!images.isEmpty()) {
            if (curIndex == images.size - 1) {
                curIndex = 0
            }
            if (this@RadarFragment.isAdded && imageSwitcher != null) {
                imageSwitcher!!.setImageDrawable(BitmapDrawable(resources, images[curIndex]))
                curIndex++
            }
        }
    }

    /**
     * Ricarica il radar.
     */
    @UiThread(propagation = UiThread.Propagation.REUSE)
    internal open fun reloadRadar() {
        openIndeterminateDialog("Caricamento immagini in corso...")
        timerAnimate.cancel()
        getRadarImagesLink()
    }

    @Background
    internal open fun getRadarImagesLink() {
        images.clear()
        curIndex = 0
        try {
            val doc = Jsoup.connect("http://content.meteotrentino.it/dati-meteo/radar/loop/radar_inc_N.aspx").get()
            val scripts = doc.select("script")

            for (script in scripts) {
                if (script.html().contains("var immagini")) {
                    var split = script.html().split("\n".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()

                    val imgStringArray = split.firstOrNull { it.startsWith("immagini[0]") }
                            ?: ""

                    if (!imgStringArray.isEmpty()) {
                        val singles = imgStringArray.split(";".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                        for (single in singles) {
                            split = single.split("=".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()

                            val imgName = split[1].replace("\"", "")
                            images.add(LoadBitmapTask().execute("https://content.meteotrentino.it/dati-meteo/radar/loop/" + imgName).get())
                        }

                    }
                }
            }
        } catch (e: Exception) {

        }

        startRadarAnimation()
    }

    @UiThread(propagation = UiThread.Propagation.REUSE)
    internal open fun startRadarAnimation() {
        val timerTask = object : TimerTask() {
            override fun run() {
                mHandler.post(mUpdateResults)
            }
        }

        timerAnimate = Timer()
        timerAnimate.scheduleAtFixedRate(timerTask, 0, 1000)

        closeIndeterminateDialog()
    }

    @OptionsItem
    fun radar_refresh_action() {
        reloadRadar()
    }
}
