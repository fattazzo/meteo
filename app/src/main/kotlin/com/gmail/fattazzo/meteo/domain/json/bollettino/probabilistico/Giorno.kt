package com.gmail.fattazzo.meteo.domain.json.bollettino.probabilistico

import com.google.gson.annotations.SerializedName
import java.util.*

/**
 * @author fattazzo
 *         <p/>
 *         date: 27/10/17
 */
class Giorno {

    @SerializedName("nomeGiorno")
    var nome: String? = null

    @SerializedName("giorno")
    var data: Date? = null

    var fasce: List<Fascia>? = null
}