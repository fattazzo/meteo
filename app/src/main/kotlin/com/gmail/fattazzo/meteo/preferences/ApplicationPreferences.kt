package com.gmail.fattazzo.meteo.preferences

import org.androidannotations.annotations.sharedpreferences.DefaultString
import org.androidannotations.annotations.sharedpreferences.SharedPref

/**
 * @author fattazzo
 *         <p/>
 *         date: 27/10/17
 */
@SharedPref(value = SharedPref.Scope.APPLICATION_DEFAULT)
interface ApplicationPreferences {

    fun language(): String?

    @DefaultString("0")
    fun lastRunVersionName(): String
}