package com.gmail.fattazzo.meteo.domain.json.bollettino.probabilistico

import java.util.*

/**
 * @author fattazzo
 *         <p/>
 *         date: 27/10/17
 */
class BollettinoProbabilistico {

    var dataPubblicazione: Date? = null

    var giorni: List<Giorno>? = null

    fun fenomeniPresenti(): Set<String> {
        val elencoFenomeni = TreeSet<String>()

        giorni.orEmpty()
                .flatMap { it.fasce.orEmpty() }
                .flatMap { it.fenomeni.orEmpty() }
                .mapTo(elencoFenomeni) { it.nome.orEmpty() }

        return elencoFenomeni
    }
}