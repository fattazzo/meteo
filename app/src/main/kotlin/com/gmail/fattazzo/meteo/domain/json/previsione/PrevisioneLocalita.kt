package com.gmail.fattazzo.meteo.domain.json.previsione

import com.google.gson.annotations.SerializedName
import java.util.*

/**
 *
 * @author fattazzo
 *
 * date: 26/ott/2017
 */
class PrevisioneLocalita {

    @SerializedName("fonte-da-citare")
    var citazioneHtml: String? = null

    @SerializedName("nome-titolare")
    var nomeTitolare: String? = null

    @SerializedName("nome-editore")
    var nomeEditore: String? = null

    var dataPubblicazione: Date? = null

    var evoluzioneBreve: String? = null

    @SerializedName("previsione")
    val previsioni: List<Previsione>? = null
}
