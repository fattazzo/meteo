package com.gmail.fattazzo.meteo.domain.json.bollettino.probabilistico

import com.google.gson.annotations.SerializedName

/**
 * @author fattazzo
 *         <p/>
 *         date: 27/10/17
 */
class Fenomeno {

    @SerializedName("fenomeno")
    var nome: String? = null

    var valore: Int = 0

    var probabilita: String? = null
}