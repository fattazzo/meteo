/**
 *
 */
package com.gmail.fattazzo.meteo.fragment.home

import android.widget.LinearLayout
import com.afollestad.materialdialogs.MaterialDialog
import com.gmail.fattazzo.meteo.R
import com.gmail.fattazzo.meteo.domain.json.previsione.PrevisioneLocalita
import com.gmail.fattazzo.meteo.fragment.BaseFragment
import com.gmail.fattazzo.meteo.manager.MeteoManager
import com.gmail.fattazzo.meteo.preferences.ApplicationPreferencesManager
import org.androidannotations.annotations.*
import java.text.SimpleDateFormat


/**
 * @author fattazzo
 *
 *
 * date: 09/giu/2014
 */
@OptionsMenu(R.menu.home_menu)
@EFragment(R.layout.fragment_home)
open class HomeFragment : BaseFragment() {

    @ViewById
    lateinit internal var containerLayout: LinearLayout

    @Bean
    lateinit internal var meteoManager: MeteoManager

    @Bean
    lateinit internal var applicationPreferencesManager: ApplicationPreferencesManager

    private var previsioneLocalita: PrevisioneLocalita? = null

    override fun getTitleResId(): Int {
        return R.string.nav_home
    }

    @UiThread(propagation = UiThread.Propagation.REUSE)
    open fun bindView() {
        try {
            containerLayout.removeAllViews()

            val layoutParams = LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
            layoutParams.setMargins(20, 20, 20, 0)

            val previsioneView = PrevisioneView_.build(context)
            previsioneView.bind(previsioneLocalita)
            containerLayout.addView(previsioneView, layoutParams)

            for (giorno in previsioneLocalita?.previsioni?.get(0)?.giorni.orEmpty()) {
                val giornoView = GiornoView_.build(context)
                giornoView.bind(giorno)
                containerLayout.addView(giornoView, layoutParams)
            }
        } catch (e: Exception) {
            previsioneLocalita = null
        }
    }

    @Background
    open fun downloadPrevisione() {
        try {
            if (previsioneLocalita == null) {
                previsioneLocalita = meteoManager.caricaPrevisioneLocalita()
            }
        } finally {
            closeIndeterminateDialog()
        }

        bindView()
    }

    @UiThread(propagation = UiThread.Propagation.REUSE)
    open fun caricaPrevisione() {
        openIndeterminateDialog(getString(R.string.forecast_loading_dialog))
        downloadPrevisione()
    }

    @AfterViews
    fun initViews() {

        caricaPrevisione()

        dateFormat = SimpleDateFormat("EEEE dd/MM/yyyy", applicationPreferencesManager.getAppLocale())
    }

    @OptionsItem
    fun home_refresh_action() {
        previsioneLocalita = null
        caricaPrevisione()
    }

    override fun backPressed() {
        MaterialDialog.Builder(context)
                .icon(resources.getDrawable(R.drawable.info))
                .title(R.string.app_name)
                .content(R.string.exit_message)
                .positiveText(android.R.string.yes)
                .positiveColorRes(R.color.colorPrimary)
                .negativeText(android.R.string.no)
                .negativeColorRes(R.color.colorPrimary)
                .onPositive(MaterialDialog.SingleButtonCallback { _, _ -> activity.finish() })
                .show()
    }

    companion object {

        private val TAG = "HomeFragment"

        private var dateFormat: SimpleDateFormat? = null
    }
}
