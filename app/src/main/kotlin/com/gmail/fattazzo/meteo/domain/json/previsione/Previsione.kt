package com.gmail.fattazzo.meteo.domain.json.previsione

/**
 *
 * @author fattazzo
 *
 * date: 26/ott/2017
 */
class Previsione {

    var localita: String? = null

    var quota: Int? = null

    var giorni : List<Giorno>? = null
}