package com.gmail.fattazzo.meteo.widget.providers.previsione.locale

import android.content.Context
import android.content.Intent
import android.widget.RemoteViews
import android.widget.RemoteViewsService
import com.gmail.fattazzo.meteo.R
import com.gmail.fattazzo.meteo.activity.SplashActivity
import com.gmail.fattazzo.meteo.domain.json.previsione.Giorno
import com.gmail.fattazzo.meteo.manager.MeteoManager
import com.gmail.fattazzo.meteo.manager.MeteoManager_
import com.gmail.fattazzo.meteo.settings.widget.bollettino.BollettinoWidgetsSettingsManager
import com.gmail.fattazzo.meteo.utils.LoadBitmapTask
import com.gmail.fattazzo.meteo.widget.providers.previsione.LoadPrevisioneLocalitaTask
import com.gmail.fattazzo.meteo.widget.providers.previsione.fascia.corrente.FasceListRemoteViewsFactory
import java.text.SimpleDateFormat
import java.util.*

/**
 * @author fattazzo
 *
 *
 * date: 27/10/17
 */
class PrevisioneLocaleGridRemoteViewsFactory(private val mContext: Context, intent: Intent) : RemoteViewsService.RemoteViewsFactory {
    private val mWidgetItems = ArrayList<Giorno>()

    private val preferencesManager: BollettinoWidgetsSettingsManager

    private var data: String? = null

    var meteoManager: MeteoManager? = null

    init {
        meteoManager = MeteoManager_.getInstance_(mContext)

        data = intent.getStringExtra(FasceListRemoteViewsFactory.EXTRA_DATA)

        val previsione = try {
            LoadPrevisioneLocalitaTask(meteoManager!!).execute().get()
        } catch (e: Exception) {
            null
        }

        mWidgetItems.addAll(previsione?.previsioni.orEmpty().firstOrNull()?.giorni.orEmpty())

        preferencesManager = BollettinoWidgetsSettingsManager(mContext)
    }

    override fun onCreate() {
    }

    override fun onDataSetChanged() {
    }

    override fun onDestroy() {
    }

    override fun getCount(): Int {
        return mWidgetItems.size
    }

    override fun getViewAt(position: Int): RemoteViews {
        val giorno = mWidgetItems[position]

        val rv = RemoteViews(mContext.packageName, R.layout.widget_previsione_locale_item)
        try {
            rv.setBitmap(R.id.iconaImageView, "setImageBitmap", LoadBitmapTask().execute(giorno.icona).get())
        } catch (e: Exception) {
            rv.setBitmap(R.id.iconaImageView, "setImageBitmap", null)
        }

        data.let {
            if (position == 0) rv.setTextViewText(R.id.oraTV, data) else rv.setTextViewText(R.id.oraTV, "")
        }
        rv.setTextColor(R.id.oraTV, preferencesManager.textColor)

        val dateFormatter = SimpleDateFormat("EEEE dd/MM/yyyy", Locale.getDefault())
        rv.setTextViewText(R.id.giornoTV, dateFormatter.format(giorno.data).capitalize())
        rv.setTextColor(R.id.giornoTV, preferencesManager.textColor)

        rv.setTextViewText(R.id.testoTV, giorno.testo.orEmpty().capitalize())
        rv.setTextColor(R.id.testoTV, preferencesManager.textColor)

        val fillInIntent = Intent(mContext, SplashActivity::class.java)
        rv.setOnClickFillInIntent(R.id.widget_background_layout, fillInIntent)

        return rv
    }

    override fun getLoadingView(): RemoteViews? {
        return null
    }


    override fun getViewTypeCount(): Int {
        return 1
    }

    override fun getItemId(position: Int): Long {
        return 0
    }

    override fun hasStableIds(): Boolean {
        return true
    }

    companion object {
        val EXTRA_DATA = "extraData"
    }

}