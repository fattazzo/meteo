package com.gmail.fattazzo.meteo

/**
 * @author fattazzo
 *         <p/>
 *         date: 27/10/17
 */
class Config {

    companion object {

        val PREVISIONE_LOCALITA_URL: String = "https://www.meteotrentino.it/protcivtn-meteo/api/front/previsioneOpenDataLocalita?localita=TRENTO"

        val BOLLETTINO_PROBABILISTICO_URL: String = "https://www.meteotrentino.it/protcivtn-meteo/api/front/previsioneOpenDataProbabilistico"
    }
}