package com.gmail.fattazzo.meteo.fragment.home

import android.content.Context
import android.support.constraint.ConstraintLayout
import android.util.AttributeSet
import android.widget.ImageView
import android.widget.TextView
import com.gmail.fattazzo.meteo.R
import com.gmail.fattazzo.meteo.domain.json.previsione.Fascia
import com.squareup.picasso.Picasso
import org.androidannotations.annotations.EViewGroup
import org.androidannotations.annotations.ViewById

/**
 * @author fattazzo
 *         <p/>
 *         date: 26/10/17
 */
@EViewGroup(R.layout.item_fascia)
open class FasciaView : ConstraintLayout {

    constructor(context: Context?) : super(context)
    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    @ViewById
    lateinit internal var descrizioneTV: TextView

    @ViewById
    lateinit internal var oreTV: TextView

    @ViewById
    lateinit internal var iconaImageView: ImageView

    @ViewById
    lateinit internal var descrizioneIconaTV: TextView

    @ViewById
    lateinit internal var probPrecTV: TextView
    @ViewById
    lateinit internal var intensitaPrecTV: TextView
    @ViewById
    lateinit internal var descTemporaliTV: TextView

    @ViewById
    lateinit internal var ventoValleDescTV: TextView
    @ViewById
    lateinit internal var ventoQuotaDescTV: TextView

    @ViewById
    lateinit internal var zeroTermicoTV: TextView

    fun bind(fascia: Fascia) {

        descrizioneTV.text = fascia.descrizione.orEmpty().capitalize()

        oreTV.text = "( ore ${fascia.ore} )"

        if (!fascia.icona.isNullOrBlank()) {
            Picasso.with(context).load(fascia.icona).into(iconaImageView)
        }
        descrizioneIconaTV.text = fascia.descIcona.orEmpty()

        probPrecTV.text = fascia.descProbabilitaPrecipitazioni.orEmpty()
        intensitaPrecTV.text = fascia.descIntensitaPrecipitazioni.orEmpty()
        descTemporaliTV.text = fascia.descProbabilitaTemporali.orEmpty()

        val sbQuota = StringBuilder()
        if (fascia.descVentoIntensitaQuota.orEmpty().isNotBlank()) {
            sbQuota.append(fascia.descVentoIntensitaQuota)
        }
        if (fascia.descVentoDirezioneQuota.orEmpty().isNotBlank()) {
            sbQuota.append(" direzione ").append(fascia.descVentoDirezioneQuota)
        }
        ventoQuotaDescTV.text = sbQuota.toString()

        val sbValle = StringBuilder()
        if (fascia.descVentoIntensitaValle.orEmpty().isNotBlank()) {
            sbValle.append(fascia.descVentoIntensitaValle)
        }
        if (fascia.descVentoDirezioneValle.orEmpty().isNotBlank()) {
            sbValle.append(" direzione ").append(fascia.descVentoDirezioneValle)
        }
        ventoValleDescTV.text = sbValle.toString()

        zeroTermicoTV.text = if (fascia.zeroTermico == null) "--" else "${fascia.zeroTermico} metri"
    }

}