package com.gmail.fattazzo.meteo.utils


import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import com.gmail.fattazzo.meteo.R
import com.gmail.fattazzo.meteo.fragment.BaseFragment

/**
 * @author fattazzo
 *
 *
 * date: 20/10/17
 */
class FragmentUtils {

    companion object {

        @JvmOverloads
        fun replace(activity: AppCompatActivity, fragment: Fragment, containerResId: Int = R.id.container) {
            val transaction = activity.supportFragmentManager.beginTransaction()
            transaction.replace(containerResId, fragment, fragment.javaClass.simpleName.replace("_", "")).commit()

            if (fragment is BaseFragment) {
                activity.setTitle(fragment.getTitleResId())
            }
        }

        @JvmOverloads
        fun add(activity: AppCompatActivity, fragment: Fragment, containerResId: Int = R.id.container) {
            val transaction = activity.supportFragmentManager.beginTransaction()
            transaction.add(containerResId, fragment, fragment.javaClass.simpleName.replace("_", "")).commit()
        }
    }
}