package com.gmail.fattazzo.meteo.widget.providers.previsione.fascia.corrente

import android.content.Context
import android.content.Intent
import android.view.View
import android.widget.RemoteViews
import android.widget.RemoteViewsService
import com.gmail.fattazzo.meteo.R
import com.gmail.fattazzo.meteo.activity.SplashActivity
import com.gmail.fattazzo.meteo.manager.MeteoManager
import com.gmail.fattazzo.meteo.manager.MeteoManager_
import com.gmail.fattazzo.meteo.settings.widget.bollettino.BollettinoWidgetsSettingsManager
import com.gmail.fattazzo.meteo.utils.LoadBitmapTask
import com.gmail.fattazzo.meteo.widget.providers.previsione.LoadPrevisioneLocalitaTask
import java.text.SimpleDateFormat
import java.util.*


/**
 * @author fattazzo
 *
 *
 * date: 27/10/17
 */
class FasceListRemoteViewsFactory(private val mContext: Context, intent: Intent) : RemoteViewsService.RemoteViewsFactory {
    private val mWidgetItems = ArrayList<FasciaWidget>()

    private var data: String? = null

    private val preferencesManager: BollettinoWidgetsSettingsManager

    var meteoManager: MeteoManager? = null

    init {
        meteoManager = MeteoManager_.getInstance_(mContext)

        data = intent.getStringExtra(EXTRA_DATA)

        val previsione = try {
            LoadPrevisioneLocalitaTask(meteoManager!!).execute().get()
        } catch (e: Exception) {
            null
        }

        for (giorno in previsione?.previsioni.orEmpty().firstOrNull()?.giorni.orEmpty()) {
            giorno.fasce.orEmpty().mapTo(mWidgetItems) { FasciaWidget(giorno.data, it, giorno.temperaturaMin, giorno.temperaturaMax) }
        }

        preferencesManager = BollettinoWidgetsSettingsManager(mContext)
    }

    override fun onCreate() {
    }

    override fun onDataSetChanged() {
        println("onDataSetChanged")
    }

    override fun onDestroy() {
    }

    override fun getCount(): Int {
        return mWidgetItems.size
    }

    override fun getViewAt(position: Int): RemoteViews {
        val fascia = mWidgetItems[position].fascia

        val rv = RemoteViews(mContext.packageName, R.layout.widget_fasce_item)
        try {
            rv.setBitmap(R.id.iconaImageView, "setImageBitmap", LoadBitmapTask().execute(fascia.icona).get())
        } catch (e: Exception) {
            rv.setBitmap(R.id.iconaImageView, "setImageBitmap", null)
        }

        mWidgetItems[position].data.let {
            if (position == 0) rv.setTextViewText(R.id.syncDateTV, data) else rv.setTextViewText(R.id.syncDateTV, "")
        }
        rv.setTextColor(R.id.syncDateTV, preferencesManager.textColor)

        val dateFormatter = SimpleDateFormat("EEEE dd/MM/yyyy", Locale.getDefault())
        rv.setTextViewText(R.id.giornoTV, dateFormatter.format(mWidgetItems[position].data).capitalize())
        rv.setTextColor(R.id.giornoTV, preferencesManager.textColor)

        rv.setTextViewText(R.id.descrizioneTV, fascia.descrizione.orEmpty().capitalize() + " ore " + fascia.ore)
        rv.setTextColor(R.id.descrizioneTV, preferencesManager.textColor)

        rv.setTextViewText(R.id.temporaliTV, fascia.descProbabilitaTemporali.orEmpty().capitalize())
        rv.setTextColor(R.id.temporaliTV, preferencesManager.textColor)

        rv.setTextViewText(R.id.precipitazioniTV, fascia.descProbabilitaPrecipitazioni.orEmpty().capitalize())
        rv.setTextColor(R.id.precipitazioniTV, preferencesManager.textColor)

        rv.setTextViewText(R.id.ventoTV, fascia.descVentoIntensitaValle.orEmpty().capitalize())
        rv.setTextColor(R.id.ventoTV, preferencesManager.textColor)

        rv.setViewVisibility(R.id.tempMinTV, View.GONE)
        rv.setTextColor(R.id.tempMinTV, preferencesManager.textColor)
        if (mWidgetItems[position].temperaturaMin != null) {
            rv.setViewVisibility(R.id.tempMinTV, View.VISIBLE)
            rv.setTextViewText(R.id.tempMinTV, mWidgetItems[position].temperaturaMin.toString() + " °C")
        }

        rv.setViewVisibility(R.id.tempMaxTV, View.GONE)
        rv.setTextColor(R.id.tempMaxTV, preferencesManager.textColor)
        if (mWidgetItems[position].temperaturaMax != null) {
            rv.setViewVisibility(R.id.tempMaxTV, View.VISIBLE)
            rv.setTextViewText(R.id.tempMaxTV, mWidgetItems[position].temperaturaMax.toString() + " °C")
        }

        val fillInIntent = Intent(mContext, SplashActivity::class.java)
        rv.setOnClickFillInIntent(R.id.widget_background_layout, fillInIntent)
        return rv
    }

    override fun getLoadingView(): RemoteViews? {
        return null
    }


    override fun getViewTypeCount(): Int {
        return 1
    }

    override fun getItemId(position: Int): Long {
        return 0
    }

    override fun hasStableIds(): Boolean {
        return true
    }

    companion object {

        val EXTRA_DATA = "extraData"
    }

}