package com.gmail.fattazzo.meteo.activity

import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.view.GravityCompat
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.MenuItem
import com.gmail.fattazzo.meteo.R
import com.gmail.fattazzo.meteo.fragment.AboutFragment_
import com.gmail.fattazzo.meteo.fragment.BaseFragment
import com.gmail.fattazzo.meteo.fragment.SettingsFragment
import com.gmail.fattazzo.meteo.fragment.bollettino.probabilistico.BollettinoProbabilisticoFragment_
import com.gmail.fattazzo.meteo.fragment.home.HomeFragment_
import com.gmail.fattazzo.meteo.fragment.radar.RadarFragment_
import com.gmail.fattazzo.meteo.fragment.stazioni.StazioniFragment
import com.gmail.fattazzo.meteo.fragment.valanghe.ValangheFragment
import com.gmail.fattazzo.meteo.fragment.webcam.WebcamFragment
import com.gmail.fattazzo.meteo.fragment.wiki.WikiFragment_
import com.gmail.fattazzo.meteo.parser.versioni.VersioniManager
import com.gmail.fattazzo.meteo.utils.FragmentUtils
import org.androidannotations.annotations.AfterViews
import org.androidannotations.annotations.EActivity
import org.androidannotations.annotations.ViewById


/**
 * @author fattazzo
 *
 *
 * date: 09/giu/2014
 */
@EActivity(R.layout.activity_main)
open class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    @ViewById
    lateinit internal var nav_view: NavigationView

    @ViewById
    lateinit internal var drawer_layout: DrawerLayout

    @ViewById
    lateinit internal var toolbar: Toolbar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (savedInstanceState == null) {
            FragmentUtils.replace(this, HomeFragment_.builder().build())
        }
    }

    @AfterViews
    fun initMaterial() {
        setSupportActionBar(toolbar)

        val toggle = ActionBarDrawerToggle(this, drawer_layout, toolbar, 0, 0)
        drawer_layout.setDrawerListener(toggle)
        toggle.syncState()

        nav_view.setNavigationItemSelectedListener(this)
    }

    override fun onNavigationItemSelected(menuItem: MenuItem): Boolean {
        when (menuItem.itemId) {
            R.id.nav_home -> FragmentUtils.replace(this, HomeFragment_.builder().build())
            R.id.nav_boll_prob -> FragmentUtils.replace(this, BollettinoProbabilisticoFragment_.builder().build())
            R.id.nav_stazioni_meteo -> FragmentUtils.replace(this, StazioniFragment())
            R.id.nav_stazioni_neve -> FragmentUtils.replace(this, ValangheFragment())
            R.id.nav_radar -> FragmentUtils.replace(this, RadarFragment_.builder().build())
            R.id.nav_webcam -> FragmentUtils.replace(this, WebcamFragment())
            R.id.nav_preferences -> FragmentUtils.replace(this, SettingsFragment())
            R.id.nav_guida -> FragmentUtils.replace(this, WikiFragment_.builder().build())
            R.id.nav_about -> FragmentUtils.replace(this, AboutFragment_.builder().build())
        }

        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)

        val versioniManager = VersioniManager(this)
        if (versioniManager.checkShowVersionChangelog()) {
            versioniManager.showVersionChangelog()
        }
    }

    override fun onBackPressed() {
        val fragmentManager = supportFragmentManager

        val fragments = fragmentManager.fragments
        val fragment = fragments?.firstOrNull()

        if (fragment != null) {
            if (fragment is BaseFragment) {
                fragment.backPressed()
            }
            if (fragment is SettingsFragment) {
                fragment.backPressed()
            }
        }
    }
}
