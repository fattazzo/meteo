package com.gmail.fattazzo.meteo.fragment.home;

import android.support.test.espresso.action.ViewActions;
import android.support.test.runner.AndroidJUnit4;

import com.gmail.fattazzo.meteo.BaseTest;
import com.gmail.fattazzo.meteo.R;

import junit.framework.AssertionFailedError;

import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.pressBack;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.RootMatchers.isDialog;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

/**
 * @author fattazzo
 *         <p/>
 *         date: 29/02/16
 */
@RunWith(AndroidJUnit4.class)
public class BollettinoGeneraleTest extends BaseTest {

    @Test
    public void caricaBollettinoGenerale() {

        selectMenu(AppMenu.HOME);

        //onView(withId(R.id.home_layout_domani_include)).perform(ViewActions.scrollTo()).check(matches(isDisplayed()));
        //onView(withId(R.id.home_layout_dopodomani_include)).perform(ViewActions.scrollTo()).check(matches(isDisplayed()));
        //onView(withId(R.id.home_tendenza_label)).perform(ViewActions.scrollTo()).check(matches(isDisplayed()));

        //onView(withId(R.id.home_refresh_action)).perform(click());

        //onView(withId(R.id.home_layout_domani_include)).perform(ViewActions.scrollTo()).check(matches(isDisplayed()));
        //onView(withId(R.id.home_layout_dopodomani_include)).perform(ViewActions.scrollTo()).check(matches(isDisplayed()));
        //onView(withId(R.id.home_tendenza_label)).perform(ViewActions.scrollTo()).check(matches(isDisplayed()));

        // Cambio pagina e ritorno sulla home per verificare che il bollettino sia ancora caricato
        selectMenu(AppMenu.BOLL_LOCALE);
        selectMenu(AppMenu.HOME);

        //onView(withId(R.id.home_layout_domani_include)).perform(ViewActions.scrollTo()).check(matches(isDisplayed()));
        //onView(withId(R.id.home_layout_dopodomani_include)).perform(ViewActions.scrollTo()).check(matches(isDisplayed()));
        //onView(withId(R.id.home_tendenza_label)).perform(ViewActions.scrollTo()).check(matches(isDisplayed()));
    }

    @Test
    public void infoDialogBollettinoGenerale() {

        selectMenu(AppMenu.HOME);

        /**
        // Dialog previsione
        //onView(withId(R.id.home_img_previsione_info)).perform(ViewActions.scrollTo()).perform(click());
        onView(withText("Informazioni previsione")).inRoot(isDialog()).check(matches(isDisplayed()));
        onView(withId(R.id.prev_info_data_prev)).check(matches(isDisplayed()));
        onView(withText("Informazioni previsione")).inRoot(isDialog()).perform(pressBack());

        // Dialog oggi
        try {
            onView(withId(R.id.home_oggi_info)).check(matches(isDisplayed()));
            // View is displayed
            onView(withId(R.id.home_oggi_info)).perform(ViewActions.scrollTo()).perform(click());
            onView(withText("Informazioni dettagliate")).inRoot(isDialog()).check(matches(isDisplayed()));
            onView(withId(R.id.oggi_info_data)).check(matches(isDisplayed()));
            onView(withText("Informazioni dettagliate")).inRoot(isDialog()).perform(pressBack());
        } catch (AssertionFailedError e) {
            // View not displayed
        }

        // Dialog domani
        onView(withId(R.id.home_domani_info)).perform(ViewActions.scrollTo()).perform(click());
        onView(withText("Informazioni dettagliate")).inRoot(isDialog()).check(matches(isDisplayed()));
        onView(withId(R.id.domani_info_data)).check(matches(isDisplayed()));
        onView(withText("Informazioni dettagliate")).inRoot(isDialog()).perform(pressBack());

        // Dialog dopodomani
        onView(withId(R.id.home_dopodomani_info)).perform(ViewActions.scrollTo()).perform(click());
        onView(withText("Informazioni dettagliate")).inRoot(isDialog()).check(matches(isDisplayed()));
        onView(withId(R.id.domani_info_data)).check(matches(isDisplayed()));
        onView(withText("Informazioni dettagliate")).inRoot(isDialog()).perform(pressBack());

         **/
    }
}
