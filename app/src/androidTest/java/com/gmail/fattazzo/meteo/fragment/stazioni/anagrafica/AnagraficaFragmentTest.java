package com.gmail.fattazzo.meteo.fragment.stazioni.anagrafica;

import com.gmail.fattazzo.meteo.BaseTest;
import com.gmail.fattazzo.meteo.R;
import com.gmail.fattazzo.meteo.db.handlers.MeteoDatabaseHandler;

import org.junit.Before;
import org.junit.Test;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static com.gmail.fattazzo.meteo.matcher.Matcher.withListNotEmpty;
import static com.gmail.fattazzo.meteo.matcher.Matcher.withListSize;
import static org.junit.Assert.assertNotNull;

/**
 * @author fattazzo
 *         <p/>
 *         date: 05/03/16
 */
public class AnagraficaFragmentTest extends BaseTest {

    private MeteoDatabaseHandler databaseHandler;

    @Before
    public void setUp() throws Exception {

        databaseHandler = MeteoDatabaseHandler.getInstance(getContext());
        assertNotNull("Context nullo", databaseHandler);
    }

    @Test
    public void caricaAnagraficaStazioni() {

        selectMenu(AppMenu.STAZIONI);

        // Mi sposto sulla sezione anagrafica
        String[] stringArray = getContext().getResources().getStringArray(R.array.stazioni_sezioni);
        onView(withText(stringArray[1])).perform(click());

        // Carico le stazioni presenti e verifico che tutte siano caricate nella ListView
        checkView();

        // Eseguo un refresh
        onView(withId(R.id.anag_stazioni_refresh_button)).perform(click());

        // Verifico di nuovo che tutto sia caricato
        checkView();

        // Cambio menu e ritorno per verificare che tutto funzioni
        selectMenu(AppMenu.HOME);
        selectMenu(AppMenu.STAZIONI);
        onView(withText(stringArray[1])).perform(click());

        checkView();
    }

    private void checkView() {
        int countStazioni = databaseHandler.getCountStazioni();
        onView(withId(R.id.anag_stazioni_list)).check(matches(withListNotEmpty()));
        onView(withId(R.id.anag_stazioni_list)).check(matches(withListSize(countStazioni)));
    }
}