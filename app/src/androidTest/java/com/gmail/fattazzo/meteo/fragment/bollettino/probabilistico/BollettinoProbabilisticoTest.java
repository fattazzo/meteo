package com.gmail.fattazzo.meteo.fragment.bollettino.probabilistico;

import android.support.test.espresso.matcher.ViewMatchers;
import android.support.test.runner.AndroidJUnit4;

import com.gmail.fattazzo.meteo.BaseTest;
import com.gmail.fattazzo.meteo.R;

import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.withEffectiveVisibility;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

/**
 * @author fattazzo
 *         <p/>
 *         date: 29/02/16
 */
@RunWith(AndroidJUnit4.class)
public class BollettinoProbabilisticoTest extends BaseTest {

    @Test
    public void caricaBollettinoProbabilistico() {

        /**
        // Apro la sezione del bollettino probabilistico
        selectMenu(AppMenu.BOLL_PROB);

        // Verifico che la previsione si stata caricata
        onView(withId(R.id.boll_prob_layout)).check(matches(withEffectiveVisibility(ViewMatchers.Visibility.VISIBLE)));

        // Vado sulla home e ritorno sul bollettino probabilistico per verificare che sia ancora caricata la previsione
        selectMenu(AppMenu.HOME);
        selectMenu(AppMenu.BOLL_PROB);
        onView(withId(R.id.boll_prob_layout)).check(matches(withEffectiveVisibility(ViewMatchers.Visibility.VISIBLE)));
         **/
    }
}
