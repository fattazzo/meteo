package com.gmail.fattazzo.meteo.parser.xml;

import com.gmail.fattazzo.meteo.domain.valanghe.bollettino.Bollettino;

import org.junit.Test;

import java.util.Locale;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

/**
 * @author fattazzo
 *         <p/>
 *         date: 01/03/16
 */
public class BollettinoValangheXmlParserTest {

    @Test
    public void testCaricaBollettino() throws Exception {

        BollettinoValangheXmlParser parser = new BollettinoValangheXmlParser();

        Bollettino bollettino = null;
        try {
            bollettino = parser.caricaBollettino();
        } catch (Exception e) {
            fail("Errore durante il caricamento del bollettino valanghe " + e.getMessage());
        }

        assertNotNull("Nessun bollettino valanghe caricato", bollettino);
        try {
            String giornoEmissione = bollettino.getGiornoEmissione(Locale.ITALIAN);
            assertNotNull("Giorno emissione non valido", giornoEmissione);
        } catch (Exception e) {
            fail("Errore durante il caricamento del giorno di emissione");
        }
    }
}