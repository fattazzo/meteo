package com.gmail.fattazzo.meteo.parser.xml;

import org.junit.Test;

/**
 * @author fattazzo
 *         <p/>
 *         date: 01/03/16
 */
public class BollettinoProbabilisticoXmlParserTest {

    @Test
    public void testCaricaBollettino() throws Exception {

        /**
         BollettinoProbabilisticoXmlParser parser = new BollettinoProbabilisticoXmlParser();

         BollettinoProbabilistico bollettinoProbabilistico = null;
         try {
         bollettinoProbabilistico = parser.caricaBollettino();
         } catch (Exception e) {
         fail("Errore durante il caricamento del bollettino probabilistico " + e.getMessage());
         }

         assertNotNull("Nessun bollettino probabilistico caricato", bollettinoProbabilistico);
         assertTrue("Giorno emissione non presente", bollettinoProbabilistico.getGiornoEmissione() != null && !bollettinoProbabilistico.getGiornoEmissione().isEmpty());
         assertTrue("Nessun fenomento presente", bollettinoProbabilistico.getFenomeni() != null && !bollettinoProbabilistico.getFenomeni().isEmpty());
         List<Giorno> giorni = bollettinoProbabilistico.getFenomeni().get(0).getGiorni();
         assertTrue("Nessun giorno per il fenomeno", giorni != null && !giorni.isEmpty());
         Giorno giorno = giorni.get(0);
         assertNotNull("Valore giorno nullo", giorno.getValore());

         **/
    }
}